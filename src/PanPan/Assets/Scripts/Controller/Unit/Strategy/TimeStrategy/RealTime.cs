﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealTime : TimeStrategy
{
    protected override bool IsThisStrategyOver()
    {
        return false;
    }

    protected override bool MustStart()
    {
        return false;
    }

    public override void UpdateTime()
    {
    }

    public override float GetDeltaTime()
    {
        return Time.deltaTime;
    }

    public override float GetFixedDeltaTime()
    {
        return Time.fixedDeltaTime;
    }

    public override float GetTimeMultiplier()
    {
        return 1f;
    }
}
