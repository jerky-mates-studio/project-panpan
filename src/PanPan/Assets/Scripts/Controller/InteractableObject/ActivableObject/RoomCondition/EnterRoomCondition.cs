using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class EnterRoomCondition : ActivationCondition
{
    /**
     * This method is called by Unity when this object collides with another
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Trigger an activation when the player enter the room
        if (collision.tag == "Player")
        {
            TriggerActivation(true);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // Trigger a deactivation when the player enter the room
        if (collision.tag == "Player")
        {
            TriggerActivation(false);
        }
    }
}
