using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(DoorBrokeEvent))]
public class BreakableDoorSoundManager : MonoBehaviour
{
    [Tooltip("The audio clip to play when a door is broken")]
    [SerializeField]
    private AudioClip brokenDoorClip;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        GetComponent<DoorBrokeEvent>().DoorBroke += PlayDoorBrokenSound;
    }

    private void OnDisable()
    {
        GetComponent<DoorBrokeEvent>().DoorBroke -= PlayDoorBrokenSound;
    }

    private void PlayDoorBrokenSound()
    {
        audioSource.PlayOneShot(brokenDoorClip);
    }
}
