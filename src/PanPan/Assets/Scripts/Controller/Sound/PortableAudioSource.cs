﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Allow an audio source to follow a game object. This audio source is destroyed once the clip has ficish playing.
 * It ensures a clip can play totally even if the source of sound is destroyed
 */
[RequireComponent(typeof(AudioSource))]
public class PortableAudioSource : MonoBehaviour
{
    [Tooltip("The object to follow")]
    public GameObject toFollow;
    [Tooltip("The minimum lifetime  of this object before it is destroyed (in seconds)")]
    private float minimumLifetime = 2f;

    [HideInInspector] public AudioSource audioSource;

    private float currentLifetime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        audioSource.Play();
    }

    /** This method is called is called when you first add the component to a GameObject.
     * You can also call the Reset method manually by right clicking the component header in the inspector,
     * and choosing the ‘Reset’ menu item.
     * It will automatically assign the AudioSource component to the "audioSource" variable
     */
    void Reset()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = true;
    }

    // Update is called once per frame
    // It follows the "toFollow" game object and it is destroyed only once the clip has finish playing
    void Update()
    {
        currentLifetime += Time.deltaTime;

        if(toFollow != null)
            transform.position = toFollow.transform.position;
        // Ensure this object is not destroyed before the clip starts playing
        if (currentLifetime >= minimumLifetime && !audioSource.isPlaying)
            GameObject.Destroy(gameObject);
    }
}
