using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class SkillExplanationTextDisplayer : MonoBehaviour
{
    [Tooltip("The skill data that will trigger the explanation display")]
    [SerializeField]
    private SkillItemData skillData;

    private void OnEnable()
    {
        PlayerPickUpNewSkillEvent.GetInstance().PlayerPickUpNewSkill += HandlePickedUpSkill;
    }

    private void OnDisable()
    {
        PlayerPickUpNewSkillEvent.GetInstance().PlayerPickUpNewSkill -= HandlePickedUpSkill;
    }

    private void HandlePickedUpSkill(SkillItemData skillItemData)
    {
        if( skillData.SkillName == skillItemData.SkillName )
            GetComponent<Animator>().SetBool("IsHidden", false);
    }
}
