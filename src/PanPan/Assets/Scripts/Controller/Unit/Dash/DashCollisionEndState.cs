using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashCollisionEndState : DashState
{
    private float stopDistance;
    private AI unitAI;

    public DashCollisionEndState(DashableUnit context) : base(context)
    {
        unitAI = Context.GetComponent<AI>();
    }

    public override void HandleCollision(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Shootable_Unit") || collision.gameObject.layer == LayerMask.NameToLayer("Block_Bullet"))
        {
            NonShooterUnit collidedNonShooterUnit = collision.gameObject.GetComponent<NonShooterUnit>();
            KillableUnit collidedKillableUnit = collision.gameObject.GetComponent<KillableUnit>();

            // Dashing units never take dash damages
            if (collidedNonShooterUnit != null && collidedNonShooterUnit.IsDashing())
                return;

            // Stops the projected unit and deal dash damage to unit
            stopDistance = 0f;
            Context.DamageUnit(Context.gameObject);

            // If collided object can take damage, apply dash damage to it too 
            if (collidedNonShooterUnit != null || collidedKillableUnit != null)
                Context.DamageUnit(collision.gameObject);

        }
    }

    public override void OnEnterState()
    {
        stopDistance = Context.ProjectionDistance;
        if (unitAI != null)
            unitAI.enabled = false;
    }

    public override void UpdateState()
    {
        if (stopDistance > 0f) {
            float distance = Mathf.Min( Context.ProjectionSpeed * Time.deltaTime, stopDistance);
            Vector3 movement = new Vector3(Context.ProjectionMovement.x, Context.ProjectionMovement.y) * distance;
            stopDistance -= distance;
            // Make absolute translation in order to avoid strange behaviour if the object rotate during the movement
            Context.transform.position = Context.transform.position + movement;
        }
        else
        {
            Context.SwitchState(Context.IdleState);
        }

    }
}
