using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDiesCondition : ActivationCondition
{
    private void OnEnable()
    {
        PlayerDiedEvent.GetInstance().PlayerDied += OnPlayerDies;
    }

    private void OnDisable()
    {
        PlayerDiedEvent.GetInstance().PlayerDied -= OnPlayerDies;
    }

    /// <summary>
    /// Trigger the activation of the condition when the player dies and that this condition is activated.
    /// </summary>
    /// <param name="player">The player that is dead (not used)</param>
    private void OnPlayerDies(Hero player)
    {
        TriggerActivation(true);
    }
}
