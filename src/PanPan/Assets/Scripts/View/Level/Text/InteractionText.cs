﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionText : FloatingText
{
    [Tooltip("The game object that holds the sprite of the Interaction button")]
    [SerializeField]
    private GameObject buttonSprite;

    // Start is called before the first frame update
    void Start()
    {
        DeviceManager.Instance.GetComponent<DeviceManager>().OnDeviceChanged += UpdateInteractionButton;
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Is triggered when the player use a new device to play. It changes the sprite of the interaction button
    /// in order to match the button of the new device
    /// </summary>
    /// <param name="sender">The sender of the event</param>
    /// <param name="e">The event triggered</param>
    private void UpdateInteractionButton(object sender, EventArgs e)
    {
        buttonSprite.GetComponent<SpriteRenderer>().sprite = DeviceManager.Instance.GetComponent<DeviceManager>().CurrentDevice.InteractionButton;
    }
}
