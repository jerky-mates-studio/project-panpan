﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitTookDamagesEvent))]
[RequireComponent(typeof(MovingStrategy))]
public class Enemy : ShooterUnit
{
    [System.Serializable]
    public struct LootPossibility
    {
        [Tooltip("The object that can be looted")]
        public GameObject loot;
        [Tooltip("The chances that this object is looted")]
        [Range(0, 1)]
        public float chances;
        [Tooltip("The amount of object that is looted")]
        public int amount;
        [Tooltip("The minimum multiplier for the amount of loot")]
        [Min(0.5f)]
        public float minMultiplier;
        [Tooltip("The maximum multiplier for the amount of loot")]
        public float maxMultiplier;
    }

    [Header("Loot")]
    [Tooltip("The list of object this enemy can possibly loot. The float represents the chance that a particular object is dropped when the enemy die")]
    public List<LootPossibility> possibleLoots = new List<LootPossibility>(0);

    private MovingStrategy currentMovingStrategy;
    public MovingStrategy CurrentMovingStrategy { get => currentMovingStrategy; }

    private UnitTookDamagesEvent unitTookDamagesEvent;

    // Needed to fix a bug that make some units trigger the "Enemy died" event twice
    private bool haveAlreadyDied = false;

    protected override void Awake()
    {
        base.Awake();
        InitialiazeStrategy<MovingStrategy>(GetComponent<MovingStrategy>(), GetComponent<MovingStrategy>(), out currentMovingStrategy);
        unitTookDamagesEvent = GetComponent<UnitTookDamagesEvent>();
    }

    protected override void Update()
    {
        base.Update();
        currentMovingStrategy.Move();
    }

    protected override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        unitTookDamagesEvent.NotifyUnitTookDamages(damage);
    }


    /**
     * Drop the loot listed in "possibleLoots" acoording to the probabilities attached to each loot object.
     * The amount of loot depends on the multiplier applied to the amount property (round to the above integer).
     * The multiplier is a random float chosen uniformely between the "minMultiplier" and "maxMultiplier" properties of the LootPossibility
     */
    private void DropLoot()
    {
        foreach(LootPossibility lootPossibility in possibleLoots)
        {
            float probability = Random.Range(0f, 1f);
            if (probability <= lootPossibility.chances)
            {
                // prevent all the loots to have exact same position by adding random position in a circle of radius 0.01 centered on the enemy
                Vector3 offsetVector = Random.insideUnitCircle * 0.01f;
                GameObject loot = Instantiate(lootPossibility.loot, transform.position + offsetVector, transform.rotation);
                float multiplier = Random.Range(lootPossibility.minMultiplier, lootPossibility.maxMultiplier);
                int lootAmount = (int) Mathf.Ceil(lootPossibility.amount * multiplier);
                loot.GetComponent<HealItem>().itemAmount = lootAmount;
            }
                
        }
    }

    protected override void Die()
    {
        if (haveAlreadyDied)
            return;

        haveAlreadyDied = true;

        base.Die();
        EnemyDiedEvent.GetInstance().TriggerEnemyDied();
        DropLoot();
        Destroy(gameObject);
    }
}
