﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeHandler : CustomButton
{
    [Tooltip("The sound to play when the sales is accepted")]
    [SerializeField]
    private AudioClip saleAcceptedSound;

    [Header("UI elements' configuration")]
    [Tooltip("The UI game object that represents the skill image")]
    [SerializeField]
    private GameObject skillImage;
    [Tooltip("The UI game object that represents the skill name")]
    [SerializeField]
    private GameObject skillName;
    [Tooltip("The UI game object that represents the skill description")]
    [SerializeField]
    private GameObject description;
    [Tooltip("The UI game object that represents the skill price")]
    [SerializeField]
    private GameObject price;
    [Tooltip("The UI game object that represents the skill's current value")]
    [SerializeField]
    private GameObject currentValue;
    [Tooltip("The UI game object that represents the skill's value if upgraded")]
    [SerializeField]
    private GameObject upgradedValue;
    [Tooltip("The prefab of the inventory slot to use in order to display the price")]
    [SerializeField]
    private GameObject invotorySlotPrefab;


    private Upgrade upgrade;
    private List<GameObject> inventorySlots = new List<GameObject>();

    /**
     * Set the "upgrade" object to this object and set up all the data in the UI elements
     */
    public void SetUpgradeObject(Upgrade upgrade)
    {
        this.upgrade = upgrade;
        UpdateUpgradeInfo();
    }

    /**
     * Update the data in the UI elements according to the current upgrade
     */
    private void UpdateUpgradeInfo()
    {
        skillImage.GetComponent<Image>().sprite = upgrade.SkillImage;
        skillName.GetComponent<Text>().text = upgrade.SkillName;
        description.GetComponent<Text>().text = upgrade.Description;
        currentValue.GetComponent<Text>().text = upgrade.Values[upgrade.CurrentUpgradeLevel].ToString();
        if (upgrade.HasReachedMaxLevel())
            upgradedValue.GetComponent<Text>().text = upgrade.Values[upgrade.CurrentUpgradeLevel].ToString();
        else
            upgradedValue.GetComponent<Text>().text = upgrade.Values[upgrade.CurrentUpgradeLevel + 1].ToString();
        BuildPrice();
    }

    /**
     * Destroy all the inventory slot in the price UI element and clear the list of inventory slot.
     */
    private void CleanInventorySlots()
    {
        foreach (GameObject slot in inventorySlots)
        {
            Destroy(slot);
        }
        inventorySlots.Clear();
    }

    /**
     * Erase the previous price data and fill it accordingly to the price data of the current upgrade
     */
    private void BuildPrice()
    {
        CleanInventorySlots();
        if(!upgrade.HasReachedMaxLevel())
        {
            for (int i = 0; i < upgrade.Prices[upgrade.CurrentUpgradeLevel].materials.Length; i++)
            {
                Material material = upgrade.Prices[upgrade.CurrentUpgradeLevel].materials[i];
                int amount = upgrade.Prices[upgrade.CurrentUpgradeLevel].materialAmounts[i];
                GameObject inventorySLot = Instantiate<GameObject>(invotorySlotPrefab, price.transform);
                inventorySLot.GetComponentInChildren<Image>().sprite = material.Image;
                inventorySLot.GetComponentInChildren<Text>().text = amount.ToString();
                inventorySlots.Add(inventorySLot);
            }
        }
        
    }

    /**
     * This is called when the player press the button to purchase this upgrade
     */
    public void BuyUpgrade()
    {
        if (upgrade.Buy())
        {
            UpdateUpgradeInfo();
            PlaySound(saleAcceptedSound);
        }
        else
        {
            PlayClickSound();
        }
    }
}
