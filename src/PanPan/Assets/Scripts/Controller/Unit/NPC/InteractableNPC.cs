﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;

[RequireComponent(typeof(CircleCollider2D))]
public class InteractableNPC : MonoBehaviour
{
    [Tooltip("The game object that shows that the interaction is possible")]
    [SerializeField]
    private GameObject interactionInfo;

    [Header("Interaction's info")]
    [Tooltip("The dialogue to display.")]
    [SerializeField]
    private Discussion dialogue;


    private bool isPlayerInRange = false;
    private Hero player;
    private SteadyDialogueHandler dialogueHandler;

    public SteadyDialogueHandler DialogueHandler { get => dialogueHandler;}

    public virtual void Start()
    {
        interactionInfo.GetComponent<FloatingText>().Speaker = gameObject;

        dialogueHandler = SteadyDialogueHandler.Instance.GetComponent<SteadyDialogueHandler>();

        dialogue.Speakers = GetSpeakers();
        Hero.Player().GetComponent<Hero>().PlayerActionMap.FindAction("Interact").started += TriggerInteraction;
    }

    /**
     * Return the list of speaker for this discussion. The first element is the PNJ gameobject that is parent of this script
     * and the second one is the player;
     */
    protected List<GameObject> GetSpeakers()
    {
        List<GameObject> speakers = new List<GameObject>(2);
        speakers.Add(transform.parent.gameObject);
        speakers.Add(Hero.Player());
        return speakers;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            interactionInfo.SetActive(true);
            isPlayerInRange = true;
        }
            
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            interactionInfo.SetActive(false);
            isPlayerInRange = false;
        }
            
    }

    /**
     * The interaction is triggered by the player
     */
    public void TriggerInteraction(InputAction.CallbackContext context)
    {
        
        if (context.phase is InputActionPhase.Started && isPlayerInRange)
        {
            HandleInteraction();
        }

    }

    /**
     * Will handle the interaction that this object is meant to
     */
    public virtual void HandleInteraction()
    {
        DialogueHandler.StartDialogue(dialogue);
    }

}
