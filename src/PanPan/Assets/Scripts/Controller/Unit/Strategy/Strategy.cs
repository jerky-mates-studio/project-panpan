﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Strategy<T>: MonoBehaviour where T : Strategy<T>
{
    public T NextStrategy { get; set; }
    private bool isUnlocked = true;

    /**
     * Return true iif the next strategy should start
     */
    protected abstract bool IsThisStrategyOver();

    /**
     * Return true iif this strategy must be used
     */
    protected abstract bool MustStart();

    /**
     * Called each frame and change of strategy if it is needed
     */
    public virtual T UpdateStrategy()
    {
        if (IsThisStrategyOver() || NextStrategy.MustStart())
            return NextStrategy;
        else
            return (T)this;
    }

    /// <summary>
    /// Lock/Unlock this Strategy if 'isUnlocked' is false/true. A locked Strategy can't be used.
    /// </summary>
    /// <param name="isUnlocked">True iif the Strategy must unlocked, false it it must be locked</param>
    public void SetUnlocked(bool isUnlocked)
    {
        this.isUnlocked = isUnlocked;
    }

    /// <summary>
    /// Check if this Strategy is locked or not
    /// </summary>
    /// <returns>True iif this Strategy is unlocked</returns>
    public bool IsUnlocked()
    {
        return isUnlocked;
    }

}
