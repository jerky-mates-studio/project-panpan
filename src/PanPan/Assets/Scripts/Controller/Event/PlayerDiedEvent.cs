using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDiedEvent
{
    private static PlayerDiedEvent instance;

    public static PlayerDiedEvent GetInstance()
    {
        if (instance == null)
            instance = new PlayerDiedEvent();
        return instance;
    }

    public delegate void PlayerDiedDelegate(Hero player);
    public event PlayerDiedDelegate PlayerDied;

    public void TriggerPlayerDied(Hero player) => PlayerDied?.Invoke(player);
}
