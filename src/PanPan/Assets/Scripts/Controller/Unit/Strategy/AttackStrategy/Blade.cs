﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Blade : AttackStrategy
{

    [Header("Blade settings")]
    [Tooltip("The reference to the blade of the player")]
    [SerializeField]
    private GameObject blade;
    [Tooltip("The minimum between 2 blade attacks in seconds")]
    [SerializeField]
    private float bladeCooldown = 2f;
    private float bladeCooldownTimer = 0;
    // has the strike action began ?
    private bool hasStartStriking = false;
    // is the striking animation over ?
    private bool isStrikingOver = true;
    private bool isStriking = false;

    private void Update()
    {
        bladeCooldownTimer = Math.Max(0, bladeCooldownTimer - Time.deltaTime);
        // tell the animator that he has to speed up in order to not be slowed down
        GetComponent<Animator>().SetFloat("Speed", 1f/GetComponent<Hero>().CurrentTimeStrategy.GetTimeMultiplier());
    }

    public override void Attack()
    {
        if (!IsUnlocked())
            return;

        if (!hasStartStriking)
        {
            bladeCooldownTimer = bladeCooldown;
            hasStartStriking = true;
            isStrikingOver = false;
            blade.SetActive(true);
            GetComponent<Animator>().SetTrigger("Strike");
        }
    }

    protected override bool IsThisStrategyOver()
    {
        if (!IsUnlocked())
            return true;

        bool isOver = hasStartStriking && isStrikingOver;
        if (isOver)
        {
            hasStartStriking = false;
            blade.SetActive(false);

        }
        return isOver;
    }

    protected override bool MustStart()
    {
        if (!IsUnlocked())
            return true;

        return isStriking && bladeCooldownTimer <= 0f;
    }

    /**
     * Is called when the Sword button is pressed
     */
    public void OnUseSword(InputAction.CallbackContext context)
    {
        isStriking = context.ReadValueAsButton();
    }

    /**
     * This is called by the animator when the stike animation is over
     */
    public void EndStrikingStrategy()
    {
        isStrikingOver = true;
    }
}
