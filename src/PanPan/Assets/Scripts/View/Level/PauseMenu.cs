﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using static UnityEngine.InputSystem.InputAction;

public class PauseMenu : MonoBehaviour
{
    public static bool isPaused = false;

    [Tooltip("The HUD game object to disable on game pause. Just let it blank if there is no HUD in this scene.")]
    [SerializeField]
    private GameObject HUD = null;
    private InputActionMap UIActionMap;
    private InputActionMap playerActionMap;

    private void Start()
    {
        playerActionMap = PlayerInput.GetPlayerByIndex(0).actions.actionMaps[0];
        playerActionMap.FindAction("Pause").started += Pause;

        UIActionMap = PlayerInput.GetPlayerByIndex(0).actions.actionMaps[1];
        UIActionMap.FindAction("Resume").started += Resume;

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Unbind the controls to the action maps in order to avoid action triggered by destroyed objects 
    /// </summary>
    private void OnDestroy()
    {
        playerActionMap.FindAction("Pause").started -= Pause;
        UIActionMap.FindAction("Resume").started -= Resume;
    }

    /**
     * Is called when the game is paused
     */
    private void Pause(CallbackContext ctx)
    {
        if (!isPaused)
            gameObject.SetActive(true);
    }
    
    /// <summary>
    /// End the pause of the game
    /// </summary>
    /// <param name="ctx">The context of the callback</param>
    private void Resume(CallbackContext ctx)
    {
        if (isPaused)
            gameObject.SetActive(false);
    }

    /// <summary>
    /// It's meant to be called when the "Save" button is pushed. It saves all the game data.
    /// </summary>
    public void OnSave()
    {
        SaveSystem.Save();
    }

    /// <summary>
    /// Quit the application without saving
    /// </summary>
    public void OnExit()
    {
        Application.Quit();
    }

    private void OnEnable()
    {
        if(Hero.Player() != null)
            Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("UI");
        if (HUD != null)
            HUD.SetActive(false);
        Time.timeScale = 0f;
        isPaused = true;
    }

    private void OnDisable()
    {
        if (Hero.Player() != null)
            Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("Player");
        if (HUD != null)
            HUD.SetActive(true);
        Time.timeScale = 1f;
        isPaused = false;
    }
}
