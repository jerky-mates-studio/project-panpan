﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{

    [Header("Sound parameters")]
    [Tooltip("The lowest volume the pitch can be played")]
    public float lowPitchRange = .95f;                //The lowest a sound effect will be randomly pitched.
    [Tooltip("The highest volume the pitch can be played")]
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.
    [Tooltip("The volume to play the audio clip")]
    [SerializeField]
    [Range(0f, 1f)]
    private float volume = 0.5f;

    /// <summary>
    /// This method plays the "clip" by slightly changing its pitch.
    /// This allows the clip sounds less repetitive.
    /// If a clip is already playing, it is replaced by the new 'clip'
    /// </summary>
    /// <param name="clip">The new clip to play</param>
    public void RandomizePitch(AudioClip clip)
    {
        RandomizePitch(clip, this.volume);
    }

    /// <summary>
    /// This method plays the "clip" by slightly changing its pitch.
    /// This allows the clip sounds less repetitive.
    /// If a clip is already playing, it is replaced by the new 'clip'
    /// </summary>
    /// <param name="clip">The new clip to play</param>
    /// <param name="volume">The volume to play this clip at</param>
    public void RandomizePitch(AudioClip clip, float volume)
    {
        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Game/PortableAudioSource"));
        go.GetComponent<PortableAudioSource>().toFollow = gameObject;
        AudioSource audioSource = go.GetComponent<AudioSource>();

        audioSource.clip = clip;
        audioSource.volume = volume;
        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);
        //Set the pitch of the audio source to the randomly chosen pitch.
        audioSource.pitch = randomPitch;
    }
}