﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonSelector : MonoBehaviour
{
    [Tooltip("The button to select by default when the panel is enabled")]
    [SerializeField]
    private GameObject defaultSelectedButton;
    [Tooltip("Check this box if the first enable call must be ignore")]
    [SerializeField]
    private bool ignoreFirstCall = false;

    private void OnEnable()
    {
        if (!ignoreFirstCall)
            EventSystem.current.SetSelectedGameObject(defaultSelectedButton);
        else
            ignoreFirstCall = false;
    }
}
