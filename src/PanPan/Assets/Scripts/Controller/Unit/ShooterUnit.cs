﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public abstract class ShooterUnit : NonShooterUnit
{

    [Header("Bullet features")]
    [Tooltip("The bullet game object that this unit can shoot")]
    public GameObject bullet;
    [Tooltip("The bullet type that this unit can shoot")]
    [SerializeField]
    private BulletType bulletType;
    [Tooltip("The original position of the bullet")]
    [SerializeField]
    private Vector3 bulletOrigin;

    [Header("Body parts")]
    [Tooltip("The game object that represents the body of the Unit")]
    [SerializeField]
    private GameObject upperBody;
    [Tooltip("The game object that represents the legs of the Unit")]
    [SerializeField]
    private GameObject legs;
    private Quaternion initialRotation;

    public float timer { get; set; }
    public BulletType BulletType { get => bulletType;}
    public GameObject UpperBody { get => upperBody; }
    public GameObject Legs { get => legs; }

    virtual protected void Awake()
    {
        timer = bullet.GetComponent<Bullet>().timeBetweenBullet;
        initialRotation = legs.transform.localRotation;
    }

    /**
     * Initialize a specified type of strategy
     */
    protected void InitialiazeStrategy<S>(S defaultStrategy, S otherStrategy, out S strategyVariable) where S : Strategy<S>
    {
        defaultStrategy.NextStrategy = otherStrategy;
        otherStrategy.NextStrategy = defaultStrategy;
        strategyVariable = defaultStrategy;
    }

    virtual protected void Update()
    {
        timer += Time.deltaTime;
    }

    /*
     * Shoot a bullet if it doesn't exceed the shoot rate
     */
    public virtual void Fire()
    {
        if (timer >= bullet.GetComponent<Bullet>().timeBetweenBullet)
        {
            timer = timer % bullet.GetComponent<Bullet>().timeBetweenBullet;
            CreateBullet();
        }
    }

    /**
     * Create a bullet according to the "bullet" prefab
     */
    public virtual GameObject CreateBullet()
    {
        Quaternion bulletRotation = bullet.GetComponent<Bullet>().GetBulletRotation(transform.rotation);
        Vector3 bulletPosition = getBulletInitialPosition();
        GameObject currentBullet = Instantiate(bullet, bulletPosition, bulletRotation);
        currentBullet.GetComponent<Bullet>().SetBulletType(bulletType);
        return currentBullet;
    }

    /// <summary>
    /// Compute the original position of the bullet according to the upper body transform 
    /// </summary>
    /// <returns>Vector3 the original position of the bullet</returns>
    public Vector3 getBulletInitialPosition()
    {
        return transform.position + upperBody.transform.TransformVector(bulletOrigin);
    }

    /// <summary>
    /// Rotate the legs so that they are always pointing towards the movement direction.
    /// If the player don't move, the initial local rotation is used
    /// </summary>
    public void RotateLegs(Vector2 playerMove)
    {
        if (playerMove.Equals(new Vector2(0, 0)))
        {
            legs.transform.localRotation = initialRotation;
        }
        else
        {
            LookAt(legs, legs.transform.position + new Vector3(playerMove.x, playerMove.y));
        }
        GetComponent<Animator>().SetFloat("MovementSpeed", Mathf.Abs(playerMove.x) + Mathf.Abs(playerMove.y));
    }

    /// <summary>
    /// Make a game object looks toward a point
    /// </summary>
    /// <param name="legsObject">The game object to rotate</param>
    /// <param name="toLook">The point the game object must look at</param>
    protected virtual void LookAt(GameObject legsObject, Vector3 toLook)
    {
        // Get the vector that goes from "toLook" to the unity's position
        Vector3 difference = toLook - legsObject.transform.position;
        difference.Normalize();
        float angle = (Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg) + initialRotation.eulerAngles.z - legsObject.transform.parent.localRotation.eulerAngles.z;
        legsObject.transform.localRotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
