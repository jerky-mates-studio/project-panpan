﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class CreditsScroller : MonoBehaviour
{
    [Tooltip("The scroll speed in unit/second")]
    public float scrollingSpeed = 80;

    private Vector3 textStartPosition;
    private float finishScrollHeight;
    private PlayerController control;

    private void Awake()
    {
        //Set the credit text to the text child
        Text textChild = GetComponentInChildren<Text>();
        textChild.text = Resources.Load<TextAsset>("Credits").text;
        textStartPosition = transform.GetChild(0).position;

        TextGenerator textGen = new TextGenerator();
        TextGenerationSettings generationSettings = textChild.GetGenerationSettings(textChild.rectTransform.rect.size);

        float textHeight = textGen.GetPreferredHeight(textChild.text, generationSettings);
        finishScrollHeight = textHeight - textStartPosition.y + Screen.height;

        control = new PlayerController();
        control.UI.Cancel.performed +=
            ctx =>
            {
                if (ctx.interaction is PressInteraction)
                {
                    OnCreditsEnd();
                }
            };

    }

    /// <summary>
    /// The method that handles what happen when the credits end
    /// </summary>
    private void OnCreditsEnd()
    {
        SceneManager.LoadScene("AbandonnedLevel");
    }


    private void OnDisable()
    {
        transform.GetChild(0).position = textStartPosition;
        control.Disable();
    }

    private void OnEnable()
    {
        control.Enable();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.GetChild(0).position.y < finishScrollHeight)
        {

            float distance = scrollingSpeed * Time.deltaTime;
            if (control.UI.Submit.ReadValue<float>() > 0)
                distance *= 2;
            transform.GetChild(0).position += new Vector3(0, distance, 0);
        }
        else
            OnCreditsEnd();
    }
}
