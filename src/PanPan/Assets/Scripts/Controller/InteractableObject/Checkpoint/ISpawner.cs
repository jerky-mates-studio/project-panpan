using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawner
{
    /// <summary>
    /// Make spawn the unit at the center of the spawner
    /// </summary>
    /// <param name="unit">The unit to spawn</param>
    void Spawn(ShooterUnit unit);

    /// <summary>
    /// Mark the spanwer as activated, and thus make it capable of spawning units
    /// </summary>
    void OnSpawnerActivated();

    /// <summary>
    /// Mark the spanwer as deactivated, and thus make it unable of spawning units
    /// </summary>
    void OnSpawnerDeactivated();
}
