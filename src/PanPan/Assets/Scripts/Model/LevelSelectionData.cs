﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "Model/LevelData")]
[HelpURL("https://stackoverflow.com/questions/32306704/how-to-pass-data-between-scenes-in-unity")]
public class LevelSelectionData : ScriptableObject
{
    public string levelName;
}
