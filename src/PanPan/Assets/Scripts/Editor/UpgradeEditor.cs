﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Upgrade))]
public class UpgradeEditor : Editor
{
    private int minValuesLength = 2;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        // Ensure there at least 2 elements in the "Values" list
        // (only one element doesn't make sens)
        SerializedProperty valuesList = serializedObject.FindProperty("values");
        if (valuesList.arraySize < minValuesLength)
            valuesList.arraySize = minValuesLength;

        // Ensure the size of "Prices" list is always equal to size of "Values" list -1
        SerializedProperty pricesList = serializedObject.FindProperty("prices");
        pricesList.arraySize = valuesList.arraySize - 1;

        // Ensure for all the prices that "materials" and "materialAmount" have the same size; 
        for (int i=0; i<pricesList.arraySize; i++)
        {
            SerializedProperty price = pricesList.GetArrayElementAtIndex(i);
            price.FindPropertyRelative("materialAmounts").arraySize = price.FindPropertyRelative("materials").arraySize;
        }

        //Apply modifications
        serializedObject.ApplyModifiedProperties();
    }
}
