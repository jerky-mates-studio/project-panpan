﻿    using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class TestUnit
{
    /**
     * Return a "RiffleBullet" which has a 0 maximum spead
     */
    private GameObject GetBulletTestInstance()
    {
        GameObject bullet = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Bullet/RiffleBullet"));
        // ensure the bullet go right where we want to
        bullet.GetComponent<Bullet>().maximumSpread = 0;
        return bullet;
    }

    /**
     * Return a "Enemy_1" that drops no loot
     */
    private GameObject GetEnemyTestInstance()
    {
        GameObject enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Enemy_1"));
        // prevent the enemy to drop loot during the tests
        enemy.GetComponent<Enemy>().possibleLoots = new List<Enemy.LootPossibility>(0);
        return enemy;
    }

    // 1
    [UnityTest]
    /**
     * Test if an Enemy always look at the player
     */
    public IEnumerator EnemyLookAt()
    {
        // 2
        GameObject player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));
        GameObject enemy = GetEnemyTestInstance();

        enemy.transform.position = new Vector3(0, 0, 0);
        Vector3 initial_rotation = enemy.transform.eulerAngles;
        player.transform.position = new Vector3(1, 1, 0);
        // 5
        // We have to wait at least 1s because of the reaction time of the enemy
        yield return new WaitForSeconds(1f);
        // 6
        Assert.AreNotEqual(enemy.transform.eulerAngles.z, initial_rotation.z);
        // The "z" and the value "45" must be the same withe a maximal error of "0.01f"
        Assert.AreEqual(enemy.transform.eulerAngles.z, 45, 0.01f);
        // 7
        Object.Destroy(enemy);
        Object.Destroy(player);
    }

    [UnityTest]
    /**
     * Test if a bullet is destroyer if it reach is maximum range
     */
    public IEnumerator BulletTooLong()
    {
        // 2
        GameObject bullet = GetBulletTestInstance();

        bullet.transform.position = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(5.2f);
        // 6
        Assert.IsTrue(bullet == null);
        // 7
        Object.Destroy(bullet);
    }

    [UnityTest]
    /**
     * Test if an Enemy bullet can touch the player and if it (the bullet) is destroyed after
     */ 
    public IEnumerator BulletTouchUnit()
    {
        // 2
        GameObject bullet = GetBulletTestInstance();
        GameObject player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));

        player.transform.position = new Vector3(10, 0, 0);
        bullet.transform.position = new Vector3(0, 0, 0);
        bullet.GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Enemy"));
        yield return new WaitForSeconds(1f);
        // 6
        Assert.IsTrue(bullet == null);
        // 7
        Object.Destroy(bullet);
        Object.Destroy(player);
    }

    [UnityTest]
    /**
     * Test if a player bullet can't touch the player
     */ 
    public IEnumerator BulletDontTouchUnit()
    {
        // 2
        GameObject bullet = GetBulletTestInstance();
        GameObject player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));

        player.transform.position = new Vector3(10, 0, 0);
        bullet.transform.position = new Vector3(0, 0, 0);
        bullet.GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Player"));
        yield return new WaitForSeconds(1f);
        // 6
        Assert.IsTrue(bullet != null);
        // 7
        Object.Destroy(bullet);
        Object.Destroy(player);
    }

    [UnityTest]
    /**
     * Test if a player bullet can kill the player
     */
    public IEnumerator BulletKillPlayer()
    {
        // 2
        GameObject player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));
        player.transform.position = new Vector3(10, 0, 0);

        int bulletNumber = 5;
        GameObject[] bullets = new GameObject[bulletNumber];
        // Instantiate enough bullet to kill the player
        for(int i=0; i<bulletNumber; i++)
        {
            GameObject bullet = GetBulletTestInstance();
            bullet.transform.position = new Vector3(0, 0, 0);
            bullet.GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Enemy"));
        }
        yield return new WaitForSeconds(1f);
        // 6
        Assert.IsTrue(player.GetComponent<NonShooterUnit>().IsDead());
        // 7
        foreach(GameObject bullet in bullets){
            Object.Destroy(bullet);
        }
        Object.Destroy(player);
    }

    [UnityTest]
    /**
     * Test if a player bullet can kill the enemy
     */
    public IEnumerator BulletKillEnemy()
    {
        // 2
        GameObject player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));
        GameObject enemy = GetEnemyTestInstance();
        enemy.transform.position = new Vector3(10, 0, 0);

        int bulletNumber = 5;
        GameObject[] bullets = new GameObject[bulletNumber];
        // Instantiate enough bullet to kill the enemy
        for (int i = 0; i < bulletNumber; i++)
        {
            GameObject bullet = GetBulletTestInstance();
            bullet.transform.position = new Vector3(0, 0, 0);
            bullet.GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Player"));
        }
        yield return new WaitForSeconds(1f);
        // 6
        Assert.IsTrue(enemy == null);
        // 7
        foreach (GameObject bullet in bullets)
        {
            Object.Destroy(bullet);
        }
        Object.Destroy(enemy);
        Object.Destroy(player);

    }

    [UnityTest]
    /**
     * Test if a player has not reached yet the portal
     */
    public IEnumerator PlayerHasNotReachedPortal()
    {
        // 2
        GameObject portal = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Portal"));
        portal.transform.position = new Vector3(10, 0, 0);
        GameObject player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));

        yield return new WaitForSeconds(1f);
        // 6
        Assert.IsFalse(player.GetComponent<Hero>().HasReachedPortal());

        Object.Destroy(portal);
        Object.Destroy(player);

    }

    [UnityTest]
    /**
     * Test if a player not reached the portal
     */
    public IEnumerator PlayerHasReachedPortal()
    {
        // 2
        GameObject portal = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Portal"));
        GameObject player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));

        yield return new WaitForSeconds(1f);
        // 6
        Assert.IsTrue(player.GetComponent<Hero>().HasReachedPortal());

        Object.Destroy(portal);
        Object.Destroy(player);

    }

    [UnityTest]
    /**
     * Test if a player not reached the portal
     */
    public IEnumerator EnemySeePlayerTroughLoot()
    {
        // 2
        GameObject loot = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Loot/Food"));
        loot.transform.position = new Vector3(5, 0, 0);
        GameObject player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));
        GameObject enemy = GetEnemyTestInstance();
        enemy.transform.position = new Vector3(10, 0, 0);

        yield return new WaitForSeconds(1f);
        // 6
        Assert.IsTrue(enemy.GetComponent<EniacAI>().FightState.isPlayerOnSight());

        Object.Destroy(loot);
        Object.Destroy(player);
        Object.Destroy(enemy);

    }
}

