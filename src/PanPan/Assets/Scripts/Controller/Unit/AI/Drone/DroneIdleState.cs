using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneIdleState : AIState
{
    private DroneAI Context { get; set; }

    public DroneIdleState(DroneAI context) : base(context)
    {
        Context = context;
    }

    public override void OnEnterState()
    {
        return;
    }

    public override void UpdateState()
    {
        if (isPlayerOnSight())
        {
            Context.SwitchState(Context.FightState);
        }
    }

}
