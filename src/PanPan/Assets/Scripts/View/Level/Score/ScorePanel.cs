using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ScorePanel : MonoBehaviour
{
    [Header("Score lines")]
    [Tooltip("The score line that shows the level completion time")]
    [SerializeField]
    private NumericScoreLine timeScoreLine;
    [Tooltip("The score line that shows the number of kills")]
    [SerializeField]
    private NumericScoreLine killsScoreLine;
    [Tooltip("The score line that shows the number of death")]
    [SerializeField]
    private NumericScoreLine deathScoreLine;
    [Tooltip("The line that shows the rank obtained by the player")]
    [SerializeField]
    private TextScoreLine rankScoreLine;
    [Tooltip("The line that shows the button to quit the score panel")]
    [SerializeField]
    private ButtonScoreLine buttonScoreLine;

    [Header("Score animation parameter")]
    [Tooltip("The time between the display of 2 lines of score (in seconds)")]
    [SerializeField]
    private float timeBetweenLineDisplay = 1f;

    [Header("Closure parameters")]
    [SerializeField]
    [Tooltip("The method to execute when this panel is closed")]
    private UnityEvent afterClosureAction;


    /// <summary>
    /// Assign the right text value to the right score line
    /// </summary>
    public void SetScore(ScoreUI.Score score)
    {
        timeScoreLine.EndValue = (int)Math.Round(score.completionTime);
        killsScoreLine.EndValue = score.enemiesKilled;
        deathScoreLine.EndValue = score.playerDeath;
        rankScoreLine.EndValue = RankToString(score.rank);
    }

    /// <summary>
    /// Compute the string rank to display based on the 'ScoreUI.Rank' enum value
    /// </summary>
    /// <param name="rank">The enum value to translate to string</param>
    /// <returns></returns>
    private string RankToString(ScoreUI.Rank rank)
    {
        switch (rank)
        {
            case ScoreUI.Rank.CapitalDomeCitizen:
                return "Capital Dome Citizen";
            case ScoreUI.Rank.UnionInterventionBrigadesSoldier:
                return "Union Intervention Brigades Soldier";
            case ScoreUI.Rank.UnionInterventionBrigadesCaptain:
                return "Union Intervention Brigades Captain";
            case ScoreUI.Rank.MutantEliteSoldierPrototype:
                return "Mutant Elite Soldier Prototype";
            default:
                return "";
        }
    }

    /// <summary>
    /// Called when the "Close" button is clicked
    /// </summary>
    public void OnCloseButtonClicked()
    {
        StartCoroutine(TriggerCloseAnimation());
    }

    /// <summary>
    /// Trigger the "close" animation of this panel after hiding all the score lines
    /// </summary>
    private IEnumerator TriggerCloseAnimation()
    {
        yield return StartCoroutine(HideScore());
        GetComponent<Animator>().SetTrigger("ClosePanel");
    }

    /// <summary>
    /// Called by the animator when the "open" animation has finished
    /// </summary>
    private void OnPanelOpened()
    {
        StartCoroutine(DisplayScore());
    }

    /// <summary>
    /// Called by the animator when the "close" animation has finished
    /// </summary>
    private void OnPanelClosed()
    {
        afterClosureAction?.Invoke();
    }

    /// <summary>
    /// Start the display of each score line 
    /// </summary>
    /// <returns></returns>
    private IEnumerator DisplayScore()
    {
        // Wait a little time before displaying the first score line 
        yield return new WaitForSeconds(timeBetweenLineDisplay);

        yield return StartCoroutine(timeScoreLine.DisplayLine());
        yield return new WaitForSeconds(timeBetweenLineDisplay);

        yield return StartCoroutine(killsScoreLine.DisplayLine());
        yield return new WaitForSeconds(timeBetweenLineDisplay);

        yield return StartCoroutine(deathScoreLine.DisplayLine());
        yield return new WaitForSeconds(timeBetweenLineDisplay);

        yield return StartCoroutine(rankScoreLine.DisplayLine());
        yield return new WaitForSeconds(timeBetweenLineDisplay);

        yield return StartCoroutine(buttonScoreLine.DisplayLine());
    }

    /// <summary>
    /// Hide all the scores texts and the 'Close' button
    /// </summary>
    /// <returns>An enumerator</returns>
    private IEnumerator HideScore()
    {
        Coroutine timeCoroutine = StartCoroutine(timeScoreLine.HideLine());
        Coroutine killsCoroutine = StartCoroutine(killsScoreLine.HideLine());
        Coroutine deathCoroutine = StartCoroutine(deathScoreLine.HideLine());
        Coroutine rankCoroutine = StartCoroutine(rankScoreLine.HideLine());
        Coroutine buttonCoroutine = StartCoroutine(buttonScoreLine.HideLine());

        yield return timeCoroutine;
        yield return killsCoroutine;
        yield return deathCoroutine;
        yield return rankCoroutine;
        yield return buttonCoroutine;
    }
}
