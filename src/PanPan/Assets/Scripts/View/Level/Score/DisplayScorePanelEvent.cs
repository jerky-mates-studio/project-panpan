using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayScorePanelEvent
{
    private static DisplayScorePanelEvent instance;

    public static DisplayScorePanelEvent GetInstance()
    {
        if (instance == null)
            instance = new DisplayScorePanelEvent();
        return instance;
    }

    public delegate void DisplayScorePanelDelegate(ScoreUI.Score playerScore);
    public event DisplayScorePanelDelegate ScorePanelDisplayed;

    public void TriggerScorePanelDisplay(ScoreUI.Score playerScore) => ScorePanelDisplayed?.Invoke(playerScore);
}
