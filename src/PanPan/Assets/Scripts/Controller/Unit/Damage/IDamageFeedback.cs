using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageFeedback
{
    /// <summary>
    /// Trigger a visual damage feedback in order to notify the player that this unit has taken damages
    /// </summary>
    /// <param name="damages">the number of damages taken by the unit</param>
    void TriggerDamageFeedback(float damages);
}
