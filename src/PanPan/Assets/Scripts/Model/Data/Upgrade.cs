﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Model/Upgrade")]
public class Upgrade : ScriptableObject
{
    /**
     * Return all the existing upgrades
     */
    public static Upgrade[] GetUpgrades()
    {
        return Resources.LoadAll<Upgrade>("Prefabs/Data/Upgrade");
    }

    /**
     * Reset the upgrades as if they was never upgraded
     */
    public static void Reset()
    {
        foreach (Upgrade upgrade in GetUpgrades())
            upgrade.CurrentUpgradeLevel = 0;
    }

    [System.Serializable]
    public struct Price
    {
        public Material[] materials;
        public int[] materialAmounts;
    }


    [Header("Graphical representation")]
    [Tooltip("The name of the skill reprsented")]
    [SerializeField]
    private string skillName;

    [Tooltip("The image that represents the skill")]
    [SerializeField]
    private Sprite skillImage;

    [Tooltip("The description of the skill")]
    [SerializeField]
    [TextArea(3, 10)]
    private string description;

    [Header("Internal logic")]
    [Tooltip("The index that point to the current value in \"values\"")]
    [SerializeField]
    private int currentUpgradeLevel = 0;
    [Tooltip("The different values for this skill")]
    [SerializeField]
    private float[] values;

    [Tooltip("The prices for the different upgrade level")]
    [SerializeField]
    private Price[] prices;

    

    public string SkillName { get => skillName;}
    public Sprite SkillImage { get => skillImage;}
    public string Description { get => description;}
    public float[] Values { get => values;}
    public Price[] Prices { get => prices;}
    public int CurrentUpgradeLevel { get => currentUpgradeLevel; set => currentUpgradeLevel = value; }
    public float CurrentValue { get => Values[CurrentUpgradeLevel];}

    /**
     * Return false iif the player cannot buy this upgrade level. If he can, return true and retrieve materials
     * from his inventory according to the price of the next level of the upgrade.
     */
    public bool Buy()
    {
        bool canBuy = !HasReachedMaxLevel() && HasPlayerEnoughMaterials();
        if (canBuy)
        {
            RetrievePlayerMaterials();
            CurrentUpgradeLevel ++;
        }
        return canBuy;
    }

    /**
     * Substract the materials price for the player inventory
     */
    private void RetrievePlayerMaterials()
    {
        Dictionary<Material, int> playerInventory = Hero.Player().GetComponent<Hero>().Inventory;
        Price currentPrice = Prices[CurrentUpgradeLevel];
        for (int i = 0; i < currentPrice.materials.Length; i++)
        {
            playerInventory[currentPrice.materials[i]] -= currentPrice.materialAmounts[i];
        }
        Hero.Player().GetComponent<Hero>().Inventory = playerInventory;
    }

    /**
     * Return true iif all the maximum level has been reached for this upgrade 
     */
    public bool HasReachedMaxLevel()
    {
        return CurrentUpgradeLevel + 1 == Values.Length;
    }

    /**
     * Check if the player has enough materials to buy the next level of this upgrade
     */
    private bool HasPlayerEnoughMaterials()
    {
        Dictionary<Material, int> playerInventory = Hero.Player().GetComponent<Hero>().Inventory;
        Price currentPrice = Prices[CurrentUpgradeLevel];
        for (int i=0; i<currentPrice.materials.Length; i++)
        {
            if (playerInventory[currentPrice.materials[i]] < currentPrice.materialAmounts[i])
                return false;
        }
        return true;
    }
}
