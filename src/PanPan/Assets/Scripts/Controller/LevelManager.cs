using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private void OnEnable()
    {
        StartNextLevelEvent.GetInstance().NextLevelStarted += OnStartNextLevel;
    }

    private void OnDisable()
    {
        StartNextLevelEvent.GetInstance().NextLevelStarted -= OnStartNextLevel;
    }

    private void OnStartNextLevel()
    {
        SceneManager.LoadScene("Credits");
    }
}
