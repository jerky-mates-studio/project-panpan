using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MultipleSwitchLoadedCondition: ActivationCondition
{
    public bool IsActivated { get; protected set; }

    public enum MultipleSwitchConditionState
    {
        ListenningSwitches,
        ResettingSwitches
    }

    public MultipleSwitchConditionState State { get; private set; } = MultipleSwitchConditionState.ListenningSwitches;

    /// <summary>
    /// Return true iif all switches in 'loadSwitches' are activated
    /// </summary>
    /// <returns>True iif all switches are activated</returns>
    protected bool AreAllSwitchesActivated()
    {
        foreach (GameObject loadSwitch in GetLoadSwitches())
        {
            LoadSwitch switchObject = loadSwitch.GetComponent<LoadSwitch>();

            if (!switchObject.IsActivated())
                return false;
        }
        return true;
    }

    /// <summary>
    /// Return true iif all switches in 'loadSwitches' are deactivated
    /// </summary>
    /// <returns>True iif all switches are deactivated</returns>
    protected bool AreAllSwitchesDeactivated()
    {
        foreach (GameObject loadSwitch in GetLoadSwitches())
        {
            LoadSwitch switchObject = loadSwitch.GetComponent<LoadSwitch>();

            if (switchObject.IsActivated())
                return false;
        }
        return true;
    }

    protected abstract GameObject[] GetLoadSwitches();

    /// <summary>
    /// Check if all switch game objects have a 'LoadSwitch' script. Print an error message if it is not the case. 
    /// </summary>
    protected void AreLoadSwitchesValid()
    {
        foreach (GameObject loadSwitch in GetLoadSwitches())
        {
            LoadSwitch switchObject = loadSwitch.GetComponent<LoadSwitch>();
            if (switchObject == null)
                Debug.LogError("One game object in the 'loadSwitches' has no 'LoadSwitch' component");
        }
    }

    /// <summary>
    /// Deactivate all the switches in 'loadSwitches'.
    /// If this object switch from 'activated' to 'deactivated', triggers an avent to notify the state changement. 
    /// </summary>
    protected void ResetAllSwicthes()
    {
        State = MultipleSwitchConditionState.ResettingSwitches;
        foreach (GameObject loadSwitch in GetLoadSwitches())
        {
            LoadSwitch switchObject = loadSwitch.GetComponent<LoadSwitch>();
            switchObject.ResetLoad();
        }
        State = MultipleSwitchConditionState.ListenningSwitches;

        // Check if this object switch from 'activated' to 'deactivated'
        if (IsActivated)
            TriggerActivation(false);

        IsActivated = false;
    }

    /// <summary>
    /// Activate all the switches in 'loadSwitches'.
    /// If this object switch from 'deactivated' to 'activated', triggers an avent to notify the state changement.
    /// </summary>
    protected void FillAllSwitches()
    {
        State = MultipleSwitchConditionState.ResettingSwitches;
        foreach (GameObject loadSwitch in GetLoadSwitches())
        {
            LoadSwitch switchObject = loadSwitch.GetComponent<LoadSwitch>();
            switchObject.FillLoad();
        }
        State = MultipleSwitchConditionState.ListenningSwitches;

        // Check if this object switch from 'activated' to 'deactivated'
        if (!IsActivated)
            TriggerActivation(true);

        IsActivated = true;
    }
}
