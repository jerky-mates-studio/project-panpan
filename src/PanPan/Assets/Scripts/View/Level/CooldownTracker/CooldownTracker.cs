﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CooldownTracker : MonoBehaviour
{
    /**
     * Return the total cooldown of the effect the parent image represents
     */
    public abstract float GetTotalCooldown();

    /**
     * Return the elapsed time since the last ability call
     */
    public abstract float GetElapsedCooldown();

    private void Update()
    {
        float cooldownPercentage = GetElapsedCooldown() / GetTotalCooldown();
        float heightPercentage = transform.GetComponent<RectTransform>().rect.height * cooldownPercentage;
        transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, heightPercentage, 0);
    }
}
