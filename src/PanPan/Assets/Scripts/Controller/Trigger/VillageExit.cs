﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillageExit : MonoBehaviour
{
    [Tooltip("The level selection game object that allows to choose the level")]
    [SerializeField]
    private GameObject levelSelectionPanel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            levelSelectionPanel.SetActive(true);
    }
}
