﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelLoader : MonoBehaviour
{
    [Tooltip("The object that passes data between this scene and the level scene")]
    public LevelSelectionData levelData;

    void Awake()
    {
        
        string mapPrefabPath = "Maps" + Path.DirectorySeparatorChar + levelData.levelName;
        MonoBehaviour.Instantiate(Resources.Load<GameObject>(mapPrefabPath));
    }

    private void Start()
    {
        SaveSystem.Load();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
