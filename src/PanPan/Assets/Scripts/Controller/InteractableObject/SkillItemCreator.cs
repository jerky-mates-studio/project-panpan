using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillItemCreator : MonoBehaviour
{
    [Tooltip("The skill item to create")]
    [SerializeField]
    private GameObject itemToCreate;
    [Tooltip("The skill item data that represents the skill item to create")]
    [SerializeField]
    private SkillItemData skillItemData;

    private void Start()
    {
        SkillItem skillItem = itemToCreate.GetComponent<SkillItem>();
        if (skillItem == null)
            Debug.LogError("SkillItemCreator::Start : the item to create is not a 'SkillItem'");
    }

    /// <summary>
    /// Create the new skill item and assign it the related skill data 
    /// </summary>
    public void CreateItem()
    {
        GameObject skillObject = Instantiate(itemToCreate, gameObject.transform);
        SkillItem skillItem = skillObject.GetComponent<SkillItem>();
        skillItem.SkillData = skillItemData;
    }
}
