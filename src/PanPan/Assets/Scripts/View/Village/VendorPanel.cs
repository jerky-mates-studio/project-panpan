﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using static UnityEngine.InputSystem.InputAction;

public class VendorPanel : MonoBehaviour
{

    private static GameObject instance;
    public static GameObject Instance { get => instance; }
    public GameObject UpgradeUIPrefab { get => upgradeUIPrefab;}
    public GameObject UpgradesParent { get => upgradesParent;}

    [Tooltip("The prefab of the Upgrade's UI representation")]
    [SerializeField]
    private GameObject upgradeUIPrefab;
    [Tooltip("The game object that will adopt all the UI elements created")]
    [SerializeField]
    private GameObject upgradesParent;

    private List<GameObject> upgradesUI = new List<GameObject>();
    private InputActionMap UIActionMap;

    public event EventHandler OnSaleEnd;
    

    private void Awake()
    {
        if (instance == null)
            instance = gameObject;
    }

    private void Start()
    {
        UIActionMap = Hero.Player().GetComponent<Hero>().UIActionMap;
        UIActionMap.FindAction("Cancel").started += OnCancel;
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Unbind the controls to the action maps in order to avoid action triggered by destroyed objects 
    /// </summary>
    private void OnDestroy()
    {
        UIActionMap.FindAction("Cancel").started -= OnCancel;
    }

    /**
     * Is called when the player hit the Cncel button
     */
    private void OnCancel(CallbackContext ctx)
    {
        if (enabled && Hero.Player().GetComponent<PlayerInput>().currentActionMap.name == "UI")
        {
            gameObject.SetActive(false);
            ClearUpgrades();
            OnSaleEnd?.Invoke(this, EventArgs.Empty);
        }
    }

    private void OnEnable()
    {
        if (Hero.Player() != null)
            Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("UI");
    }

    private void OnDisable()
    {
        if (Hero.Player() != null)
            Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("Player");
    }

    /**
     * Set up all the UI elements in order to display the sale properly
     */
    public void StartSale(List<Upgrade> upgrades)
    {
        gameObject.SetActive(true);
        bool isFirst = true;
        foreach(Upgrade upgrade in upgrades)
        {
            GameObject upgradeUI = Instantiate<GameObject>(upgradeUIPrefab, upgradesParent.transform);
            upgradeUI.GetComponent<UpgradeHandler>().SetUpgradeObject(upgrade);
            upgradesUI.Add(upgradeUI);
            if (isFirst)
            {
                isFirst = false;
                EventSystem.current.SetSelectedGameObject(upgradeUI);
            }
        }
    }

    /**
     * Clear all the UI elements to start on clean bases for the next sale
     */
    private void ClearUpgrades()
    {
        foreach (GameObject upgrade in upgradesUI)
        {
            Destroy(upgrade);
        }
        upgradesUI.Clear();
    }
}
