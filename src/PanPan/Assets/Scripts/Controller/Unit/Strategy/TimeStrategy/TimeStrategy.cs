﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TimeStrategy : Strategy<TimeStrategy>
{
    /**
     * Update the time according to the current strategy
     */
    public abstract void UpdateTime();

    /**
     * Provide the delta time between "Update" calls according to the current time strategy
     */
    public abstract float GetDeltaTime();

    /**
     * Provide the fixed delta time between "FixedUpdate" calls according to the current time strategy
     */
    public abstract float GetFixedDeltaTime();

    /**
     * Return the current multiplier that affect the time according to the current time strategy
     */
    public abstract float GetTimeMultiplier();
}
