using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivableObject : MonoBehaviour
{
    [Header("Activation parameters")]
    [SerializeField]
    [Tooltip("The GameObject that represents the activation condition")]
    private GameObject condition;
    [SerializeField]
    [Tooltip("The method to execute when the condition is met")]
    private UnityEvent activationAction;
    [SerializeField]
    [Tooltip("The method to execute when the condition is not met anymore (deactivation of the object")]
    private UnityEvent deactivationAction;

    private void OnEnable()
    {
        // We subscribe/unsubscribe in OnEnbale/OnDisbale to be sure to avoid memory leaks and bugs
        condition.GetComponent<ActivationCondition>().OnConditionMet += TriggerActivation;
    }

    private void OnDisable()
    {
        // We subscribe/unsubscribe in OnEnbale/OnDisbale to be sure to avoid memory leaks and bugs
        condition.GetComponent<ActivationCondition>().OnConditionMet -= TriggerActivation;
    }

    /// <summary>
    /// Execute the activation method "activationAction" passed as parameter in the inspector
    /// </summary>
    /// <param name="isActivated">True iif the object must be activated, false if the object must be deactivated</param>
    private void TriggerActivation(bool isActivated)
    {
        if (isActivated)
            activationAction?.Invoke();
        else
            deactivationAction?.Invoke();
    }
}
