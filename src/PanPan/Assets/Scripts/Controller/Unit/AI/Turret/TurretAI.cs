using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Turret))]
public class TurretAI : AI
{
    [Header("Shooting parameters")]
    [Tooltip("The time during the turret aim the player")]
    [SerializeField]
    private float aimTime = 1f;
    [Tooltip("The duration of the pause before and after a laser salve")]
    [SerializeField]
    private float pauseTime = 1f;
    [Tooltip("The number of lasers to shoot in 1 salve")]
    [SerializeField]
    private int lasersInSalve = 1;

    public float AimTime { get => aimTime; }
    public float PauseTime { get => pauseTime;  }
    public int LasersInSalve { get => lasersInSalve; }

    private TurretIdleState idleState;
    public TurretIdleState IdleState { get => idleState; }
    private TurretAimState aimState;
    public TurretAimState AimState { get => aimState; }
    private TurretShootState fightState;
    public TurretShootState ShootState { get => fightState; }
    private TurretAimingLaser laser;
    public TurretAimingLaser Laser { get => laser; }


    protected override void Start()
    {
        base.Start();

        laser = GetComponentInChildren<TurretAimingLaser>();

        idleState = new TurretIdleState(this);
        aimState = new TurretAimState(this);
        fightState = new TurretShootState(this);

        CurrentAIState = idleState;
        CurrentAIState.OnEnterState();
    }
}
