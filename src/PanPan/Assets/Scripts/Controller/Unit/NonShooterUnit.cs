﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// force the gameobject to have a SoundManager component 
[RequireComponent(typeof(SoundManager))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class NonShooterUnit : MonoBehaviour, IShootable, IExplosable
{
    [Header("Dialogue feature")]
    [Tooltip("The name of the unit to display in the dialogues (don't have to be unique)")]
    [SerializeField]
    private string unitName;

    [Header("Sound features")]
    [Tooltip("The sound to play when the unit die")]
    public AudioClip dieSound;
    [Tooltip("The sounds to play when the unit is hit (one is randomly chosen at each hit")]
    public AudioClip[] hitSounds;

    [Header("Unit features")]
    [Tooltip("The maximum amount of health points")]
    public float maximumHP = 100;
    [Tooltip("The current amount of health points. This value should be equal to \"maximumHP\" before the game is launch")]
    public float currentHP = 100;

    public string UnitName { get => unitName; set => unitName = value; }

    /// <summary>
    /// Make the cuurrent game object looks toward a point
    /// </summary>
    /// <param name="toLook">The point the game object must look at</param>
    public virtual void LookAt(Vector3 toLook)
    {

        // Get the vector that goes from "toLook" to the unity's position
        Vector3 difference = toLook - transform.position;
        difference.Normalize();
        float angle = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }

    /// <summary>
    /// Fill the HP of this unit at maximum
    /// </summary>
    public virtual void MakeFullLife()
    {
        currentHP = maximumHP;
    }


    /// <summary>
    /// Handles when the unit takes non shooting damage
    /// </summary>
    /// <param name="damage">The amount of damages to receive</param>
    public virtual void OnBeingDamaged(float damage)
    {
        TakeDamage(damage);
    }

    /// <summary>
    /// Return true iif this unit is dashing
    /// </summary>
    /// <returns>true iif this unit is dashing</returns>
    public virtual bool IsDashing()
    {
        return false;
    }

    /// <summary>
    /// Handles the behaviour when this unit take some damages
    /// </summary>
    /// <param name="damage">The amount of damages to receive</param>
    protected virtual void TakeDamage(float damage)
    {
        currentHP -= damage;
        if (IsDead())
            Die();
        else
        {
            // Play a random hit sound
            AudioClip randomSound = hitSounds[Random.Range(0, hitSounds.Length)];
            GetComponent<SoundManager>().RandomizePitch(randomSound);
        }
    }

    /*
     * Comes from the IShootable interface
     */
    public virtual void OnBeingShoot(Bullet bullet)
    {
        TakeDamage(bullet.Damage);
    }

    /**
     * Return true iif this unit is considered dead
     */
    public bool IsDead()
    {
        return currentHP <= 0;
    }

    /**
     * This method is called when the unit die
     */
    protected virtual void Die()
    {
        GetComponent<SoundManager>().RandomizePitch(dieSound);
    }

    public void OnBeginExplosed(float damages)
    {
        TakeDamage(damages);
    }
}