using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AI : MonoBehaviour
{
    [Tooltip("The maximum distance within an enemy can shoot at the player.The distance is in Unity unit.")]
    [SerializeField]
    private float maximumShootingDistance = 100f;
    public float MaximumShootingDistance { get => maximumShootingDistance; }

    protected AIState CurrentAIState { get ; set; }

    private GameObject player;
    public GameObject Player { get => player; }
    private Pathfinder pathfinder;
    public Pathfinder Pathfinder { get => pathfinder; }

    protected virtual void Awake()
    {
        pathfinder = GetComponent<Pathfinder>();

    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        player = Hero.Player();
    }

    // Update is called once per frame
    void Update()
    {
        CurrentAIState.UpdateState();
    }

    public void SwitchState(AIState newState)
    {
        CurrentAIState = newState;
        CurrentAIState.OnEnterState();
    }
}
