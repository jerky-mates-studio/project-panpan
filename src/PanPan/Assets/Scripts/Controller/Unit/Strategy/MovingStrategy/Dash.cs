﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(UnitDashEvent))]
public class Dash : MovingStrategy
{
    [Header("Dash settings")]
    [Tooltip("The force to apply to perform the dash")]
    [SerializeField]
    private float dashSpeed = 30f;
    [Tooltip("The minimum time between 2 dashes in seconds")]
    [SerializeField]
    private float dashCooldown = 2f;
    private float dashCooldownTimer = 0f;
    [Tooltip("The Time the dash will last in action")]
    [SerializeField]
    private float dashDuration = 0.2f;
    private float dashDurationTimer;
    private Vector2 playerDirection;
    [Tooltip("The trail gameobject for the dash")]
    [SerializeField]
    private GameObject trail;

    private bool hasStartDashing = false;
    private bool isDashing = false;

    private void Update()
    {
        dashCooldownTimer = Math.Max(0, dashCooldownTimer - Time.deltaTime);
        dashDurationTimer = Math.Max(0, dashDurationTimer - Time.deltaTime);
    }

    protected override bool IsThisStrategyOver()
    {
        if (!IsUnlocked())
            return true;

        bool isOver = dashDurationTimer <= 0f && hasStartDashing;
        if (isOver)
        {
            hasStartDashing = false;
            trail.SetActive(false);
        }    
        return isOver;
    }

    private void ResetVariables()
    {
        dashDurationTimer = dashDuration;
        dashCooldownTimer = dashCooldown;
        hasStartDashing = true;
        trail.SetActive(true);

        GetComponent<UnitDashEvent>().TriggerUnitDash();
    }

    public override void Move()
    {
        if (!IsUnlocked())
            return;

        base.Move();
        if (dashDurationTimer <= 0f && !hasStartDashing)
        {
            ResetVariables();            
        }

        Vector2 movement = GetDashDirection() * dashSpeed * GetComponent<Hero>().CurrentTimeStrategy.GetFixedDeltaTime();

        // Make the translation relative to the world. (ignore the current rotation todo the translation) 
        Rigidbody2D rb2D = gameObject.GetComponent<Rigidbody2D>();
        rb2D.MovePosition(rb2D.position + movement);
    }

    /// <summary>
    /// Compute the dash direction based on the player inputs.
    /// Dash towards the direction the player is looking at if there is no player input.
    /// </summary>
    /// <returns>The direction in which the player must dash</returns>
    private Vector2 GetDashDirection()
    {
        // Make sure the player can dash even if he is not currently moving
        Vector2 playerDirection = this.playerMove;
        if (playerDirection == new Vector2(0f, 0f))
        {
            GameObject upperBody = GetComponent<ShooterUnit>().UpperBody;
            Vector3 playerForward = upperBody.transform.up;
            playerDirection = new Vector2(playerForward.x, playerForward.y);
        }
        return playerDirection;
    }

    /**
     * Is called when the dash button is pressed 
     */
    public void OnDash(InputAction.CallbackContext context)
    {
        isDashing = context.ReadValueAsButton();
    }

    protected override bool MustStart()
    {
        if (!IsUnlocked())
            return false;

        return isDashing && dashCooldownTimer <= 0f;
    }

    public override void HandleCollision(Collision2D collision)
    {
        IDashCollisionHandler dashCollisionHandler = collision.gameObject.GetComponent<IDashCollisionHandler>();
        if (! (dashCollisionHandler is null) )
        {
            dashCollisionHandler.OnDashCollision(GetDashDirection());
        }
    }

    public override bool IsDashing()
    {
        return true;
    }
}
