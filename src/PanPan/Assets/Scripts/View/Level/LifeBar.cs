﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{

    private Transform maxHP;
    private Transform currentHP;
    private GameObject player;

    /*
     * Initialize the variables
     */
    public void Start()
    {
        Transform content = transform.GetChild(0).GetChild(0).GetChild(0);
        maxHP = content.GetChild(0);
        currentHP = content.GetChild(1);
        player = GameObject.FindWithTag("Player");

    }

    // Update is called once per frame
    void Update()
    {
        float xScale = player.GetComponent<Hero>().currentHP / player.GetComponent<Hero>().maximumHP;
        currentHP.localScale = new Vector3(xScale, 1, 1);
    }
}
