﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TestUpgrade
{
    private Upgrade upgrade;
    private GameObject player;
    private Material drug;
    private Material energyCell;

    /**
     * This is called before each test
     */
    [SetUp]
    public void SetUp()
    {
        upgrade = Resources.Load<Upgrade>("Prefabs/TestData/TestUpgrade");
        player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Character/Player"));
        //give the player enough material to let him buy the upgrade
        drug = Resources.Load<Material>(Material.materialPath + "/Drug");
        energyCell = Resources.Load<Material>(Material.materialPath + "/EnergyCell");
        player.GetComponent<Hero>().Inventory[drug] = 20;
        player.GetComponent<Hero>().Inventory[energyCell] = 20;
    }

    /**
     * Destroy the player and reset the upgrade. This is called after each test
     */
    [TearDown]
    public void TearDown()
    {
        upgrade.CurrentUpgradeLevel = 0;
        GameObject.Destroy(player);
    }

    /**
     * Test if the player can buy an upgrade if he has enough money
     */
    [Test]
    public void TestCanBuy()
    {
        Assert.IsTrue(upgrade.Buy());
    }
    
    /**
     * Test if the player can't buy an upgrade if he hasn't enough money
     */
    [Test]
    public void TestCantBuy()
    {
        // make player materials not enough to buy
        player.GetComponent<Hero>().Inventory[drug] = 10;
        player.GetComponent<Hero>().Inventory[energyCell] = 10;
        Assert.IsFalse(upgrade.Buy());
    }

    /**
     * Test if the materials are retrieved from the player inventry when he has enough to buy
     */
    [Test]
    public void TestMaterialRetrieve()
    {
        Assert.IsTrue(upgrade.Buy());
        Assert.AreEqual(player.GetComponent<Hero>().Inventory[drug], 0);
        Assert.AreEqual(player.GetComponent<Hero>().Inventory[energyCell], 0);
    }

    /**
     * Test if the materials are not retrieved from the player inventry when he has not enough to buy
     */
    [Test]
    public void TestMaterialNotRetrieve()
    {
        // make player materials not enough to buy
        player.GetComponent<Hero>().Inventory[drug] = 10;
        player.GetComponent<Hero>().Inventory[energyCell] = 10;

        Assert.IsFalse(upgrade.Buy());
        Assert.AreEqual(player.GetComponent<Hero>().Inventory[drug], 10);
        Assert.AreEqual(player.GetComponent<Hero>().Inventory[energyCell], 10);
    }

    /**
     * Test if the upgrade level is incresed when the player buy it
     */
    [Test]
    public void TestUpgradeLevelIncrease()
    {
        Assert.IsTrue(upgrade.Buy());
        Assert.AreEqual(upgrade.CurrentUpgradeLevel, 1);
    }

    /**
     * Test if the upgrade level is incresed when the player can't buy it
     */
    [Test]
    public void TestUpgradeLevelDontIncrease()
    {
        // make player materials not enough to buy
        player.GetComponent<Hero>().Inventory[drug] = 10;
        player.GetComponent<Hero>().Inventory[energyCell] = 10;

        Assert.IsFalse(upgrade.Buy());
        Assert.AreEqual(upgrade.CurrentUpgradeLevel, 0);
    }

    /**
     * Test if the upgrade don't reach max level when the player buy only one level
     */
    [Test]
    public void TestHasNotReachMaxLevel()
    {
        Assert.IsFalse(upgrade.HasReachedMaxLevel());
        Assert.IsTrue(upgrade.Buy());
        Assert.IsFalse(upgrade.HasReachedMaxLevel());
    }

    /**
     * Test if the upgrade reach max level when the player buy the 2 levels
     */
    [Test]
    public void TestHasReachMaxLevel()
    {
        Assert.IsFalse(upgrade.HasReachedMaxLevel());
        Assert.IsTrue(upgrade.Buy());
        Assert.IsFalse(upgrade.HasReachedMaxLevel());

        // make player materials enough to buy the 3rd level
        player.GetComponent<Hero>().Inventory[drug] = 30;
        player.GetComponent<Hero>().Inventory[energyCell] = 30;

        Assert.IsTrue(upgrade.Buy());
        Assert.IsTrue(upgrade.HasReachedMaxLevel());
    }


}
