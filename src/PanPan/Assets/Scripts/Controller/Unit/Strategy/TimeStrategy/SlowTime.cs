﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SlowTime : TimeStrategy
{

    [Header("Slow Time settings")]
    [Tooltip("The duration of the slow time in seconds")]
    [SerializeField]
    private float duration = 2f;
    [Tooltip("The fraction by which the flow of time is reduced.")]
    [SerializeField]
    [Range(0.01f, 1f)]
    private float timeFraction = 0.5f;
    [Tooltip("The fraction by which the flow of time is reduced for the player only.")]
    [SerializeField]
    [Range(0.01f, 1f)]
    private float playerTimeFraction = 0.6f;
    [Tooltip("The minimum time between 2 Slow Time in seconds. This cooldown applies after the Slow Time effect disappear.")]
    [SerializeField]
    private float cooldown = 2f;

    private SwitchColor slowTimeImageEffect;


    private float durationTimer = 0f;
    private float cooldownTimer = 0f;
    private bool hasStartSlowTime = false;
    private bool isSlowTime = false;

    private void Start()
    {
        if (SwitchColor.Instance() != null)
            slowTimeImageEffect = SwitchColor.Instance();
        else
            Debug.Log("SlowTimeEffect is null");

    }

    /**
     * Return the total cooldown before onather slow time
     */
    public float GetCooldown()
    {
        return cooldown;
    }

    /**
     * Return the remaining time to wait before the next Slow Time
     */
    public float GetRemainingTime()
    {
        return cooldownTimer;
    }

    protected override bool IsThisStrategyOver()
    {
        bool isOver = !IsUnlocked() || (hasStartSlowTime && durationTimer <= 0f);
        if (isOver)
        {
            hasStartSlowTime = false;
            Time.timeScale = 1f;
            // it will be null in unit Test and in the SampleScene
            if(slowTimeImageEffect != null)
                slowTimeImageEffect.TriggerSwitch();
        }
        return isOver;
    }

    protected override bool MustStart()
    {
        
        if (!IsUnlocked())
            return false;

        return isSlowTime && cooldownTimer <= 0f;
    }

    public override void UpdateTime()
    {
        if (! hasStartSlowTime)
        {
            hasStartSlowTime = true;
            Time.timeScale = timeFraction;
            durationTimer = duration;
            cooldownTimer = cooldown + duration;
            // it will be null in unit Test and in the SampleScene
            if (slowTimeImageEffect != null)
                slowTimeImageEffect.TriggerSwitch();
        }
        
    }

    /**
     * Is called when the slow time button is pushed
     */
    public void OnSlowTime(InputAction.CallbackContext context)
    {
        isSlowTime = context.ReadValueAsButton();
    }

    // Update is called once per frame
    void Update()
    {
        float previousCooldownTimer = cooldownTimer;
        if(!PauseMenu.isPaused)
            durationTimer = Mathf.Max(0, durationTimer - Time.unscaledDeltaTime);
        cooldownTimer = Mathf.Max(0, cooldownTimer - Time.deltaTime);
        // The player can now launch another Slow Time
        if (previousCooldownTimer != cooldownTimer && cooldownTimer <= 0f)
            gameObject.GetComponentInChildren<ParticleSystem>().Play();
    }

    public override float GetDeltaTime()
    {
        return Time.unscaledDeltaTime * playerTimeFraction;
    }

    public override float GetFixedDeltaTime()
    {
        return Time.fixedUnscaledDeltaTime * playerTimeFraction;
    }

    public override float GetTimeMultiplier()
    {
        return timeFraction / playerTimeFraction;
    }
}
