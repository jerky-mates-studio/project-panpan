using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneAI : AI
{
    [Header("Placement parameters")]
    [Tooltip("The perfect distance for the drone to shoot (in unit)")]
    [SerializeField]
    private float bestShootingRadius = 5f;
    public float BestShootingRadius { get => bestShootingRadius; }
    [Tooltip("The pause before and after the drone shoot (in seconds)")]
    [SerializeField]
    private float pauseTime = 0.2f;
    public float PauseTime { get => pauseTime; }

    private DroneIdleState idleState;
    public DroneIdleState IdleState { get => idleState; }
    private DroneFightState fightState;
    public DroneFightState FightState { get => fightState; }

    protected override void Start()
    {
        base.Start();
        idleState = new DroneIdleState(this);
        fightState = new DroneFightState(this);
        CurrentAIState = IdleState;
        CurrentAIState.OnEnterState();
    }


}
