﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using static UnityEngine.InputSystem.InputAction;

[RequireComponent(typeof(AudioSource))]
public class SteadyDialogueHandler : MonoBehaviour
{
    private static GameObject instance;

    public static GameObject Instance { get => instance;}

    public event EventHandler OnDialogueEnd;

    [Header("Sound settings")]
    [Tooltip("The sound to display at each letter printed")]
    [SerializeField]
    private AudioClip printLetterSound;

    [Header("Display settings")]
    [Tooltip("The TMP component that represents the name of the talking Unit.")]
    [SerializeField]
    private GameObject unitName;
    [Tooltip("The TMP component that represents the text of the talking Unit.")]
    [SerializeField]
    private GameObject unitText;
    [Tooltip("The time to write a letter in the dialogue panel.")]
    [SerializeField]
    private float writeTime = 0;

    private Discussion dialogue;
    private string currentSentence = "";
    private bool isCurrentSentenceWritten = true;
    private bool isTalking = false;

    private void Awake()
    {
        if (Instance == null)
            instance = gameObject;
    }

    private void Start()
    {
        Hero.Player().GetComponent<Hero>().UIActionMap.FindAction("Submit").started += OnSubmit;
        Hero.Player().GetComponent<Hero>().UIActionMap.FindAction("Cancel").started += OnCancel;
    }

    /**
     * Called when the player hit the Submit button
     */
    private void OnSubmit(CallbackContext ctx)
    {
        if (isTalking)
        {
            DisplayNextSentence();
        }
    }

    /**
     * Called when the player hit the Cancel button
     */
    private void OnCancel(CallbackContext ctx)
    {
        if (ctx.interaction is PressInteraction && isTalking)
        {
            EndDialogue();
        }
    }

    /**
     * Display the "dialogue" in the dialogue panel
     */
    public void StartDialogue(Discussion dialogue)
    {
        isTalking = true;
        Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("UI");
        dialogue.Reset();
        this.dialogue = dialogue;
        gameObject.GetComponent<Animator>().SetBool("isTalking", true);
        DisplayNextSentence();
    }

    /**
     * End the dialogue and reset all the needed component in order to be ready for the next dialogue
     */
    private void EndDialogue()
    {
        isTalking = false;
        Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("Player");
        dialogue.Reset();
        gameObject.GetComponent<Animator>().SetBool("isTalking", false);
        // Check if OnDialogueEnd is null, if not: trigger the event
        OnDialogueEnd?.Invoke(this, EventArgs.Empty);

    }

    /**
     * Display the next sentence of the dialogue
     */
    private void DisplayNextSentence()
    {
        StopAllCoroutines();
        if (!isCurrentSentenceWritten)
        {
            unitText.GetComponent<TextMeshProUGUI>().text = currentSentence;
            isCurrentSentenceWritten = true;
        }
        else if (dialogue.IsOver())
            EndDialogue();
        else
        {
            List<Dialogue.DialogueLine> lines = dialogue.GetNextDialogueLines();
            Dialogue.DialogueLine line = lines[0];
            GameObject speaker = dialogue.GetSpeakers()[line.speakerIndex];
            unitName.GetComponent<TextMeshProUGUI>().text = speaker.GetComponent<NonShooterUnit>().UnitName;
            StartCoroutine(TypeText(line.sentence));
        }
        
    }

    /**
     * Type the text letter by letter in the dialogue panel. The current sentence is saved in order to be able to display it
     * even if the coroutine is stopped.
     */
    private IEnumerator TypeText(string sentence)
    {
        currentSentence = sentence;
        isCurrentSentenceWritten = false;
        unitText.GetComponent<TextMeshProUGUI>().text = "";
        foreach(char letter in sentence.ToCharArray())
        {
            unitText.GetComponent<TextMeshProUGUI>().text += letter;
            GetComponent<AudioSource>().clip = printLetterSound;
            GetComponent<AudioSource>().Play();
            // wait some time before next execution
            yield return new WaitForSeconds(writeTime);
        }
        isCurrentSentenceWritten = true;
    }

}
