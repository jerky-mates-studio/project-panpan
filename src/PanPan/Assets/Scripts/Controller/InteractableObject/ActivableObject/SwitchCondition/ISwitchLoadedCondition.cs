using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISwitchLoadedCondition
{
    /// <summary>
    /// Reset the condition to its initial state meaning that the load of the linked switches is 0 and the object linked to this condition is deactivated as well
    /// </summary>
    void Deactivate();

    /// <summary>
    /// Activate the condiation, meaning that the linked switches are fully loaded and the object linked to this condition is activated as well
    /// </summary>
    void Activate();

}
