using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EniacIdleState : AIState
{
    private EniacAI Context { get; set; }

    public EniacIdleState(EniacAI context) : base(context)
    {
        Context = context;
    }

    public override void OnEnterState()
    {
        return;
    }

    public override void UpdateState()
    {
        if (isPlayerOnSight())
        {
            Context.SwitchState(Context.FightState);
        }
    }

}
