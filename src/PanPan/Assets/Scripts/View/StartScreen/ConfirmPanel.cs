﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Interactions;

public class ConfirmPanel : MonoBehaviour
{
    [Tooltip("The button handler game object")]
    [SerializeField]
    private GameObject buttonHandler;
    private PlayerController control;

    private void Awake()
    {
        control = new PlayerController();
        control.UI.Cancel.performed +=
            ctx =>
            {
                if (ctx.interaction is PressInteraction)
                {
                    buttonHandler.GetComponent<StartScreen>().OnCancelNewGame();
                }
            };
    }

    private void OnDisable()
    {
        control.Disable();
    }

    private void OnEnable()
    {
        control.Enable();
    }


}
