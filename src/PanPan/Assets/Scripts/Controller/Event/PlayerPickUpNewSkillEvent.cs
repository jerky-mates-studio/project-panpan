using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickUpNewSkillEvent
{
    private static PlayerPickUpNewSkillEvent instance;

    public static PlayerPickUpNewSkillEvent GetInstance()
    {
        if (instance == null)
            instance = new PlayerPickUpNewSkillEvent();
        return instance;
    }

    public delegate void PlayerPickUpNewSkillDelegate(SkillItemData skillData);
    public event PlayerPickUpNewSkillDelegate PlayerPickUpNewSkill;

    public void TriggerPlayerPickUpNewSkill(SkillItemData skillData) => PlayerPickUpNewSkill?.Invoke(skillData);


}
