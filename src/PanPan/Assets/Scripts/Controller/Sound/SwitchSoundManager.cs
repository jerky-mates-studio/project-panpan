using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LoadSwitchEvent))]
[RequireComponent(typeof(AudioSource))]
public class SwitchSoundManager : MonoBehaviour
{
    [Header("Loading sound")]
    [Tooltip("The audio clip of for the switch loading sound")]
    [SerializeField]
    private AudioClip loadingSound;
    [Tooltip("The pitch of the clip when the load is empty")]
    [SerializeField]
    [Range(-3f, 3f)]
    private float minPitch = 0.5f;
    [Tooltip("The pitch of the clip when the load is complete")]
    [SerializeField]
    [Range(-3f, 3f)]
    private float maxPitch = 1.5f;

    [Header("Loaded sound")]
    [Tooltip("The audio clip of for the switch loaded sound")]
    [SerializeField]
    private AudioClip loadedSound;

    [Header("Unloaded sound")]
    [Tooltip("The audio clip of for the switch unloaded sound")]
    [SerializeField]
    private AudioClip unloadedSound;

    private LoadSwitchEvent loadSwitchEvent;
    private AudioSource audioSource;
    private float defaultPitch;

    private void Awake()
    {
        loadSwitchEvent = GetComponent<LoadSwitchEvent>();

        audioSource = GetComponent<AudioSource>();
        defaultPitch = audioSource.pitch;
    }

    private void OnEnable()
    {
        loadSwitchEvent.SwitchLoading += PlaySwitchLoadingSound;
        loadSwitchEvent.SwitchActivated += HandleSwitchChangeState;
    }

    private void OnDisable()
    {
        loadSwitchEvent.SwitchLoading -= PlaySwitchLoadingSound;
        loadSwitchEvent.SwitchActivated -= HandleSwitchChangeState;
    }

    private void PlaySwitchLoadingSound(float loadingPercentage)
    {
        float currentPitch = loadingPercentage * (maxPitch - minPitch) + minPitch;
        audioSource.pitch = currentPitch;
        audioSource.PlayOneShot(loadingSound, 0.5f);
    }

    private void HandleSwitchChangeState(bool isActivated)
    {
        if (isActivated)
            PlaySwitchLoadedSound();
        else
            PlaySwitchUnloadedSound();
    }

    private void PlaySwitchLoadedSound()
    {
        audioSource.pitch = defaultPitch;
        audioSource.PlayOneShot(loadedSound, 0.5f);
    }

    private void PlaySwitchUnloadedSound()
    {
        audioSource.pitch = defaultPitch;
        audioSource.PlayOneShot(unloadedSound, 0.38f);
    }
}
