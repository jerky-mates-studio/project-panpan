using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompletion : MonoBehaviour
{
    /// <summary>
    /// Trigger the "LevelCompletedEvent"
    /// </summary>
    public void TriggerLevelCompleted()
    {
        LevelCompletedEvent.GetInstance().TriggerLevelCompleted();
    }
}
