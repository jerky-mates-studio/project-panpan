using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitTookDamagesEvent : MonoBehaviour
{
    public delegate void UnitTookDamagesDelegate(float damages);
    public event UnitTookDamagesDelegate UnitTookDamages;
    public void NotifyUnitTookDamages(float damages) => UnitTookDamages?.Invoke(damages);

}
