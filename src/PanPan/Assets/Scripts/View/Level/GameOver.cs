﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    [Tooltip("The name of the scene to load after the game over")]
    public string villageScene = "Village";

    [Header("Game over message display")]
    [Tooltip("The message displayed when the player win")]
    public string victoryMessage = "Victory !";
    [Tooltip("The color of the \"victoryMessage\"")]
    public Color victoryColor;
    [Tooltip("The message displayed when the player dies")]
    public string deathMessage = "You died !";
    [Tooltip("The color of the \"deathMessage\"")]
    public Color deathColor;

    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // If the player is dead, trigger the "game over" animation event
        if (player.GetComponent<Hero>().IsDead())
        {
            UpdateGameOverMessage(deathMessage, deathColor);
            GetComponent<Animator>().SetTrigger("GameOver");
        } else if (player.GetComponent<Hero>().HasReachedPortal())
        {
            UpdateGameOverMessage(victoryMessage, victoryColor);
            GetComponent<Animator>().SetTrigger("GameOver");
        }
    }

    /**
     * Update the text component to be ready for the game over animation.
     * The message of the text compenent is set to "message" and its color is set to "messageColor".
     */
    private void UpdateGameOverMessage(string message, Color messageColor)
    {
        Text textChild = GetComponentInChildren<Text>();
        textChild.text = message;
        textChild.color = messageColor;
    }

    /**
     * Load the "Village". This method is called when the animation of the game over is finished
     * (see the animation "GameOverAnimation" to see the event)
     */
    public void LoadVillageScene()
    {
        SaveSystem.Save();
        SceneManager.LoadScene(villageScene);
    }
}
