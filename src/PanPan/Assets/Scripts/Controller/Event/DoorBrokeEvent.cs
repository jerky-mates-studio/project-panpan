using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBrokeEvent : MonoBehaviour
{
    public delegate void DoorBrokeDelegate();
    public event DoorBrokeDelegate DoorBroke;
    public void TriggerDoorBroke() => DoorBroke?.Invoke();
}
