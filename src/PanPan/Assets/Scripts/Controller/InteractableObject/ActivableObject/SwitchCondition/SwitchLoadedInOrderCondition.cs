using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchLoadedInOrderCondition : MultipleSwitchLoadedCondition
{
    [Header("Condition parameter")]
    [Tooltip("The time to wait before resetting the switches after a bad attempt")]
    [SerializeField]
    private float resetDelay = 1f;
    [SerializeField]
    [Min(0f)]
    [Tooltip("The LoadSwitch gameobject that must be loaded so that this condition can be true")]
    private GameObject[] loadSwitches;

    private void Awake()
    {
        AreLoadSwitchesValid();
    }

    private void OnEnable()
    {
        foreach (GameObject loadSwitch in loadSwitches)
            loadSwitch.GetComponent<LoadSwitchEvent>().SwitchActivated += HandleSwitchActivation;
    }

    private void OnDisable()
    {
        foreach (GameObject loadSwitch in loadSwitches)
            loadSwitch.GetComponent<LoadSwitchEvent>().SwitchActivated -= HandleSwitchActivation;
    }

    /// <summary>
    /// Return true iif all switches in 'loadSwitches' are activated in the list order (the one is index 0 then the one in index 1, ...).
    /// If all switch are deactivated, the order is considered as good.
    /// </summary>
    /// <returns>True iif the swicthes are activated in the good order</returns>
    private bool IsOrderGood()
    {
        if (AreAllSwitchesDeactivated())
            return true;


        int firtsUnactivatedSwitchIndex = 0;
        bool isOrderGood = true;
        foreach (GameObject loadSwitch in loadSwitches)
        {
            LoadSwitch switchObject = loadSwitch.GetComponent<LoadSwitch>();

            if ( ! switchObject.IsActivated())
                break;

            firtsUnactivatedSwitchIndex++;
        }

        // Check that all switches after the last activated switch are not activated yet
        for (int i = firtsUnactivatedSwitchIndex; i < loadSwitches.Length; i++)
        {
            LoadSwitch switchObject = loadSwitches[i].GetComponent<LoadSwitch>();
            isOrderGood = isOrderGood && (!switchObject.IsActivated()) ;
        }

        return isOrderGood;
    }

    /// <summary>
    /// Activate the condition if all switches in 'loadSwitches' have been activated in the right order.
    /// Deactivate all switches in 'loadSwitches' if there were not activated in the right order
    /// </summary>
    /// <param name="isActivated">True if the last switch which state has changed is activated, false otherwise</param>
    private void HandleSwitchActivation(bool isActivated)
    {
        // Ignore switch deactivated event if we already are resetting the switches 
        if (!isActivated && State == MultipleSwitchConditionState.ResettingSwitches)
            return;

        if (IsOrderGood())
        {
            if (AreAllSwitchesActivated())
            {
                IsActivated = true;
                TriggerActivation(true);
            }
        } else
            StartCoroutine(DelaySwitchReset());
    }

    private IEnumerator DelaySwitchReset()
    {
        yield return new WaitForSeconds(resetDelay);
        ResetAllSwicthes();
    }

    // MultipleSwitchLoadedCondition Interface

    protected override GameObject[] GetLoadSwitches()
    {
        return loadSwitches;
    }

    //------------------------------------
}
