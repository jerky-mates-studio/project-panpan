using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIState
{

    private BoxCollider2D boxCollider;
    private GameObject player;
    public GameObject Player { get => player; }
    private float maximumShootingDistance;
    public float MaximumShootingDistance { get => maximumShootingDistance; }


    public AIState(AI context)
    {
        boxCollider = context.GetComponent<BoxCollider2D>();
        player = context.Player;
        maximumShootingDistance = context.MaximumShootingDistance;
    }

    /// <summary>
    /// Return true iif the enemy has a clear sight on the player, i.e., there is no obstacle between 
    /// the enemy and the player and the distance between them is smaller that "shootRange". 
    /// </summary>
    /// <returns>True iif the enemy has a clear sight on the player</returns>
    public bool isPlayerOnSight()
    {
        if (Player.GetComponent<Hero>().IsDead())
            return false;

        RaycastHit2D[] targets = new RaycastHit2D[1];
        // Compute the vector that link this enemy.transform and the player.transform
        Vector3 direction = (Player.transform.position - boxCollider.transform.position).normalized;
        int targetNumber = boxCollider.Raycast(direction, targets, maximumShootingDistance, LayerMask.GetMask("Shootable_Unit", "Block_Bullet"));
        if (targetNumber < 1)
            return false;
        else
        {
            return targets[0].collider.CompareTag(Player.tag);
        }
    }

    /// <summary>
    /// Return true iif the enemy has a clear sight on the player from the position passed as parameter, i.e., there is no obstacle between 
    /// the position given and the player and the distance between them is smaller that "shootRange". 
    /// </summary>
    /// <param name="enemyPosition">The position to use to see if the player is in clear sight</param>
    /// <returns>True iif the enemy has a clear sight on the player</returns>
    public bool isPlayerOnSight(Vector3 enemyPosition)
    {
        Vector3 initialPosition = boxCollider.transform.position;
        boxCollider.transform.position = enemyPosition;
        bool isOnSight = isPlayerOnSight();
        boxCollider.transform.position = initialPosition;
        return isOnSight;
    }

    /// <summary>
    /// The method that must be called when this state become the current state
    /// </summary>
    public abstract void OnEnterState();

    /// <summary>
    /// The method that must be called in order to opdate the current state
    /// </summary>
    public abstract void UpdateState();
}
