﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Walk : MovingStrategy
{
    [Header("Movement settings")]
    [Tooltip("The speed of the movement in unit/sec")]
    [SerializeField]
    private float moveSpeed = 12f;

    public float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }

    private TimeStrategy unitTimeStrategy;

    void Start()
    {
        Hero hero = GetComponent<Hero>();
        if (! (hero is null) )
            unitTimeStrategy = GetComponent<Hero>().CurrentTimeStrategy;
    }

    protected override bool IsThisStrategyOver()
    {
        if (!IsUnlocked())
            return true;

        return false;
    }

    public override void Move()
    {
        if (!IsUnlocked())
            return;

        base.Move();
        // we multiply "moveSpeed" by the time between 2 frames to get the travelled distance
        Vector2 movement;
        // If the unit start the slow time, it can move faster than otehr unit
        if (!(unitTimeStrategy is null))
            movement = this.playerMove * MoveSpeed * unitTimeStrategy.GetFixedDeltaTime();
        else
            movement = this.playerMove * MoveSpeed * Time.deltaTime;
        // Make the translation relative to the world
        Rigidbody2D rb2D = gameObject.GetComponent<Rigidbody2D>();
        rb2D.MovePosition(rb2D.position + movement);
    }

    protected override bool MustStart()
    {
        if (!IsUnlocked())
            return false;

        return false;
    }

    public override void HandleCollision(Collision2D collision)
    {
        // does nothing special when a collision happened
        return;
    }

    public override bool IsDashing()
    {
        return false;
    }
}
