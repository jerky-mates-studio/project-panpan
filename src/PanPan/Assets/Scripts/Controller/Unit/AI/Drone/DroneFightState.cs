using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneFightState : AIState
{
    private Enemy enemy;
    private Vector3 currentDestination;
    private float pauseTimer = 0f;
    private bool hasShoot = false;
    private DroneAI Context { get; set; }

    public DroneFightState(DroneAI context) : base(context)
    {
        enemy = context.GetComponent<Enemy>();
        Context = context;
    }

    public override void OnEnterState()
    {
        FindGoodShootingPosition();
    }

    /// <summary>
    /// Find a random position within the "best shooting range" (defined in the DroneAI) from the player position.
    /// Store the destination choosen in the "currentDestination" variable.
    /// </summary>
    public void FindGoodShootingPosition()
    {
        Vector2 randomPosition = Random.insideUnitCircle * Context.BestShootingRadius * 0.5f;
        currentDestination = (Context.Player.transform.position - Context.transform.position).normalized * Context.BestShootingRadius * 0.5f + Context.transform.position + new Vector3(randomPosition.x, randomPosition.y, 0);
        if (!isPlayerOnSight(currentDestination))
        {
            currentDestination = Context.transform.position;
        }
        Context.Pathfinder.SearchPath(currentDestination);
    }

    /// <summary>
    /// Ensure to pause the unit's move before and after shooting the player
    /// </summary>
    public void ShootAtPlayer()
    {
        pauseTimer += Time.deltaTime;
        if (!hasShoot && !enemy.IsDead() && pauseTimer >= Context.PauseTime)
        {
            enemy.Fire();
            hasShoot = true;
        }
        else if (pauseTimer >= (2 * Context.PauseTime) )
        {
            pauseTimer = 0f;
            hasShoot = false;
            FindGoodShootingPosition();
        }
    }

    public override void UpdateState()
    {
        if (! isPlayerOnSight())
        {
            Context.SwitchState(Context.IdleState);
        }
        else
        {
            enemy.LookAt(Context.Player.transform.position);
            float distanceFromPlayer = (Context.Player.transform.position - currentDestination).magnitude;
            if (distanceFromPlayer > Context.BestShootingRadius)
            {
                FindGoodShootingPosition();
            }else if (Context.Pathfinder.IsPathCompleted())
            {
                ShootAtPlayer();
            }
        }
    }
}
