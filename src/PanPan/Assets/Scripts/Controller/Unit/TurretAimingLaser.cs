using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class TurretAimingLaser : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private Turret turret;
    private GameObject player;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        turret = GetComponentInParent<Turret>();

        lineRenderer.startColor = turret.BulletType.BulletColor;
        lineRenderer.endColor = turret.BulletType.BulletColor;
    }

    // Start is called before the first frame update
    void Start()
    {
        player = Hero.Player();
        ActivateLaser(false);
    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(0, turret.getBulletInitialPosition());
        lineRenderer.SetPosition(1, player.transform.position);
    }

    public void ActivateLaser(bool setActive)
    {
        lineRenderer.enabled = setActive;
    }
}
