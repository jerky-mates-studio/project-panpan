using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : ShooterUnit
{
    private Animator animator;

    protected override void Awake()
    {
        base.Awake();
        animator = GetComponent<Animator>();
    }

    public override void LookAt(Vector3 toLook)
    {
        base.LookAt(toLook);
        LookAt(Legs, Vector3.right );
    }

    public override void Fire()
    {
        CreateBullet();
        animator.SetTrigger("Shoot");
    }

    protected override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        // ensure the turret is always full life
        currentHP = maximumHP;
    }
}
