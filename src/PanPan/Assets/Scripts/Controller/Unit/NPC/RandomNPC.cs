﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNPC : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Color color = Random.ColorHSV();
        color.a = 1;
        GetComponentInChildren<SpriteRenderer>().color = color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
