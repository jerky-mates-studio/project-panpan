using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompletedEvent
{
    private static LevelCompletedEvent instance;

    public static LevelCompletedEvent GetInstance()
    {
        if (instance == null)
            instance = new LevelCompletedEvent();
        return instance;
    }

    public delegate void LevelCompletedDelegate();
    public event LevelCompletedDelegate LevelCompleted;

    public void TriggerLevelCompleted() => LevelCompleted?.Invoke();
}
