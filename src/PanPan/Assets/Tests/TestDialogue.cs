﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System;

public class TestDialogue
{

    /**
     * Load the test monologue and initialize it
     */
    private Monologue LoadMonologue()
    {
        Monologue testResetMonologue = Resources.Load<Monologue>("Prefabs/TestData/Dialogue/TestMonologueReset");
        // Initialize monologue
        testResetMonologue.Reset();
        return testResetMonologue;
    }
    
    /**
     * Load the test monologue and initialize it
     */
    private Discussion LoadDiscussion()
    {
        Discussion discussion = Resources.Load<Discussion>("Prefabs/TestData/Dialogue/TestDiscussion");
        // Initialize monologue
        discussion.Reset();
        return discussion;
    }

    //We use the "Test" attribute instead of "UnityTest" because we don't test monobehaviour class 
    [Test]
    /**
     * Test if the Reset method of the monologue works well
     */
    public void TestMonologueReset()
    {
        Monologue testResetMonologue = LoadMonologue();

        string sentence1 = testResetMonologue.GetNextDialogueLines()[0].sentence;
        testResetMonologue.Reset();
        string sentence2 = testResetMonologue.GetNextDialogueLines()[0].sentence;
        Assert.AreEqual(sentence1, sentence2);
    }

    /**
     * Test if the the monologue throws an exception if we fetch all the dialogue lines without using the "Reset" method
     */
    [Test]
    public void TestMonologueExeption()
    {
        Monologue testResetMonologue = LoadMonologue();

        testResetMonologue.GetNextDialogueLines();
        Assert.Throws<IndexOutOfRangeException>( () => testResetMonologue.GetNextDialogueLines() );
    }

    /**
     * Test if the "IsOver" method is true when we fetch all the lines of the monologue
     */
    [Test]
    public void TestMonologueIsOver()
    {
        Monologue monologue = LoadMonologue();
        monologue.GetNextDialogueLines();
        Assert.IsTrue(monologue.IsOver());
    }

    /**
     * Test if the "IsOver" method is false when we call it before we fetched all the lines of the monologue
     */
    [Test]
    public void TestMonologueIsNotOver()
    {
        Monologue monologue = LoadMonologue();
        Assert.IsFalse(monologue.IsOver());
    }

    [Test]
    /**
     *  Test if the Reset method of the discussion works well
     */
    public void TestDiscussionReset()
    {
        Discussion discussion = LoadDiscussion();

        string expectedSentence1 = discussion.GetNextDialogueLines()[0].sentence;
        string expectedSentence2 = discussion.GetNextDialogueLines()[0].sentence;
        discussion.Reset();

        string sentence1 = discussion.GetNextDialogueLines()[0].sentence;
        string sentence2 = discussion.GetNextDialogueLines()[0].sentence;
        Assert.AreEqual(expectedSentence1, sentence1);
        Assert.AreEqual(expectedSentence2, sentence2);
    }

    /**
     * Test if the the discussion throws an exception if we fetch all the dialogue lines without using the "Reset" method
     */
    [Test]
    public void TestDiscussionExeption()
    {
        Discussion discussion = LoadDiscussion();

        discussion.GetNextDialogueLines();
        discussion.GetNextDialogueLines();
        Assert.Throws<IndexOutOfRangeException>(() => discussion.GetNextDialogueLines());
    }

    /**
     * Test if the "IsOver" method is true when we fetch all the lines of the discussion
     */
    [Test]
    public void TestDiscussionIsOver()
    {
        Discussion discussion = LoadDiscussion();
        discussion.GetNextDialogueLines();
        discussion.GetNextDialogueLines();
        Assert.IsTrue(discussion.IsOver());
    }

    /**
     * Test if the "IsOver" method is false when we call it before we fetched all the lines of the discussion
     */
    [Test]
    public void TestDiscussionIsNotOver()
    {
        Discussion discussion = LoadDiscussion();
        Assert.IsFalse(discussion.IsOver());

        discussion.GetNextDialogueLines();
        Assert.IsFalse(discussion.IsOver());

    }
}
