﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UpgradesPanel : MonoBehaviour
{
    private GameObject upgradeParent;
    private float upgradeHeight;
    private InputActionMap UIActionMap;

    // Start is called before the first frame update
    void Start()
    {
        UIActionMap = Hero.Player().GetComponent<Hero>().UIActionMap;
        UIActionMap.FindAction("Navigate").performed += Navigate;
        upgradeHeight = GetComponentInParent<VendorPanel>().UpgradeUIPrefab.GetComponent<RectTransform>().rect.height;
        upgradeParent = GetComponentInParent<VendorPanel>().UpgradesParent;
    }

    /// <summary>
    /// Unbind the controls to the action maps in order to avoid action triggered by destroyed objects 
    /// </summary>
    private void OnDestroy()
    {
        UIActionMap.FindAction("Navigate").performed -= Navigate;
    }

    /// <summary>
    /// Is called when the user try to navigate between the upgrades of the Vendor Panel.
    /// Allows the ScrollRect to scroll to always display the selected upgrades.
    /// </summary>
    /// <param name="context">The context of the action</param>
    private void Navigate(InputAction.CallbackContext context)
    {
        Vector2 dir = context.ReadValue<Vector2>();
        if (dir.y < 0)
            upgradeParent.transform.position += new Vector3(0f, upgradeHeight, 0f);
        else if(dir.y > 0)
            upgradeParent.transform.position -= new Vector3(0f, upgradeHeight, 0f);
    }
}
