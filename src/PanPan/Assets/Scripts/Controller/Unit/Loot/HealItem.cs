﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealItem : MonoBehaviour
{
    [Tooltip("The amount of items the player will gain by picking up this item")]
    public int itemAmount = 40;

    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    /**
     * This method is called by Unity when this object collides with another.
     * If this object is collided by the player, heal him and destroy this heal item.
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Only collide with the objects on the "Unit" layer and with unit which tag is different
        // from the shooter (Enemy can't shoot another Enemy + Unit can't shoot itself)
        if (collision.CompareTag(player.tag))
        {
            OnPlayerCollided(collision);
        }

    }

    protected virtual void OnPlayerCollided(Collider2D collision)
    {
        collision.gameObject.GetComponent<Hero>().Heal(itemAmount);
        Destroy(gameObject);
    }


}
