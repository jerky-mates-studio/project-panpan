using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(Seeker))]
[RequireComponent(typeof(Enemy))]
public class Pathfinder : MonoBehaviour
{
    // the component responsible to compute the paths
    private Seeker seeker;
    // The Enemy script of this game object
    private Enemy enemy;
    // The width of the units, to be sure that all units can reached all the destinatination
    private float unitWidth = 0.5f;

    // The path this unit is currently following
    private Path path;
    // The path's way point where the unit currently is
    private int currentWayPoint = 0;

    private void Awake()
    {
        seeker = gameObject.GetComponent<Seeker>();
        enemy = gameObject.GetComponent<Enemy>();
    }


    /// <summary>
    /// Look for the shortest path on the map betwwen the current position of the unit
    /// and the destination position
    /// </summary>
    /// <param name="destination">The destination we want to find a path to</param>
    public void SearchPath(Vector3 destination)
    {
        seeker.StartPath(transform.position, destination, OnPathComputed);
    }

    /// <summary>
    /// Stop the unit if it was moving to a destination
    /// </summary>
    public void StopSearching()
    {
        path = null;
    }

    private void OnPathComputed(Path path)
    {
        if (!path.error)
        {
            this.path = path;
            currentWayPoint = 0;
        }
        else
        {
            Debug.Log(path.errorLog);
        }
    }

    /// <summary>
    /// Check if the units walked all the path
    /// </summary>
    /// <returns>True iif the unit has reached the end of the path</returns>
    public bool IsPathCompleted()
    {
        return path.vectorPath.Count <= currentWayPoint;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 enemyNextMove = new Vector2(0, 0);
        if (! (path is null || IsPathCompleted()) )
        { 
            Vector2 difference = (Vector2)(path.vectorPath[currentWayPoint] - transform.position);
            enemyNextMove = difference.normalized;
            float nextWayPointDistance = difference.magnitude;

            if (nextWayPointDistance <= unitWidth)
            {
                currentWayPoint++;
            }

        }
        enemy.CurrentMovingStrategy.playerMove = enemyNextMove;
    }
}
