using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitDashEvent))]
[RequireComponent(typeof(SoundManager))]
public class PlayerSoundManager : MonoBehaviour
{
    [Tooltip("The audio clip to play when the player begins to dash")]
    [SerializeField]
    private AudioClip dashClip;
    [Tooltip("The audio clip to play when the player picks up a new skill")]
    [SerializeField]
    private AudioClip pickUpClip;

    private void OnEnable()
    {
        GetComponent<UnitDashEvent>().UnitDash += PlayDashSound;
        PlayerPickUpNewSkillEvent.GetInstance().PlayerPickUpNewSkill += PlayPickUpNewSkillSound;
    }

    private void OnDisable()
    {
        GetComponent<UnitDashEvent>().UnitDash -= PlayDashSound;
        PlayerPickUpNewSkillEvent.GetInstance().PlayerPickUpNewSkill -= PlayPickUpNewSkillSound;
    }

    private void PlayDashSound()
    {
        GetComponent<SoundManager>().RandomizePitch(dashClip);
    }

    private void PlayPickUpNewSkillSound(SkillItemData skillData)
    {
        // Do nothing for now as the music plays together with the background music -> the effect is not very cool
        // GetComponent<SoundManager>().RandomizePitch(pickUpClip, 1f);
    }
}
