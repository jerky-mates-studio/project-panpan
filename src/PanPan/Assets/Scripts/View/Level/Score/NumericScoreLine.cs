using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(LoadSwitchEvent))]
public class NumericScoreLine : MonoBehaviour, IScoreLine
{
    [Tooltip("The UI element that introduce the score value")]
    [SerializeField]
    private TMP_Text scoreText;
    [Tooltip("The UI element that display score value")]
    [SerializeField]
    private TMP_Text scoreValue;

    [Header("Animation parameters")]
    [Tooltip("The suffix to put after the score value")]
    [SerializeField]
    private string scoreSuffix = "";
    [Tooltip("The sound to play when a score line is displayed")]
    [SerializeField]
    private AudioClip lineDisplayedSound;
    [Tooltip("The duration of the score display animation (in seconds)")]
    [SerializeField]
    private float displayAnimationDuration = 2f;
    [Tooltip("The time between 2 frames of the score display animation (in seconds)")]
    [SerializeField]
    private float timeBetweenFrames = 0.15f;
    [Tooltip("The maximum font size to use during the score display animation")]
    [SerializeField]
    private float maxFontSize = 100f;

    private AudioSource audioSource;
    private LoadSwitchEvent loadSwitchEvent;
    private int endValue;
    public int EndValue { get => endValue; set => endValue = value; }
    
    private float initialFontSize;
    private Color initialTextColor;
    private Color initialValueColor;

    public IEnumerator DisplayLine()
    {
        scoreText.gameObject.SetActive(true);
        scoreValue.gameObject.SetActive(true);
        audioSource.PlayOneShot(lineDisplayedSound);

        scoreText.color = initialTextColor;
        scoreValue.color = initialValueColor;

        yield return StartCoroutine(AnimateScoreDisplay());

        yield return null;
    }

    /// <summary>
    /// Handle the animation of the score displayed
    /// </summary>
    /// <returns></returns>
    public IEnumerator AnimateScoreDisplay()
    {
        int frameNumber = (int)(displayAnimationDuration / timeBetweenFrames);
        if (EndValue == 0)
            frameNumber = 1;

        int scoreValueIncrement = (int)Math.Ceiling(EndValue / (double)frameNumber);
        float fontSizeIncrement = (float)Math.Ceiling((maxFontSize - initialFontSize) / frameNumber);
        int currentValue = 0;

        for (int i = 0; i < frameNumber; i++)
        {
            scoreValue.text = currentValue.ToString() + scoreSuffix;
            currentValue = Mathf.Min(currentValue + scoreValueIncrement, EndValue);
            scoreValue.fontSize = Mathf.Min(scoreValue.fontSize + fontSizeIncrement, maxFontSize);
            loadSwitchEvent.TriggerSwitchLoading(i / (float)frameNumber);
            yield return new WaitForSeconds(timeBetweenFrames);
        }

        scoreValue.text = currentValue.ToString() + scoreSuffix;
        loadSwitchEvent.TriggerSwitchActivated(true);
    }

    public IEnumerator HideLine()
    {
        Color newColor = initialTextColor;
        newColor.a = 0f;
        scoreText.color = newColor;

        newColor = initialValueColor;
        newColor.a = 0f;
        scoreValue.color = newColor;

        scoreText.gameObject.SetActive(false);
        scoreValue.gameObject.SetActive(false);
        yield return null;
    }

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        loadSwitchEvent = GetComponent<LoadSwitchEvent>();

        initialTextColor = scoreText.color;
        initialValueColor = scoreValue.color;
        initialFontSize = scoreValue.fontSize;

        InitDisplay();
    }

    private void InitDisplay()
    {
        Color newColor = initialTextColor;
        newColor.a = 0f;
        scoreText.color = newColor;

        newColor = initialValueColor;
        newColor.a = 0f;
        scoreValue.color = newColor;

        scoreText.gameObject.SetActive(false);
        scoreValue.gameObject.SetActive(false);
    }

}
