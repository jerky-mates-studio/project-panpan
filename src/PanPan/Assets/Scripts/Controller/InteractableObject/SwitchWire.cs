using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

[RequireComponent(typeof(SpriteShapeRenderer))]
public class SwitchWire : MonoBehaviour
{
    [Header("Wire customization")]
    [SerializeField]
    [Tooltip("The LoadSwitch gameobject that must be loaded so that this condition can be true")]
    private GameObject loadSwitch;

    private UnityEngine.Material rendererMaterial;


    // Start is called before the first frame update
    void Start()
    {
        rendererMaterial = GetComponent<SpriteShapeRenderer>().materials[1];
        rendererMaterial.SetInt("_IsActive", 0);

        if (loadSwitch.GetComponent<LoadSwitch>() == null)
            Debug.LogError("the 'loadSwitch' member has no 'LoadSwitch' scripts");

    }

    /// <summary>
    /// Set the variable of the shader according to the state of the load switch
    /// </summary>
    /// <param name="isActivated"></param>
    private void ActivateWire(bool isActivated)
    {
        if (isActivated)
            rendererMaterial.SetInt("_IsActive", 1);
        else
            rendererMaterial.SetInt("_IsActive", 0);
    }

    private void OnEnable()
    {
        loadSwitch.GetComponent<LoadSwitchEvent>().SwitchActivated += ActivateWire;
    }

    private void OnDisable()
    {
        loadSwitch.GetComponent<LoadSwitchEvent>().SwitchActivated -= ActivateWire;
    }

}
