using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashIdleState : DashState
{
    private AI unitAI;

    public DashIdleState(DashableUnit context) : base(context)
    {
        unitAI = Context.GetComponent<AI>();
    }

    public override void HandleCollision(Collision2D collision)
    {
        return;
    }

    public override void OnEnterState()
    {
        if (unitAI != null)
            unitAI.enabled = true;
    }

    public override void UpdateState()
    {
        return;
    }
}
