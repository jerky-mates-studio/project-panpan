using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillName
{
    Walk,
    Dash,
    Shoot,
    Slash,
    TimeStop
}

[CreateAssetMenu(menuName = "Model/SkillItem")]
public class SkillItemData : ScriptableObject
{
    [Tooltip("The name of the skill this object represents")]
    [SerializeField]
    private SkillName skillName;
    [Tooltip("The image that that match to this material")]
    [SerializeField]
    private Sprite image;

    public SkillName SkillName { get { return skillName; } }
    public Sprite Image { get { return image; } }
}
