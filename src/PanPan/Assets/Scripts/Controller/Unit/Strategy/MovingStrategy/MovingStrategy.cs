﻿using UnityEngine;
using UnityEngine.InputSystem;

public abstract class MovingStrategy: Strategy<MovingStrategy>
{
    private Vector2 _player_move;
    public Vector2 playerMove { get =>_player_move; set => _player_move = value; }

    /**
     * Move the unit according to the current strategy
     */
    public virtual void Move()
    {
        GetComponent<ShooterUnit>().RotateLegs(playerMove);
    }

    /// <summary>
    /// Is called every time player trigger a mouvement input
    /// </summary>
    /// <param name="context">The context object that triggersthe input (given by the Player Input component of the hero </param>
    public void OnMove(InputAction.CallbackContext context)
    {
        playerMove = context.ReadValue<Vector2>().normalized;
    }

    /// <summary>
    /// Handle the collsiion when the player run into something
    /// </summary>
    /// <param name="collision">The collision object the player run into</param>
    public abstract void HandleCollision(Collision2D collision);

    /// <summary>
    /// Return true iif this unit is dashing
    /// </summary>
    /// <returns>true iif this unit is dashing</returns>
    public abstract bool IsDashing();
}
