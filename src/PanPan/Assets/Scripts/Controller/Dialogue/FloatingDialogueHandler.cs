﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FloatingDialogueHandler : MonoBehaviour
{
    [Header("Discussion's flow")]
    [Tooltip("The time (in seconds) a sentence will be displayed before displaying the next one")]
    [SerializeField]
    private float timeBetweenSentences = 1f;
    [Tooltip("The time (in seconds) before the monologue loop again.")]
    [SerializeField]
    private float timeBetweenLoop = 3f;
    [Tooltip("The floating text prefab.")]
    [SerializeField]
    private GameObject floatingTextPrefab;
    private List<GameObject> floatingTexts;

    [Tooltip("The discussion to display.")]
    [SerializeField]
    private Discussion discussion;

    private float dialoguesTimer = 0f;

    private void Awake()
    {
        InitializeFloatingTexts();
    }


    /**
     * Initialize the floating texts of all the speakers
     */
    private void InitializeFloatingTexts()
    {
        // Initialize the floating texts for the current dialogue
        floatingTexts = new List<GameObject>();
        foreach (GameObject speaker in discussion.GetSpeakers())
        {
            GameObject floatingText = Instantiate<GameObject>(floatingTextPrefab);
            floatingText.GetComponent<FloatingText>().Speaker = speaker;
            floatingText.SetActive(false);
            floatingTexts.Add(floatingText);
        }

    }

    private void Update()
    {

        dialoguesTimer -= Time.deltaTime;
        if (dialoguesTimer <= 0)
        {
            if (discussion.IsOver())
            {
                DisableFloatingTexts();
                discussion.Reset();
                dialoguesTimer += timeBetweenLoop;
            }
            else
            {
                DisplaySentence();
                dialoguesTimer += timeBetweenSentences;
            }
        }
    }

    /**
     * Display the currrent sentence in the floating text
     */
    private void DisplaySentence()
    {
        DisableFloatingTexts();
        foreach (Dialogue.DialogueLine line in discussion.GetNextDialogueLines())
        {
            GameObject floatingText = floatingTexts[line.speakerIndex];
            floatingText.SetActive(true);
            floatingText.GetComponent<TextMeshPro>().text = line.sentence;
        }
    }

    /**
     * Disable the floating texts of the dialogues
     */
    private void DisableFloatingTexts()
    {
        foreach (GameObject floatingText in floatingTexts)
        {
            // the speaker is dead so we destroy the dialogue
            if (floatingText == null)
                Destroy(gameObject);
            else
                floatingText.SetActive(false);
        }
    }
}
