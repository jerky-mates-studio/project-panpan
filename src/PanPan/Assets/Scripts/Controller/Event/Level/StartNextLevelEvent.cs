using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartNextLevelEvent
{
    private static StartNextLevelEvent instance;

    public static StartNextLevelEvent GetInstance()
    {
        if (instance == null)
            instance = new StartNextLevelEvent();
        return instance;
    }

    public delegate void StartNextLevelDelegate();
    public event StartNextLevelDelegate NextLevelStarted;

    public void TriggerStartNextLevel() => NextLevelStarted?.Invoke();
}
