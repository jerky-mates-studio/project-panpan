﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AttackStrategy : Strategy<AttackStrategy>
{
    /**
     * Make an attack according to the current strategy
     */
    public abstract void Attack();

}
