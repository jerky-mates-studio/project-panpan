using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashableUnit : MonoBehaviour, IDashCollisionHandler
{
    [Header("Dash effect parameters")]
    [Tooltip("The maximum distance the object will travel after a dash")]
    [SerializeField]
    private float projectionDistance = 5f;
    [Tooltip("The speed at which this unit is projected")]
    [SerializeField]
    private float projectionSpeed = 20f;
    [Tooltip("The dammage dealt when this unit collide another one after being dashed into")]
    [SerializeField]
    private float projectionDamage = 20f;

    public DashCollisionEndState CollisionEndState { get; set; }
    public DashIdleState IdleState { get; set; }
    private DashState currentState;

    public Vector2 ProjectionMovement { get; set; }
    public float ProjectionDistance { get => projectionDistance; }
    public float ProjectionSpeed { get => projectionSpeed; }
    public float ProjectionDamage { get => projectionDamage; }

    public void Awake()
    {
        CollisionEndState = new DashCollisionEndState(this);
        IdleState = new DashIdleState(this);
        SwitchState(IdleState);
    }

    public void Update()
    {
        currentState.UpdateState();
    }

    public void SwitchState(DashState newState)
    {
        currentState = newState;
        currentState.OnEnterState();
    }

    /**
     * This method is called by Unity when this object collides with another
     */
    private void OnCollisionEnter2D(Collision2D collision)
    {
        currentState.HandleCollision(collision);
    }

    /// <summary>
    /// This method is called when a dashing object collide this unit
    /// </summary>
    /// <param name="dash_direction">The direction of the dashing object</param>
    public void OnDashCollision(Vector2 dash_direction)
    {
        DamageUnit(gameObject);
        ProjectionMovement = dash_direction;
        SwitchState(CollisionEndState);
    }

    /// <summary>
    /// Apply projection damages to the gameobject if it is a KillableUnit or a NonShooterUnit
    /// </summary>
    /// <param name="gameObject">The game object to apply damages to</param>
    public void DamageUnit(GameObject gameObject)
    {
        KillableUnit killableUnit = gameObject.GetComponent<KillableUnit>();
        if( killableUnit != null)
        {
            killableUnit.TakeDamages(ProjectionDamage);
        }
        else
        {
            NonShooterUnit nonShooterUnit = gameObject.GetComponent<NonShooterUnit>();
            nonShooterUnit.OnBeingDamaged(ProjectionDamage);
        }
    }
}
