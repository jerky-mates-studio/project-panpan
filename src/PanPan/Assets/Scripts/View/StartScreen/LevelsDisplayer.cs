﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelsDisplayer : MonoBehaviour
{
    [Tooltip("The directory that contains all the maps. This path is relative to the \"Resources\" folder.")]
    public string mapDirectory;
    [Tooltip("The prefab used the create all the GUI buttons.")]
    public GameObject buttonPrefab;
    [Tooltip("The level selection panel.")]
    public GameObject levelSelection;

    [Header("Button placement")]
    [Tooltip("The Y position of the first button. (0; 0) is the top mid of the screen.")]
    public float buttonOffset = -20;
    [Tooltip("The distance between 2 buttons in pixels")]
    public float buttonDistance = 30;

    public GameObject FirstButton {get; set;}

    // Start is called before the first frame update
    void Awake()
    {
        List<string> mapNames = GetMapNames();
        CreateMapButtons(mapNames);
    }

    /**
     * Return the path of each maps (files in "tmx" format) contained in the "mapDirectory" directory
     */
    private List<string> GetMapNames()
    {
        List<string> paths = new List<string>();
        foreach (GameObject map in Resources.LoadAll<GameObject>(mapDirectory))
        {
            paths.Add(map.name);
        }
        return paths;
    }

    /**
     * Create a button in the scroll view for each "mapsPath"
     */ 
    private void CreateMapButtons(List<string> mapNames)
    {
        float currentButtonY = buttonOffset;
        // get the "content" gameobject of the scroll view
        Transform content = transform.GetChild(0).GetChild(0).GetChild(0);
        bool first = true;
        foreach(string mapName in mapNames)
        {
            
            GameObject currentButton =  Instantiate(buttonPrefab, content);
            currentButton.GetComponentsInChildren<Text>()[0].text = mapName;
            currentButton.GetComponent<Button>().onClick.AddListener(() => levelSelection.GetComponent<LevelSelection>().LoadLevelScene(mapName));
            /*
            The position of all the created buttons are handled by the "Vertical Layout Group" of the "content" GameObject.
            The "content" GameObject is automatically resized by its "Content Size Fitter" component.
            */
            /*
             * Select the first button
            */
            if (first)
                FirstButton = currentButton;
        }
        
    }
}
