using System.Collections;
using UnityEngine;

[RequireComponent(typeof(KillableUnit))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(ObjectExplodeEvent))]
public class ExplosiveUnit : MonoBehaviour
{

    [Header("Explosion parameters")]
    [Tooltip("The range of the barrel explosion")]
    [SerializeField]
    private float explosionRange = 3f;
    [Tooltip("The damages dealt by the explosion")]
    [SerializeField]
    private float explosionDamages = 50f;

    private KillableUnit killableUnit;

    // Start is called before the first frame update
    void Start()
    {
        killableUnit = GetComponent<KillableUnit>();
        OnEnable();
    }

    private void OnEnable()
    {
        if( killableUnit is object)
        {
            // We subscribe/unsubscribe in OnEnbale/OnDisbale to be sure to avoid memory leaks and bugs
            killableUnit.OnUnitDie += PrepareExplosion;
        }

    }

    private void OnDisable()
    {
        if (killableUnit is object)
        {
            // We subscribe/unsubscribe in OnEnbale/OnDisbale to be sure to avoid memory leaks and bugs
            killableUnit.OnUnitDie -= PrepareExplosion;
        }
    }

    /// <summary>
    /// Trigger the explosion animation
    /// </summary>
    private void PrepareExplosion()
    {
        GetComponent<Animator>().SetTrigger("explode");
    }

    /// <summary>
    /// Make damages to all in range Units (is called in the animator)
    /// </summary>
    private void Explode()
    {
        // find the center of the barrel
        Vector2 explosionOrigin = transform.position;
        Collider2D[] units = Physics2D.OverlapCircleAll(explosionOrigin, explosionRange, LayerMask.GetMask("Shootable_Unit"));
        foreach(Collider2D unit in units)
        {
            if(unit != null && unit.gameObject != gameObject)
            {
                // create IExplosable interface that takes the amount of damages and the origin of the explosion
                unit.GetComponent<IExplosable>().OnBeginExplosed(explosionDamages);
            }
        }
        //Call the particle system and make the sprite disappear + add ligth
        GetComponentInChildren<ParticleSystem>().Play();

        GetComponent<ObjectExplodeEvent>().TriggerObjectExplode();
    }

    /// <summary>
    /// Clean the game object after the explosion (called in the animator)
    /// </summary>
    private void CleanUnit()
    {
        Destroy(gameObject);
    }


}
