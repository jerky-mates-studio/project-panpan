﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[CreateAssetMenu(menuName = "Model/Dialogue/Discussion")]
[System.Serializable]
public class Discussion : Dialogue
{
    [Header("Discussion's content")]
    [Tooltip("The discussion between the speakers")]
    [SerializeField]
    private Monologue[] replies;
    private int currentReply = 0;

    private List<GameObject> speakers;

    public List<GameObject> Speakers { get => speakers; set => speakers = value; }

    public override List<DialogueLine> GetNextDialogueLines()
    {
        if (IsOver())
            throw new IndexOutOfRangeException("Make the call the \"Reset\" method before using the discussion");
        List<DialogueLine> lines = new List<DialogueLine>();
        Monologue reply = replies[currentReply];
        AddMonologueDialogLine(reply, lines);
        if (reply.IsOver())
            currentReply++;
        return lines;
    }

    public override void Reset()
    {
        currentReply = 0;
        foreach (Monologue monologue in replies)
            monologue.Reset();
    }

    public override bool IsOver()
    {
        return currentReply >= replies.Length;
    }

    /**
     * Return the list of speakers in the dialogue.
     * Each speaker must appear only one time
     */
    public List<GameObject> GetSpeakers()
    {
        return Speakers;
    }

    /**
     * Add the next sentence of the "monologue" to the "dialogLines" if the "monologue" is not over
     */
    private void AddMonologueDialogLine(Monologue monologue, List<DialogueLine> dialogLines)
    {
        string sentence = monologue.GetNextDialogueLines()[0].sentence;
        foreach(int i in monologue.GetSpeakerIndexes())
            dialogLines.Add(new DialogueLine(sentence, i));
    }

    
}
