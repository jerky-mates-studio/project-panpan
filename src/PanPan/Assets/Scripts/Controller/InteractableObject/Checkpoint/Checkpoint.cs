using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(CheckpointEvent))]
public class Checkpoint : MonoBehaviour, ISpawner
{

    [Header("Checkpoint members")]
    [Tooltip("The foreground sprite game object that needs to be animated")]
    [SerializeField]
    private GameObject foreGroundSprite;
    [Tooltip("The light that appear when the spawn begins")]
    [SerializeField]
    private Light2D spawnLight;
    [Tooltip("The total time of the spawning animation")]
    [SerializeField]
    private float spawnTime = 3f;

    [Header("Idle animation parameter")]
    [Tooltip("The wavelength of the sine animation")]
    [SerializeField]
    private float idleWavelength = 5.0f;
    [Tooltip("The speed of the sine animation")]
    [SerializeField]
    private float idleWaveSpeed = 0.7f;

    [Header("Active animation parameter")]
    [Tooltip("The wavelength of the sine animation")]
    [SerializeField]
    private float activeWavelength = 10.0f;
    [Tooltip("The speed of the sine animation")]
    [SerializeField]
    private float activeWaveSpeed = -1.2f;

    [Header("Light animation parameter")]
    [Tooltip("The number of frame of the animation")]
    [SerializeField]
    private int lightAnimationStepsByPhase = 10;
    [Tooltip("The max value the \"pointLightInnerRadius\" must reach during the 1st phase")]
    [SerializeField]
    private float lightInnerRadiusMax = 0.3f;

    private SpriteRenderer spriteRenderer;
    private MaterialPropertyBlock propertyBlock;

    private bool isActivated = false;

    private void Awake()
    {
        foreGroundSprite.SetActive(false);
        spawnLight.enabled = false;
    }


    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = foreGroundSprite.GetComponent<SpriteRenderer>();
        propertyBlock = new MaterialPropertyBlock();
        spriteRenderer.GetPropertyBlock(propertyBlock);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Spawn(ShooterUnit unit)
    {
        StartCoroutine(StartSpawningAnimation(unit));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Hero>() != null)
        {
            CheckpointManager.GetInstance().OnCheckpointActivated(GetComponent<ISpawner>());
        }
        
    }

    public void OnSpawnerActivated()
    {
        if (isActivated)
            return;

        foreGroundSprite.SetActive(true);
        GetComponent<CheckpointEvent>().TriggerCheckpointActivated();
        isActivated = true;
    }

    public void OnSpawnerDeactivated()
    {
        foreGroundSprite.SetActive(false);
        isActivated = false;
    }

    private IEnumerator StartSpawningAnimation(ShooterUnit unit)
    {
        unit.transform.position = transform.position;
        SetActiveMaterialProperties();
        StartCoroutine(StartLightAnimation());
        GetComponent<CheckpointEvent>().TriggerCheckpointStartRespawning();

        yield return new WaitForSeconds(spawnTime);

        SetIdleMaterialProperties();
        unit.MakeFullLife();
        GetComponent<CheckpointEvent>().TriggerCheckpointStopRespawning();
    }

    private IEnumerator StartLightAnimation()
    {
        spawnLight.enabled = true;

        float increment = lightInnerRadiusMax / lightAnimationStepsByPhase;

        // We take half of spawnTime because we turn on the light, then turn 2 off (2 phases)
        // We divide again by the number of steps to have the time between 2 steps
        float animationStepTime = spawnTime * 0.5f / lightAnimationStepsByPhase;

        // We turn on the light crescendo
        for (int i = 0; i < lightAnimationStepsByPhase; i++)
        {
            spawnLight.pointLightInnerRadius += increment;
            yield return new WaitForSeconds(animationStepTime);
        }

        // We turn off the light crescendo
        for (int i = 0; i < lightAnimationStepsByPhase; i++)
        {
            spawnLight.pointLightInnerRadius -= increment;
            yield return new WaitForSeconds(animationStepTime);
        }

        spawnLight.enabled = false;
    }

    /// <summary>
    /// Set the material property values to the active parameter
    /// </summary>
    private void SetActiveMaterialProperties()
    {
        propertyBlock.SetFloat("_Wavelength", activeWavelength);
        propertyBlock.SetFloat("_WaveSpeed", activeWaveSpeed);
        spriteRenderer.SetPropertyBlock(propertyBlock);
    }

    /// <summary>
    /// Set the material property values to the idle parameter
    /// </summary>
    private void SetIdleMaterialProperties()
    {
        propertyBlock.SetFloat("_Wavelength", idleWavelength);
        propertyBlock.SetFloat("_WaveSpeed", idleWaveSpeed);
        spriteRenderer.SetPropertyBlock(propertyBlock);

    }
}
