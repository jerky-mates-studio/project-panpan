using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitTookDamagesEvent))]
public class TintDamageFeedback : MonoBehaviour, IDamageFeedback
{
    [Tooltip("The game object that contains the sprite render to use to tint the object")]
    [SerializeField]
    private GameObject sprite;
    [Tooltip("The color to use for the damage feedback")]
    [SerializeField]
    private Color damageTint = Color.red;
    [Tooltip("The duration of the damage tint (in seconds)")]
    [Min(0f)]
    [SerializeField]
    private float tintDuration = 0.2f;

    private SpriteRenderer spriteRenderer;

    private Color normalTint = Color.white;

    private void Awake()
    {
        spriteRenderer = sprite.GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        GetComponent<UnitTookDamagesEvent>().UnitTookDamages += TriggerDamageFeedback;
    }

    private void OnDisable()
    {
        GetComponent<UnitTookDamagesEvent>().UnitTookDamages -= TriggerDamageFeedback;
    }

    public void TriggerDamageFeedback(float damages)
    {
        spriteRenderer.color = damageTint;
        StartCoroutine(DeactivateDamageTint());
    }


    private IEnumerator DeactivateDamageTint()
    {
        yield return new WaitForSeconds(tintDuration);
        spriteRenderer.color = normalTint;
    }
}
