using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IExplosable
{
    /// <summary>
    /// Is called each time this unit is in the range of an explosion
    /// </summary>
    /// <param name="damages">The damages dealt by the explosion</param>
    void OnBeginExplosed(float damages);

}
