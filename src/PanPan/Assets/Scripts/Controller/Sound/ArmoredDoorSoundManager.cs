using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ArmoredDoorMovingEvent))]
[RequireComponent(typeof(AudioSource))]
public class ArmoredDoorSoundManager : MonoBehaviour
{
    [Tooltip("The clip to play when the armored door is opening")]
    [SerializeField]
    private AudioClip openingSound;
    [Tooltip("The clip to play when the armored door is closing")]
    [SerializeField]
    private AudioClip closingSound;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();    
    }

    private void OnEnable()
    {
        GetComponent<ArmoredDoorMovingEvent>().ArmoredDoorOpening += PlayOpeningSound;
        GetComponent<ArmoredDoorMovingEvent>().ArmoredDoorClosing += PlayClosingSound;
    }

    private void OnDisable()
    {
        GetComponent<ArmoredDoorMovingEvent>().ArmoredDoorOpening -= PlayOpeningSound;
        GetComponent<ArmoredDoorMovingEvent>().ArmoredDoorClosing -= PlayClosingSound;
    }

    private void PlayOpeningSound()
    {
        audioSource.clip = openingSound;
        audioSource.Play();
    }

    private void PlayClosingSound()
    {
        audioSource.clip = closingSound;
        audioSource.Play();
    }
}
