using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDiedEvent
{
    private static EnemyDiedEvent instance;

    public static EnemyDiedEvent GetInstance()
    {
        if (instance == null)
            instance = new EnemyDiedEvent();
        return instance;
    }

    public delegate void EnemyDiedDelegate();
    public event EnemyDiedDelegate EnemyDied;

    public void TriggerEnemyDied() => EnemyDied?.Invoke();
}
