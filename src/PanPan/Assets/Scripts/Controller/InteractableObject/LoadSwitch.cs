using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LoadSwitchEvent))]
public class LoadSwitch : MonoBehaviour, IShootable
{
    [Header("Load parameters")]
    [SerializeField]
    [Tooltip("The energy load the switch must take to be activated")]
    private float maximumLoad = 100f;
    [SerializeField]
    [Tooltip("The rate at which the switch loss the load in energy/sec")]
    private float loadLossRate = 35f;

    [Header("Load gauge parameters")]
    [SerializeField]
    [Tooltip("The image component that display the current energy load")]
    private GameObject gaugeComponent;
    [SerializeField]
    [Tooltip("The complete UI component that contains the gauge")]
    private GameObject gaugeUI;
    [Tooltip("The type of bullet that will trigger the Switch")]
    [SerializeField]
    private BulletType bulletType;

    // The current energy load of the switch
    private float currentLoad = 0f;
    private Image gaugeImage;

    private LoadSwitchEvent loadSwitchEvent;

    public BulletType BulletType { get => bulletType; set => bulletType = value; }

    private void Awake()
    {
        gaugeImage = gaugeComponent.GetComponent<Image>();
        GetComponent<SpriteRenderer>().material.SetColor("_Color", bulletType.BulletColor);

        loadSwitchEvent = GetComponent<LoadSwitchEvent>();
    }

    public void OnBeingShoot(Bullet bullet)
    {
        if (!IsActivated() && bulletType.ShooterTag == bullet.Type.ShooterTag)
        {
            currentLoad += bullet.Damage;
            if (IsActivated())
            {
                // Invoke the event if there are subscriber and only if the Switch is activated
                loadSwitchEvent.TriggerSwitchActivated(true);
            }
            else
                loadSwitchEvent.TriggerSwitchLoading(currentLoad / maximumLoad);
        }
    }

    /// <summary>
    /// Return true iif the switch is activated i.e. if the current load is equal or greater to the maximum load
    /// </summary>
    /// <returns>True iif the switch is activated</returns>
    public bool IsActivated()
    {
        return currentLoad >= maximumLoad;
    }

    /// <summary>
    /// Reset the load of the switch to 0 which deactivate it if it was activated.
    /// Trigger an event if the state switch from 'activated' to 'deactivated'.
    /// </summary>
    public void ResetLoad()
    {
        bool hasActivationStateChanged = IsActivated();
        currentLoad = 0f;

        if (hasActivationStateChanged)
            loadSwitchEvent.TriggerSwitchActivated(false);
    }

    /// <summary>
    /// Totally fill the load of the switch which activate it if it was deactivated.
    /// Trigger an event if the state switch from 'deactivated' to 'activated'.
    /// </summary>
    public void FillLoad()
    {
        bool hasActivationStateChanged = !IsActivated();
        currentLoad = maximumLoad;
        
        if (hasActivationStateChanged)
            loadSwitchEvent.TriggerSwitchActivated(true);
    }

    /// <summary>
    /// Lose some load over time according to the "loadLossRate".
    /// The switch stop loosing load after it has reached its maximum load 
    /// </summary>
    private void LoseLoad()
    {
        if (!IsActivated() && currentLoad > 0)
        {
            currentLoad = Mathf.Max(0f, currentLoad - loadLossRate * Time.deltaTime);
        }
    }

    private void UpdateGaugeDisplay()
    {
        float loadPercent = currentLoad / maximumLoad;
        gaugeImage.fillAmount = loadPercent;
        if ( IsActivated() || currentLoad <= 0f)
        {
            if (gaugeUI.activeSelf)
                gaugeUI.SetActive(false);
        }
        else if ( !IsActivated() && currentLoad > 0f && !gaugeUI.activeSelf)
            gaugeUI.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        LoseLoad();
        UpdateGaugeDisplay();
    }
}
