﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Bullet : MonoBehaviour
{
    [Header("Gun feature")]
    [Tooltip("The minimum time between 2 shoots in seconds")]
    public float timeBetweenBullet = 0.2f;
    [Tooltip("The maximum error between where the player aim and the bullet direction in degrees")]
    [Range(0, 180)]
    public float maximumSpread = 0;
    [Tooltip("The sound to play when a bullet is fired")]
    public AudioClip gunSound;

    [Header("Bullet features")]
    [Tooltip("The speed of the bullet in unit/seconds")]
    public float speed = 100;
    [Tooltip("The maximal range of the bullet")]
    public float range = 100;
    [Tooltip("The amount of damages the bullet will do to the target")]
    public float damage = 20;

    private BulletType bulletType;
    private float travelledDistance = 0;

    public float Damage { get => damage; set => damage = value; }
    public BulletType Type { get => bulletType; }

    private void Start()
    {
        GetComponent<SoundManager>().RandomizePitch(gunSound);
    }

    /**
     * Apply the color of the bullet type to the different component of the bullet
     */
    private void PaintBullet()
    {
        gameObject.GetComponentInChildren<SpriteRenderer>().color = bulletType.BulletColor;
        gameObject.GetComponentInChildren<Light2D>().color = bulletType.BulletColor;
        gameObject.GetComponentInChildren<TrailRenderer>().startColor = bulletType.BulletColor;
        gameObject.GetComponentInChildren<TrailRenderer>().endColor = bulletType.BulletColor;
    }

    /**
     * Set the new bullet type and apply the new color to the bullet
     */
    public void SetBulletType(BulletType bulletType)
    {
        this.bulletType = bulletType;
        PaintBullet();
    }

    /**
     * The getter of the "shooterTag" variable
     */
    public string GetShooterTag()
    {
        return bulletType.ShooterTag;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    /**
     * Move the bullet in straight line and destroy it if it travelled too far
     */ 
    void Move()
    {
        if (travelledDistance >= range)
        {
            Destroy(gameObject);
        }
        else
        {
            float distance = speed * Time.deltaTime;
            Vector3 movement = Vector3.right * distance;
            travelledDistance += distance;
            transform.Translate(movement);
        }
    }

    /**
     * This method is called by Unity when this object collides with another
     */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Only collide with the objects on the "Unit" layer and with unit which tag is different
        // from the shooter (Enemy can't shoot another Enemy + Unit can't shoot itself)
        if (collision.tag != bulletType.ShooterTag && collision.gameObject.layer == LayerMask.NameToLayer("Shootable_Unit"))
        {
            collision.gameObject.GetComponent<IShootable>().OnBeingShoot(this);
            Destroy(gameObject);
        }
        else if(collision.gameObject.layer == LayerMask.NameToLayer("Block_Bullet"))
        {
            Destroy(gameObject);
        }

    }

    /**
     * Compute the rotation of the next bullet to be shoot. This bullet will have the same rotation as
     * the "shooterRotation" but with a maximal error of "maximumSpread".
     */
    public Quaternion GetBulletRotation(Quaternion shooterRotation)
    {
        
        float error = UnityEngine.Random.Range(-maximumSpread, maximumSpread);
        float degreeRotation = shooterRotation.eulerAngles.z + error;
        return Quaternion.AngleAxis(degreeRotation, Vector3.forward);
    }

}
