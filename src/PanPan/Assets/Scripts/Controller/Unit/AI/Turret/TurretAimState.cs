using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAimState : AIState
{
    private TurretAI Context { get; set; }
    private float timer = 0f;
    private Turret enemy;

    public TurretAimState(TurretAI context) : base(context)
    {
        Context = context;
        enemy = context.GetComponent<Turret>();

    }
    public override void OnEnterState()
    {
        timer = 0f;
        Context.Laser.ActivateLaser(true);
    }

    public override void UpdateState()
    {
        if (!isPlayerOnSight())
        {
            Context.Laser.ActivateLaser(false);
            Context.SwitchState(Context.IdleState);
        }
        else
        {
            enemy.LookAt(Player.transform.position);
            timer += Time.deltaTime;
            if (timer >= Context.AimTime)
            {
                Context.Laser.ActivateLaser(false);
                Context.SwitchState(Context.ShootState);
            }
        }
    }
}
