using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllSwitchesLoadedCondition : MultipleSwitchLoadedCondition, ISwitchLoadedCondition
{
    [Header("Condition parameter")]
    [SerializeField]
    [Min(0f)]
    [Tooltip("The LoadSwitch gameobject that must be loaded so that this condition can be true")]
    private GameObject[] loadSwitches;

    private void Awake()
    {
        AreLoadSwitchesValid();
    }

    private void OnEnable()
    {
        foreach (GameObject loadSwitch in loadSwitches)
            loadSwitch.GetComponent<LoadSwitchEvent>().SwitchActivated += HandleSwitchActivation;
    }

    private void OnDisable()
    {
        foreach (GameObject loadSwitch in loadSwitches)
            loadSwitch.GetComponent<LoadSwitchEvent>().SwitchActivated -= HandleSwitchActivation;
    }

    /// <summary>
    /// Handle the activation/deactivation of a LoadSwitch
    /// </summary>
    /// <param name="isActivated">True iif the switch has been activated, false if it has beeen deactivated</param>
    private void HandleSwitchActivation(bool isActivated)
    {
        if (isActivated)
        {
            if (AreAllSwitchesActivated())
            {
                this.IsActivated = true;
                TriggerActivation(true);
            }
        }
        else
            ResetAllSwicthes();
    }


    // MultipleSwitchLoadedCondition interfaces

    protected override GameObject[] GetLoadSwitches()
    {
        return loadSwitches;
    }

    //------------------------------------------

    // ISwitchLoadedCondition interfaces

    public void Deactivate()
    {
        ResetAllSwicthes();
    }

    public void Activate()
    {
        FillAllSwitches();
    }

    //------------------------------------------
}
