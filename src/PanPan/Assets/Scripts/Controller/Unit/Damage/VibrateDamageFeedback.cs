using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitTookDamagesEvent))]
public class VibrateDamageFeedback : MonoBehaviour, IDamageFeedback
{
    [Tooltip("The gameobject to make vibrate when the unit took damages")]
    [SerializeField]
    private GameObject body;

    [Header("Vibration")]
    [Tooltip("The frequency (rapidity) of the vibration (greater == more quickly, lesser == slower")]
    [Min(0f)]
    [SerializeField]
    private float frequency = 10f;
    [Tooltip("The maximum distance of the game object with its initial position while vibrating (in Unity unit)")]
    [Min(0f)]
    [SerializeField]
    private float maxAmplitude = 0.25f;
    [Tooltip("How much time the game object will vibrate ? (in seconds)")]
    [Min(0f)]
    [SerializeField]
    private float vibrationTime = 0.2f;
    [Tooltip("Vibrates along the Y axis if true, vibrates along the X axis otherwise")]
    [SerializeField]
    private bool isYVibration = true;

    private float vibrationTimer = 0f;
    private Vector3 initialPosition;

    private void Start()
    {
        initialPosition = body.transform.localPosition;
    }

    private void OnEnable()
    {
        GetComponent<UnitTookDamagesEvent>().UnitTookDamages += TriggerDamageFeedback;
    }

    private void OnDisable()
    {
        GetComponent<UnitTookDamagesEvent>().UnitTookDamages -= TriggerDamageFeedback;
    }

    private void Update()
    {
        if (vibrationTimer > 0f)
        {
            ProcessVibration();
            vibrationTimer -= Time.deltaTime;
        }
        else
            StopVibration();
    }

    /// <summary>
    /// Stops the vibration of the 'body' game object
    /// </summary>
    private void StopVibration()
    {
        body.transform.localPosition = initialPosition;
    }

    /// <summary>
    /// Make the 'body' game object vibrates along the Y axis if 'isYVibration', along the X axis otherwise
    /// </summary>
    private void ProcessVibration()
    {
        float movement = maxAmplitude * Mathf.Sin(frequency * Time.realtimeSinceStartup);
        Vector3 newPosition;
        if (isYVibration)
            newPosition = initialPosition + new Vector3(0f, movement, 0f);
        else
            newPosition = initialPosition + new Vector3(movement, 0f, 0f);
        body.transform.localPosition = newPosition;
    }

    // IDamageFeedback interfaces

    public void TriggerDamageFeedback(float damages)
    {
        vibrationTimer = vibrationTime;
    }

    // ---------------------------
}
