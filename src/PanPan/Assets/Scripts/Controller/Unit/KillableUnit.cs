using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitTookDamagesEvent))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class KillableUnit : MonoBehaviour, IShootable
{

    [Header("Death settings")]
    [Tooltip("The number current amount of life points left")]
    [SerializeField]
    private float curretLifePoints = 100f;
    [Tooltip("The maximum amount of life points for this unit")]
    [SerializeField]
    private float maxLifePoints = 100f;

    public delegate void DeathTrigger();
    public event DeathTrigger OnUnitDie;

    private UnitTookDamagesEvent unitTookDamagesEvent;


    private void Awake()
    {
        unitTookDamagesEvent = GetComponent<UnitTookDamagesEvent>();
    }

    /// <summary>
    /// Check if the unit has some life points left
    /// </summary>
    /// <returns>True iif if this units has still some life points</returns>
    public bool IsDead()
    {
        return curretLifePoints <= 0f;
    }

    /// <summary>
    /// Make the unit loose some life points
    /// </summary>
    /// <param name="damages">The amount of damages to apply to this unit</param>
    public void TakeDamages(float damages)
    {
        if( !IsDead())
        {
            curretLifePoints -= damages;
            if ( IsDead())
            {
                // Trigger the death of the unit
                OnUnitDie?.Invoke();
            }
        }
        unitTookDamagesEvent.NotifyUnitTookDamages(damages);
    }

    public void OnBeingShoot(Bullet bullet)
    {
        TakeDamages(bullet.Damage);
    }
}
