﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRecorder : MonoBehaviour
{

    private static CameraRecorder instance;
    public static CameraRecorder Instance()
    {
        return instance;
    }

    private Camera mainCamera;
    private RenderTexture _texture;

    public RenderTexture texture {set => _texture = value; }

    private void Awake()
    {
        if(Instance() == null)
            instance = this;
        InitTextureRecorder();
    }

    private void Start()
    {
        mainCamera = Camera.main;
    }

    private void OnEnable()
    {
        InitTextureRecorder();
    }

    private void InitTextureRecorder()
    {
        texture = new RenderTexture(Screen.width, Screen.height, 24);
        GetComponent<Camera>().targetTexture = _texture;
    }
}
