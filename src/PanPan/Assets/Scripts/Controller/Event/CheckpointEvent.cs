using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointEvent : MonoBehaviour
{
    // Checkpoint activated event
    public delegate void CheckpointActivatedDelegate();
    public event CheckpointActivatedDelegate CheckpointActivated;
    public void TriggerCheckpointActivated() => CheckpointActivated?.Invoke();

    // Checkpoint start respawning event
    public delegate void CheckpointStartRespawningDelegate();
    public event CheckpointStartRespawningDelegate CheckpointStartRespawning;
    public void TriggerCheckpointStartRespawning() => CheckpointStartRespawning?.Invoke();

    // Checkpoint stop respawning event
    public delegate void CheckpointStopRespawningDelegate();
    public event CheckpointStopRespawningDelegate CheckpointStopRespawning;
    public void TriggerCheckpointStopRespawning() => CheckpointStopRespawning?.Invoke();
}
