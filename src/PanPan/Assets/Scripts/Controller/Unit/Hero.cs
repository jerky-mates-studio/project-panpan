﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Walk))]
[RequireComponent(typeof(Dash))]
[RequireComponent(typeof(Fire))]
[RequireComponent(typeof(SlowTime))]
[RequireComponent(typeof(RealTime))]
public class Hero : ShooterUnit
{
    private static GameObject player;
    public static GameObject Player()
    {
        return player;
    }

    [Header("Sound settings")]
    [Tooltip("the sounds to play when the player heal himself")]
    public AudioClip healedSound;

    [Header("Hero Upgrade")]
    [Tooltip("The \"HealEfficiency\" upgrade")]
    [SerializeField]
    private Upgrade healEfficiency;
    [Tooltip("The \"RiffleDamage\" upgrade")]
    [SerializeField]
    private Upgrade riffleDamage;

    private MovingStrategy currentMovingStrategy;
    private AttackStrategy currentAttackStrategy;
    private TimeStrategy currentTimeStrategy;


    private bool useMouseCamera = true;
    private Vector3 mousePosition = new Vector3(0, 0, 0);
    private Vector3 gamepadRotation = new Vector3(0, 0, 0);
    private bool portalReached = false;


    // The { get; set; } properties will generate getter and setter for the implicit private variable "inventory"
    // This is explain here: https://stackoverflow.com/questions/6709072/c-getter-setter
    public Dictionary<Material, int> Inventory { get; set; } = new Dictionary<Material, int>();

    public PlayerController PlayerController { get; set; }
    public InputActionMap PlayerActionMap { get; set; }
    public InputActionMap UIActionMap { get; set; }
    public TimeStrategy CurrentTimeStrategy { get => currentTimeStrategy;}

    private void Reset()
    {
        // Initialize the moving strategies
        Walk walk = gameObject.AddComponent<Walk>();
        Dash dash = gameObject.AddComponent<Dash>();
        Fire fire = gameObject.AddComponent<Fire>();
        Blade blade = gameObject.AddComponent<Blade>();
        
        dash.SetUnlocked(false);
        blade.SetUnlocked(false);
        

        bullet = Resources.Load<GameObject>("prefabs/Unit/RiffleBullet");
    }

    protected override void Awake()
    {
        base.Awake();
        if (Player() == null)
            player = gameObject;
        else
        {
            Destroy(player);
            player = gameObject;
        }
        PlayerController = new PlayerController();
        GetComponent<PlayerInput>().SwitchCurrentActionMap("UI");
        UIActionMap = GetComponent<PlayerInput>().currentActionMap;
        GetComponent<PlayerInput>().SwitchCurrentActionMap("Player");
        PlayerActionMap = GetComponent<PlayerInput>().currentActionMap;

        // initialize inventory
        foreach (Material material in Resources.LoadAll<Material>(Material.materialPath))
            Inventory.Add(material, 0);
        
        Dash dash = gameObject.GetComponent<Dash>();
        Blade blade = gameObject.GetComponent<Blade>();
        SlowTime slowTime = gameObject.GetComponent<SlowTime>();

        dash.SetUnlocked(false);
        blade.SetUnlocked(false);
        slowTime.SetUnlocked(false);
        

        InitialiazeStrategy(gameObject.GetComponent<Walk>(), gameObject.GetComponent<Dash>(), out currentMovingStrategy);
        InitialiazeStrategy(gameObject.GetComponent<Fire>(), gameObject.GetComponent<Blade>(), out currentAttackStrategy);
        InitialiazeStrategy(gameObject.GetComponent<RealTime>(), gameObject.GetComponent<SlowTime>(), out currentTimeStrategy);
    }

    private void OnEnable()
    {
        PlayerController.Enable();
    }

    private void OnDisable()
    {
        PlayerController.Disable();
    }

    private void FixedUpdate()
    {
        if (!IsDead())
        {
            Rotate();
            currentMovingStrategy.Move();
            currentTimeStrategy.UpdateTime();
        }

    }

    public override bool IsDashing()
    {
        return currentMovingStrategy.IsDashing();
    }

    public override GameObject CreateBullet()
    {
        GameObject newBullet = base.CreateBullet();
        newBullet.GetComponent<Bullet>().Damage = riffleDamage.CurrentValue;
        return newBullet;
    }

    protected override void Update()
    {
        base.Update();
        currentMovingStrategy = currentMovingStrategy.UpdateStrategy();
        currentAttackStrategy = currentAttackStrategy.UpdateStrategy();
        currentTimeStrategy = currentTimeStrategy.UpdateStrategy();
        timer += currentTimeStrategy.GetDeltaTime();

        if (!IsDead())
        {
            // Move "Attack()" in the Update() method because the shoot rate is very time sensitive and need as many updates as possible
            currentAttackStrategy.Attack();
        }
    }

    /*
     * Rotate the hero on order to always make him "look" toward the mouse
     */
    private void Rotate()
    {
        if (!useMouseCamera)
        {
            Vector3 lookPoint = transform.position + gamepadRotation;
            base.LookAt(lookPoint);
        }
        else if (useMouseCamera && Camera.main != null)
        {
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            base.LookAt(mouseWorldPosition);
        }
    }

    /**
     * Is called when the position of the mouse change
     */
    public void OnLookMouse(InputAction.CallbackContext context)
    {
        mousePosition = context.ReadValue<Vector2>();
        useMouseCamera = true;
    }

    /**
     * Is called when the value of the "look gamepad" input change
     */
    public void OnLookGamepad(InputAction.CallbackContext context)
    {
        Vector2 rotation = context.ReadValue<Vector2>();
        // Ensure the player still look at the same point when the joystick is in "rest" state (aka (0,0) position)
        if(rotation != new Vector2(0, 0))
            gamepadRotation = new Vector3(rotation.x, rotation.y, 0);
        useMouseCamera = false;
    }

    /**
     * Incerase the value of "currentHP" by "healValue". The new value of "currentHP" will be at most
     * the same as the "maximumHP" value.
     */
    public void Heal(float healValue)
    {
        GetComponent<SoundManager>().RandomizePitch(healedSound);
        currentHP = Math.Min(maximumHP, currentHP + (healEfficiency.CurrentValue * healValue));
        // Ensure the rigidbody is enabled if the player is alive
        if ( !IsDead() )
            gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
    }

    public override void MakeFullLife()
    {
        base.MakeFullLife();
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
    }

    /**
     * Add the items "lootName" to the inventory. The number of item added is equal to "amount"
     */
    public void PickUpLoot(Material loot, int amount)
    {
        Inventory[loot] += amount;
    }

    protected override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        // Disable the rigidbody if the player is dead
        if (IsDead())
        {
            gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            PlayerDiedEvent.GetInstance().TriggerPlayerDied(this);
        }
    }

    /**
     * Set the "portalReached" value to "true". Called by the portals when they are reached.
     */
    public void ReachPortal()
    {
        portalReached = true;
    }

    /**
     * Return true iif the portal has been  reached
     */
    public bool HasReachedPortal()
    {
        return portalReached;
    }

    /// <summary>
    /// This method is called by Unity when this object collides with another
    /// </summary>
    /// <param name="collision">The object the hero collided with</param>
    private void OnCollisionStay2D(Collision2D collision)
    {
        //let the current moving strategy handles the collission
        currentMovingStrategy.HandleCollision(collision);
    }
}
