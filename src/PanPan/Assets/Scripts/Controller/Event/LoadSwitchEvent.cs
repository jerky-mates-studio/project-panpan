using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSwitchEvent : MonoBehaviour
{
    public delegate void SwitchLoadingDelegate(float loadingPercentage);
    public event SwitchLoadingDelegate SwitchLoading;

    public void TriggerSwitchLoading(float loadingPercentage) => SwitchLoading?.Invoke(loadingPercentage);


    public delegate void SwitchActivatedDelegate(bool isLoaded);
    public event SwitchActivatedDelegate SwitchActivated;

    public void TriggerSwitchActivated(bool isLoaded) => SwitchActivated?.Invoke(isLoaded);
}
