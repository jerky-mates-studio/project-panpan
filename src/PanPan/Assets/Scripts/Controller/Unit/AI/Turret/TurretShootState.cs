using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum ShootSubstate
{
    PAUSE_BEFORE,
    SHOOTING,
    PAUSE_AFTER
}

public class TurretShootState : AIState
{
    private Turret enemy;
    private float timer = 0f;
    private float timeBetweenBullet;
    private int bulletShoot = 0;
    private ShootSubstate shootState = ShootSubstate.PAUSE_BEFORE;

    private TurretAI Context { get; set; }

    public TurretShootState(TurretAI context) : base(context)
    {
        enemy = context.GetComponent<Turret>();
        timeBetweenBullet = enemy.bullet.GetComponent<Bullet>().timeBetweenBullet;
        Context = context;
    }

    public override void OnEnterState()
    {
        bulletShoot = 0;
        timer = 0f;
        shootState = ShootSubstate.PAUSE_BEFORE;
        return;
    }

    public override void UpdateState()
    {
        timer += Time.deltaTime;

        if (shootState == ShootSubstate.PAUSE_BEFORE)
        {
            if(timer >= Context.PauseTime)
            {
                timer -= Context.PauseTime;
                shootState = ShootSubstate.SHOOTING;
            }
        }        
        else if (shootState == ShootSubstate.SHOOTING)
        {
            if(timer >= timeBetweenBullet)
            {
                timer -= timeBetweenBullet;
                enemy.Fire();
                bulletShoot++;
                if (bulletShoot >= Context.LasersInSalve)
                {
                    bulletShoot = 0;
                    shootState = ShootSubstate.PAUSE_AFTER;
                }
            }

        }
        else if(shootState == ShootSubstate.PAUSE_AFTER)
        {
            if (timer >= Context.PauseTime)
            {
                Context.SwitchState(Context.AimState);
            }
        }
    }

}
