using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActivationCondition : MonoBehaviour
{
    public delegate void ActivationTrigger(bool isActivated);
    public event ActivationTrigger OnConditionMet;

    [Tooltip("False if this condiation needs to wait another condition before being active. True otherwise.")]
    [SerializeField]
    private bool isConditionActive = true;

    /// <summary>
    /// Return true iif this condition is activated
    /// </summary>
    /// <returns>True iif this condition is activated</returns>
    public bool IsConditionActive() => isConditionActive;

    /// <summary>
    /// Trigger the "OnConditionMet" if there are object subscribed to it
    /// </summary>
    /// <param name="isActivated">True iif the object must be activated, false if the object must be deactivated</param>
    protected void TriggerActivation(bool isActivated)
    {
        if (isConditionActive)
            OnConditionMet?.Invoke(isActivated);
    }

    /// <summary>
    /// Activate or deactivated this condition
    /// </summary>
    /// <param name="isActive">True iif this condition must be activated, False otherwise</param>
    public void ActivateCondition(bool isActive)
    {
        isConditionActive = isActive;
    }

}
