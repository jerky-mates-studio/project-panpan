using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;

public class TestDashableObject
{
    [UnityTest]
    /**
     * Test if the door object is destroyed after the break occurs
     */
    public IEnumerator BreakableDoorDisappear()
    {
        // Initilize objects
        GameObject breakableDoor = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Map/BreakableDoor"));

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(1, 1));
        yield return new WaitForSeconds(8f);
        Assert.IsTrue(breakableDoor == null);

        // Destroy the object in case the test fails
        Object.Destroy(breakableDoor);
    }

    [Test]
    /**
     * Test if the door object is destroyed after the break occurs
     */
    public void BreakableDoorLaunchSplinterInGoodDirection()
    {
        // Initilize objects
        GameObject breakableDoor = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Map/BreakableDoor"));
        GameObject particleSystem = breakableDoor.GetComponentInChildren<ParticleSystem>().gameObject;

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(1, 0));
        Assert.AreEqual(0, particleSystem.transform.rotation.eulerAngles.z, 1);

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(1, 1));
        Assert.AreEqual(45, particleSystem.transform.rotation.eulerAngles.z, 1);
        particleSystem.transform.rotation = Quaternion.Euler(0, 0, 0);

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(0, 1));
        Assert.AreEqual(90, particleSystem.transform.rotation.eulerAngles.z, 1);
        particleSystem.transform.rotation = Quaternion.Euler(0, 0, 0);

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(-1, 1));
        Assert.AreEqual(135, particleSystem.transform.rotation.eulerAngles.z, 1);
        particleSystem.transform.rotation = Quaternion.Euler(0, 0, 0);

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(-1, 0));
        Assert.AreEqual(180, particleSystem.transform.rotation.eulerAngles.z, 1);
        particleSystem.transform.rotation = Quaternion.Euler(0, 0, 0);

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(-1, -1));
        Assert.AreEqual(225, particleSystem.transform.rotation.eulerAngles.z, 1);
        particleSystem.transform.rotation = Quaternion.Euler(0, 0, 0);

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(0, -1));
        Assert.AreEqual(270, particleSystem.transform.rotation.eulerAngles.z, 1);
        particleSystem.transform.rotation = Quaternion.Euler(0, 0, 0);

        breakableDoor.GetComponent<BreakableDoor>().OnDashCollision(new Vector2(1, -1));
        Assert.AreEqual(315, particleSystem.transform.rotation.eulerAngles.z, 1);
        particleSystem.transform.rotation = Quaternion.Euler(0, 0, 0);

        // Destroy the object in case the test fails
        Object.Destroy(breakableDoor);
    }
}
