﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTimeCooldownTracker : CooldownTracker
{
    public override float GetElapsedCooldown()
    {
        return GetTotalCooldown() - Hero.Player().GetComponent<SlowTime>().GetRemainingTime();
    }

    public override float GetTotalCooldown()
    {
        return Hero.Player().GetComponent<SlowTime>().GetCooldown();
    }

}
