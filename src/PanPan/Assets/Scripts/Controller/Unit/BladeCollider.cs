﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeCollider : MonoBehaviour
{
    [Header("Blade upgrades")]
    [Tooltip("The \"SwordDamage\" update")]
    [SerializeField]
    private Upgrade swordDamage;
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        // Only collide with the objects on the "Unit" layer and with unit which tag is different
        // from the shooter (Enemy can't shoot another Enemy + Unit can't shoot itself)
        if (collision.gameObject.layer == LayerMask.NameToLayer("Shootable_Unit"))
        {
            collision.gameObject.GetComponent<NonShooterUnit>().OnBeingDamaged(swordDamage.CurrentValue);
        } else if(collision.gameObject.layer == LayerMask.NameToLayer("Unit") && collision.CompareTag("Bullet") && collision.gameObject.GetComponent<Bullet>().GetShooterTag() != "Player" )
        {
            // Only collide with the bullets that are not shoot by the player
            collision.transform.Rotate(new Vector3(0, 0, 180));
            collision.gameObject.GetComponent<Bullet>().SetBulletType(GetComponentInParent<ShooterUnit>().BulletType);
        }

    }
}
