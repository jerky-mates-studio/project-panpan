﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendorNPC : InteractableNPC
{
    [Tooltip("The dialogue to start when the player want to quit the shop")]
    [SerializeField]
    private Discussion goodbyeDialogue;
    [Tooltip("The list of upgrades that the vendor sales")]
    [SerializeField]
    private List<Upgrade> upgrades;

    private VendorPanel vendorPanel;

    public override void Start()
    {
        base.Start();
        goodbyeDialogue.Speakers = base.GetSpeakers();
        vendorPanel = VendorPanel.Instance.GetComponent<VendorPanel>();
    }

    /// <summary>
    /// Is called when the interaction of the NPC is trigger.
    /// Trigger the dialogue and prepare to receive the trigger the sale after that.
    /// </summary>
    public override void HandleInteraction()
    {
        DialogueHandler.OnDialogueEnd += TriggerSale;
        base.HandleInteraction();
    }

    /// <summary>
    /// When the "hello dialogue" ends, prepare the sale to begin at the next frame.
    /// We have to wait next frame to avoid event bugs 
    /// </summary>
    /// <param name="sender">The sender of the event</param>
    /// <param name="e">The event triggered</param>
    private void TriggerSale(object sender, EventArgs e)
    {
        DialogueHandler.OnDialogueEnd -= TriggerSale;
        vendorPanel.OnSaleEnd += TriggerGoodbyeDialogue;
        StartCoroutine(TriggerVendorPanel());
    }

    /// <summary>
    /// When the sale ends, prepare the "goodbye dialogue" to begin at the next frame.
    /// We have to wait next frame to avoid event bugs
    /// </summary>
    /// <param name="sender">The sender of the event</param>
    /// <param name="e">The event triggered</param>
    private void TriggerGoodbyeDialogue(object sender, EventArgs e)
    {
        // Unsubscribe from event in order to avoid bugs
        vendorPanel.OnSaleEnd -= TriggerGoodbyeDialogue;
        StartCoroutine(TriggerGoodbyeDialogue());
    }

    /// <summary>
    /// Trigger the sale in the next frame of the Unity process in order to avoid
    /// event bugs 
    /// </summary>
    /// <returns></returns>
    private IEnumerator TriggerVendorPanel()
    {
        yield return new WaitForEndOfFrame();
        vendorPanel.StartSale(upgrades);
    }

    /// <summary>
    /// Trigger the "goodbye dialogue" in the next frame of the Unity process in order to avoid
    /// event bugs 
    /// </summary>
    /// <returns></returns>
    private IEnumerator TriggerGoodbyeDialogue()
    {
        yield return new WaitForEndOfFrame();
        DialogueHandler.StartDialogue(goodbyeDialogue);
    }
}
