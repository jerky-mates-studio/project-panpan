﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    [Tooltip("The prefab for the inventory slot")]
    public GameObject inventorySlot;

    private Dictionary<Material, GameObject> inventorySlots = new Dictionary<Material, GameObject>();
    private GameObject player;

    private void Reset()
    {

        inventorySlot = Resources.Load<GameObject>("Prefabs/GUI/Inventory Slot");
            
    }

    private void Awake()
    {
        CreateInventorySlots();        
    }

    private void Start()
    {
        player = Hero.Player();
    }

    /**
     * Create all theinventory slots according to the "imageMatching" property
     */
    private void CreateInventorySlots()
    {
        foreach (Material material in Resources.LoadAll<Material>(Material.materialPath))
        {
            GameObject newInventorySlot = Instantiate<GameObject>(inventorySlot, transform);
            newInventorySlot.GetComponentInChildren<Image>().sprite = material.Image;
            newInventorySlot.GetComponentInChildren<Text>().text = ": 0";
            inventorySlots.Add(material, newInventorySlot);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach(Material material in player.GetComponent<Hero>().Inventory.Keys)
        {
            inventorySlots[material].GetComponentInChildren<Text>().text = ": " + player.GetComponent<Hero>().Inventory[material];
        }
    }
}
