using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDashCollisionHandler
{
    void OnDashCollision(Vector2 dash_direction);
}
