using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectExplodeEvent : MonoBehaviour
{
    public delegate void ObjectExplodeDelegate();
    public event ObjectExplodeDelegate ObjectExplode;

    public void TriggerObjectExplode() => ObjectExplode?.Invoke();
}
