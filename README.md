# Project PanPan

The project PanPan is a basic top view shooter to train to use Unity.
The assets of the game are stored with [Git LFS](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/), so before cloning this project you have to install Git LFS and initialize it with the command `git lfs install`.

This project is genereted thanks to **Unity2020.1.13f1** . 

# The features of the *1st playable prototype*
- [x] basic player movement (translation + rotation)
- [X] create some basic enemies
- [X] Allows the player and the enemies to shoot
- [X] Allows the enemies and the player to die
- [X] Display walls and ground
- [X] Load a map (a dungeon floor) from a file
- [X] Build a complete level (dungeon) from a file
- [X] Add heal items that can be loot on enemies
- [X] Add musics and sound

# The features for the *Qwerty friendly updates*
- [X] Support the **WASD** controls
- [X] Support the XBOX controller for all the game (**menus included**)
- [X] Small bug fixing
- [X] Add credits to the game

# The goals for the *Gameplay Update*
- [X] Player dash
- [X] Player melee attack
- [X] Player deflect bullet
- [X] Player slow time
- [X] In game hub where you can choose levels (not anymore with the menues)
- [X] Basic NPC + basic dialogue
- [X] Allow to loot resources
- [X] Allow to buy skill upgrades with the resources
- [X] Allow the player to save the game

# The goals for the *Pretty Update* (**on standby**)
- [ ] Make the game menus a little prettier
- [ ] Add a splash screen of the studio when the game is launched
- [ ] Levels with real assets (decoration, carpets, bookcases, statues, ...) + animate assets
- [ ] Make the enemies move in *idle* state and shoot in *fight* state
- [ ] Make beautiful arts for ITCH.io 

# The goals for the *Story Update* (**on standby**)
- [ ] Add cinematics
- [ ] Write the story
- [ ] Plan the discovery of new skills (dash, melee attack, deflect shoots, slow time)
- [ ] Add special effect (camera shaking + controller vibrating when receiving damages, slow motion when deadly shoot approaches, ...)

# Dependencies
We have few depencies which are *Unity packages* that add some features very useful to our game. These can be easily intalled by using the *package manager* in the *Unity Editor*

## Universal RP
We use the **Universal RP** package to handle the light in our 2D game. There is a cool [video](https://www.youtube.com/watch?v=F5l8vP90EvU) that explains how to install it and how to use it.

## Input System
We use the new **Input System** of *Unity* which is presented and explained [here](https://blogs.unity3d.com/2019/10/14/introducing-the-new-input-system/).

# How to build a map
We use [**Tiled**](https://www.mapeditor.org/) to create the maps of our game and we use [**SuperTiled2Unity**](https://seanba.itch.io/supertiled2unity) to load them as Unity object.
Here are some tricks to create some maps that will be load correctly by our game:
- The created maps and tilesets have to be located in `src/PanPan/Assets/Resources/Maps` while the image used to create the tilesets are located in `src/PanPan/Assets/Tiles` (not really mandotory but easier for organisation)
- The map layers that have to block bullets (such as walls for example) must have the following properties: `unity:layer Block_Bullet`

![Block Bullet property](docs/Block_Bullet_layer.PNG "Block Bullet property")

- Tiles that have to block unit movement (such as the walls) must have a [collision detector](https://doc.mapeditor.org/en/stable/manual/editing-tilesets/#tile-collision-editor) set in Tiled
- Each map layer must have their `unity:layer` and `unity:SortingLayer` properties set to their respective Unity layer name. Currently available Unity layer names for map layers are **Background** and **Block_Bullet**

![Layer Properties](docs/layer_properties.PNG "Layer Properties")

- The "units" placed on the map must be in a special type of map layer: [an object layer](https://doc.mapeditor.org/en/stable/manual/objects/). Tiled allows to define some types to assign to the objects
and then, make these map objects be transformed into Unity objects according to their type. This process is explained in the **SuperTiled2Unity** [documentation](https://supertiled2unity.readthedocs.io/en/latest/manual/custom-properties.html#object-types-support)
- When you have finished creating your tiled map, **SuperTiled2Unity** will automatically translate it onto a Unity map. All you have to do is to drag your map object and drop it in the Unity scene inspector

![Drag and drop the map](docs/Drag_Drop_Map.PNG "Drag and drop the map")

- The map must have a `levelMusic` property in order to play music during the game (not mandatory). The value of this property is a path towards a music file relative to the `Resources` folder and without the exetnsion.
For exemple, if you want the `project PanPan\src\PanPan\Assets\Resources\Musics\levels\fight.wav` music to plys for a map, the value of `levelMusic` has to be `Musics\levels\fight`.

![Level Music](docs/LevelMusic.PNG "Level Music")

# Credits
This section allows us to properly thank all creators of the assets we use.

## Sounds
remaxim
OpenGameArt.org

Michel Baradari
apollo-music.de

TinyWorlds
http://tinyworlds.org/

Shapeforms Audio
https://shapeforms.com/

## Music
Joth
OpenGameArt.org

cynicmusic
OpenGameArt.org

Ville Nousiainen
http://soundcloud.com/mutkanto

Music by Matthew Pablo
http://www.matthewpablo.com

mrpoly
OpenGameArt.org

## Redditor feedback givers
DumbstufMaksMiLaugh

javawockybass

OhNoPonoGames

The_Cracked_Walnut

Riko_07

faxfrag

Squivit

besttopguy

NeonChaser

Saphirblau20

UManLutin

