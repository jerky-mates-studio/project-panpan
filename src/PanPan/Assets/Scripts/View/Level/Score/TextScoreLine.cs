using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Text))]
[RequireComponent (typeof(AudioSource))]
public class TextScoreLine : MonoBehaviour, IScoreLine
{
    [Tooltip("The sound to play when a score line is displayed")]
    [SerializeField]
    private AudioClip lineDisplayedSound;

    private TMP_Text text;
    private AudioSource audioSource;
    private string endValue;
    public string EndValue { get => endValue; set => endValue = value; }

    private Color initialColor;

    public IEnumerator DisplayLine()
    {
        text.enabled = true;
        text.color = initialColor;
        audioSource.PlayOneShot(lineDisplayedSound);
        text.text = EndValue;
        yield return null;
    }

    public IEnumerator HideLine()
    {
        Color newColor = initialColor;
        newColor.a = 0f;
        text.color = newColor;
        text.enabled = false;
        yield return null;
    }

    private void Awake()
    {
        text = GetComponent<TMP_Text>();
        audioSource = GetComponent<AudioSource>();

        initialColor = text.color;

        InitDisplay();
    }

    private void InitDisplay()
    {
        Color newColor = initialColor;
        newColor.a = 0f;
        text.color = newColor;
        text.enabled = false;
    }
}
