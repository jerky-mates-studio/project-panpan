using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;


public class TestLoadSwitch
{

    /**
     * Return a "RiffleBullet" which has a 0 maximum spread
     */
    private GameObject GetBulletTestInstance()
    {
        GameObject bullet = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Bullet/RiffleBullet"));
        // ensure the bullet go right where we want to
        bullet.GetComponent<Bullet>().maximumSpread = 0;
        bullet.GetComponent<Bullet>().damage = 10;
        return bullet;
    }

    [UnityTest]
    /**
     * Test if the load switch is not activated if there is not enough bullet to load it
     */
    public IEnumerator LoadSwitchDoesntTrigger()
    {
        // Initilize objects
        GameObject loadSwitch = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Map/LoadSwitch"));
        loadSwitch.transform.position = new Vector3(10, 0, 0);

        List<GameObject> bullets = new List<GameObject>(9);
        for(int i=0; i<9; i++)
        {
            bullets.Add(GetBulletTestInstance());
            bullets[i].transform.position = new Vector3(0, 0, 0);
            // Ensure to set a shooter to the bullet
            bullets[i].GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Player"));
        }
        Assert.IsFalse(loadSwitch.GetComponent<LoadSwitch>().IsActivated());

        // We have to wait a little to be sur the bullets hit the switch
        yield return new WaitForSeconds(1f);

        // Test assertion
        Assert.IsFalse(loadSwitch.GetComponent<LoadSwitch>().IsActivated());

        // Delete the Unity objects
        Object.Destroy(loadSwitch);
        for (int i = 0; i < 9; i++)
        {
            Object.Destroy(bullets[i]);
        }
    }

    [UnityTest]
    /**
     * Test if the load switch is activated if there is enough bullet to load it
     */
    public IEnumerator LoadSwitchTrigger()
    {
        // Initilize objects
        GameObject loadSwitch = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Map/LoadSwitch"));
        loadSwitch.transform.position = new Vector3(10, 0, 0);

        List<GameObject> bullets = new List<GameObject>(10);
        for (int i = 0; i < 10; i++)
        {
            bullets.Add(GetBulletTestInstance());
            bullets[i].transform.position = new Vector3(0, 0, 0);
            // Ensure to set a shooter to the bullet
            bullets[i].GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Player"));
        }

        // We have to wait a little to be sur the bullets hit the switch
        yield return new WaitForSeconds(1f);

        // Test assertion
        Assert.IsTrue(loadSwitch.GetComponent<LoadSwitch>().IsActivated());

        // Delete the Unity objects
        Object.Destroy(loadSwitch);
        for (int i = 0; i < 10; i++)
        {
            Object.Destroy(bullets[i]);
        }

    }

    [UnityTest]
    /**
     * Test if the load switch lose some energy over time
     */
    public IEnumerator LoadSwitchLoseEnergy()
    {
        // Initilize objects
        GameObject loadSwitch = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Map/LoadSwitch"));
        loadSwitch.transform.position = new Vector3(10, 0, 0);

        List<GameObject> bullets = new List<GameObject>(10);
        for (int i = 0; i < 5; i++)
        {
            bullets.Add(GetBulletTestInstance());
            bullets[i].transform.position = new Vector3(0, 0, 0);
            // Ensure to set a shooter to the bullet
            bullets[i].GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Player"));
        }

        // We have to wait a little to be sur the bullets hit the switch
        yield return new WaitForSeconds(1f);

        // shoot a 2nd salve of laser 
        for (int i = 5; i < 10; i++)
        {
            bullets.Add(GetBulletTestInstance());
            bullets[i].transform.position = new Vector3(0, 0, 0);
            // Ensure to set a shooter to the bullet
            bullets[i].GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Player"));
        }

        // We have to wait a little to be sur the bullets hit the switch
        yield return new WaitForSeconds(1f);

        // Test assertion
        Assert.IsFalse(loadSwitch.GetComponent<LoadSwitch>().IsActivated());

        // Delete the Unity objects
        Object.Destroy(loadSwitch);
        for (int i = 0; i < 10; i++)
        {
            Object.Destroy(bullets[i]);
        }

    }

    [UnityTest]
    /**
     * Test if the load switch doesn't load if the bullet color don't match
     */
    public IEnumerator LoadSwitchDoesntLoad()
    {
        // Initilize objects
        GameObject loadSwitch = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game/Map/LoadSwitch"));
        loadSwitch.GetComponent<LoadSwitch>().BulletType = Resources.Load<BulletType>("prefabs/data/BulletType/Enemy");
        loadSwitch.transform.position = new Vector3(10, 0, 0);

        List<GameObject> bullets = new List<GameObject>(10);
        for (int i = 0; i < 10; i++)
        {
            bullets.Add(GetBulletTestInstance());
            bullets[i].transform.position = new Vector3(0, 0, 0);
            // Ensure to set a shooter to the bullet
            bullets[i].GetComponent<Bullet>().SetBulletType(Resources.Load<BulletType>("prefabs/data/BulletType/Player"));
        }

        // We have to wait a little to be sur the bullets hit the switch
        yield return new WaitForSeconds(1f);

        // Test assertion
        Assert.IsFalse(loadSwitch.GetComponent<LoadSwitch>().IsActivated());

        // Delete the Unity objects
        Object.Destroy(loadSwitch);
        for (int i = 0; i < 10; i++)
        {
            Object.Destroy(bullets[i]);
        }

    }
}
