using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDashEvent : MonoBehaviour
{
    public delegate void UnitDashDelegate();
    public event UnitDashDelegate UnitDash;

    public void TriggerUnitDash() => UnitDash?.Invoke();
}
