﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Fire : AttackStrategy
{

    private bool isFiring = false; 

    public override void Attack()
    {
        if (!IsUnlocked())
            return;

        if (isFiring)
        {
            GetComponent<Hero>().Fire();
        }
    }

    /**
     * Is called when the fire button change state
     */
    public void OnFire(InputAction.CallbackContext context)
    {
        isFiring = context.ReadValueAsButton();
    }

    protected override bool IsThisStrategyOver()
    {
        if (!IsUnlocked())
            return true;

        return false;
    }

    protected override bool MustStart()
    {
        if (!IsUnlocked())
            return false;

        return false;
    }
}
