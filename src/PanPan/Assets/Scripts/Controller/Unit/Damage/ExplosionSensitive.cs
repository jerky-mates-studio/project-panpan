using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(KillableUnit))]
public class ExplosionSensitive : MonoBehaviour, IExplosable
{


    private KillableUnit KillableUnitScript { get; set; }

    public void Awake()
    {
        KillableUnitScript = GetComponent<KillableUnit>();
    }

    public void OnBeginExplosed(float damages)
    {
        KillableUnitScript.TakeDamages(damages);
    }
}
