﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(AudioSource))]
public class CustomButton : MonoBehaviour, ISelectHandler
{
    [Header("Button sound")]
    [Tooltip("The sound to play when the button is clicked")]
    [SerializeField]
    private AudioClip buttonClickSound;
    [Tooltip("The sound to play when the button is selected")]
    [SerializeField]
    private AudioClip buttonSelectedSound;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Play the click sound of the button 
    /// </summary>
    public void PlayClickSound()
    {
        if (buttonClickSound != null)
            PlaySound(buttonClickSound);
    }

    /// <summary>
    /// This method is called when the button is selected in yhe UI
    /// </summary>
    /// <param name="eventData">The event data</param>
    public void OnSelect(BaseEventData eventData)
    {
        if(eventData.selectedObject != null && buttonSelectedSound != null)
            PlaySound(buttonSelectedSound);
    }

    /// <summary>
    /// Play a sound if the matching button is interactable
    /// </summary>
    /// <param name="sound">The sound to play</param>
    protected void PlaySound(AudioClip sound)
    {
        if (GetComponent<Button>().interactable)
        {
            audioSource.clip = sound;
            audioSource.Play();
        }
        
    }

}
