using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EniacFightState : AIState
{
    private EniacAI Context { get; set; }
    private Enemy enemy;

    public EniacFightState(EniacAI context) : base(context)
    {
        Context = context;
        enemy = context.GetComponent<Enemy>();
    }

    public override void OnEnterState()
    {
        return;
    }

    public override void UpdateState()
    {
        if (isPlayerOnSight())
        {
            enemy.LookAt(Player.transform.position);
            if (! enemy.IsDead())
            {
                enemy.Fire();
            }
        }
        else
        {
            Context.SwitchState(Context.SearchState);
        }
    }

}
