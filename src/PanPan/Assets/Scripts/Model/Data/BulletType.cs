﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Model/BulletType")]
public class BulletType : ScriptableObject
{
    [Tooltip("The color of the bullet and its trail")]
    [SerializeField]
    private Color bulletColor;
    [Tooltip("The tag of the shooter")]
    [SerializeField]
    private string shooterTag;

    public Color BulletColor { get => bulletColor;}
    public string ShooterTag { get => shooterTag;}
}
