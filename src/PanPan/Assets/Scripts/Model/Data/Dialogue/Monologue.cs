﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Model/Dialogue/Monologue")]
[System.Serializable]
public class Monologue: Dialogue
{
    [Tooltip("The sentences in the monologue")]
    [SerializeField]
    [TextArea(3, 10)]
    private string[] sentences;
    [Tooltip("The game object that represents the speaker")]
    [SerializeField]
    private int[] speakerIndexes;
    private int currentSentence = 0;

    /**
     * Return the next sentence in the monologue.
     */
    public override List<DialogueLine> GetNextDialogueLines()
    {
        if (IsOver())
            throw new IndexOutOfRangeException("Make the call the \"Reset\" method before using the monologue");
        List<DialogueLine> dialogLine = new List<DialogueLine>(1);
        string sentence = sentences[currentSentence];
        currentSentence++;
        dialogLine.Add(new DialogueLine(sentence, 0));
        return dialogLine;
    }

    /**
     * Rewind the monologue as it was never played
     */
    public override void Reset()
    {
        currentSentence = 0;
    }

    /**
     * Return true iif the last sentence has been asked for
     */
    public override bool IsOver()
    {
        return currentSentence >= sentences.Length;
    }

    /**
     * Return the indexes of the speaker for this monologue. The indexes must match 
     * the speaker list of the discussion
     */
    public int[] GetSpeakerIndexes()
    {
        return speakerIndexes;
    }
}
