using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShootable
{
    /// <summary>
    /// Handles when the unit is touched by a bullet
    /// </summary>
    /// <param name="damage">The number of damages dealt by the bullet</param>
    void OnBeingShoot(Bullet bullet);
}
