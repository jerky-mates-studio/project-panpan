﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public abstract class Dialogue : ScriptableObject
{

    public struct DialogueLine
    {
        public string sentence;
        public int speakerIndex;

        public DialogueLine(string sentence, int speakerIndex)
        {
            this.sentence = sentence;
            this.speakerIndex = speakerIndex;
        }
    }

    /**
     * Return the next sentence in the monologue. Ensure the sentences loop if they are asked too many times.
     */
    public abstract List<DialogueLine> GetNextDialogueLines();

    /**
     * Rewind the monologue as it was never played. Be sure to always call it before using the dialogue.
     */
    public abstract void Reset();

    /**
     * Return true iif the last sentence has been asked for
     */
    public abstract bool IsOver();

}
