using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EniacAI : AI
{

    private EniacIdleState idleState;
    public EniacIdleState IdleState { get => idleState; }
    private EniacFightState fightState;
    public EniacFightState FightState { get => fightState; }
    private EniacSearchState searchState;
    public EniacSearchState SearchState { get => searchState; }


    protected override void Start()
    {
        base.Start();
        idleState = new EniacIdleState(this);
        fightState = new EniacFightState(this);
        searchState = new EniacSearchState(this);

        CurrentAIState = idleState;
        CurrentAIState.OnEnterState();
    }
}
