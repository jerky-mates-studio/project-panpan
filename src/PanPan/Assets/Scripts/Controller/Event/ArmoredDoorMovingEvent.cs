using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmoredDoorMovingEvent : MonoBehaviour
{
    // Opening event
    public delegate void ArmoredDoorOpeningDelegate();
    public event ArmoredDoorOpeningDelegate ArmoredDoorOpening;
    public void TriggerArmoredDoorOpening() => ArmoredDoorOpening?.Invoke();

    // Closing event
    public delegate void ArmoredDoorClosingDelegate();
    public event ArmoredDoorClosingDelegate ArmoredDoorClosing;
    public void TriggerArmoredDoorClosing() => ArmoredDoorClosing?.Invoke();
}
