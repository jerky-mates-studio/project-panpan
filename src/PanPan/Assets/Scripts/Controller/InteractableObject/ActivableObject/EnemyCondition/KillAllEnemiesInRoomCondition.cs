using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAllEnemiesInRoomCondition : ActivationCondition
{
    [Tooltip("The list of enemies to kill in order to trigger the condition")]
    [SerializeField]
    private GameObject[] enemiesToKill;

    private bool isAlreadyTriggered = false;

    private void Start()
    {
        foreach (GameObject enemy in enemiesToKill)
        {
            NonShooterUnit unit = enemy.GetComponent<NonShooterUnit>();
            if (unit == null)
                Debug.LogError("KillAllEnemiesInRoomCondition::Start : A unit is not a non shooter unit");
        }
    }

    private void Update()
    {
        if (isAlreadyTriggered)
            return;

        int remainingEnemies = enemiesToKill.Length;

        foreach (GameObject enemy in enemiesToKill)
        {
            if (enemy == null)
            {
                remainingEnemies--;
                continue;
            }

            NonShooterUnit unit = enemy.GetComponent<NonShooterUnit>();
            if (unit == null)
            {
                remainingEnemies--;
                continue;
            }

            if(unit.IsDead())
            {
                remainingEnemies--;
                continue;
            }
        }

        if ( remainingEnemies == 0 )
        {
            isAlreadyTriggered = true;
            TriggerActivation(true);
        }
    }
}
