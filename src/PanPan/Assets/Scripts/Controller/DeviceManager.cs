﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DeviceManager : MonoBehaviour
{
    [Tooltip("The list of supported devices. The 1st element is used by default.")]
    [SerializeField]
    private DeviceConfiguration[] supportedDevices;
    private DeviceConfiguration currentDevice;

    private PlayerInput playerInput;
    private string currentDeviceName = "";
    private static GameObject instance;


    public event EventHandler OnDeviceChanged;

    public DeviceConfiguration CurrentDevice { get => currentDevice;}
    public static GameObject Instance { get => instance; }

    private void Awake()
    {
        if (Instance == null)
            instance = gameObject;
        else
            Destroy(gameObject);
        currentDevice = supportedDevices[0];
    }

    // Start is called before the first frame update
    void Start()
    {
        playerInput = Hero.Player().GetComponent<PlayerInput>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentDeviceName != playerInput.devices[0].displayName)
        {
            UpdateCurrentDevice(playerInput.devices[0].displayName);
        }
            
    }

    /// <summary>
    /// If the "newDevice" is a supported device name, change the current device and current device name to
    /// the ones matching "newDevice". Triggered the "OnDeviceChanged" event
    /// </summary>
    /// <param name="newDevice">The name of the new device used</param>
    private void UpdateCurrentDevice(string newDevice)
    {
        currentDeviceName = newDevice;
        foreach (DeviceConfiguration device in supportedDevices)
        {
            if(device.DeviceName == newDevice) { 
                currentDevice = device;
                OnDeviceChanged?.Invoke(this, EventArgs.Empty);
                return;
            }
        }
        currentDevice = supportedDevices[0];
        OnDeviceChanged?.Invoke(this, EventArgs.Empty);
    }
}
