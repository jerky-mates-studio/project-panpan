using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(DoorBrokeEvent))]
public class BreakableDoor : MonoBehaviour, IDashCollisionHandler
{
    private ParticleSystem doorParticleSystem;
    private BoxCollider2D boxCollider;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        doorParticleSystem = gameObject.GetComponentInChildren<ParticleSystem>();
        boxCollider = gameObject.GetComponent<BoxCollider2D>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    public void OnDashCollision(Vector2 dash_direction)
    {
        float collisionAngle = Mathf.Atan2(dash_direction.y, dash_direction.x) * Mathf.Rad2Deg;
        doorParticleSystem.transform.Rotate(new Vector3(0, 0, collisionAngle));
        doorParticleSystem.Play();
        boxCollider.enabled = false;
        spriteRenderer.enabled = false;

        GetComponent<DoorBrokeEvent>().TriggerDoorBroke();

        StartCoroutine(DestroyGameObject());
    }

    /// <summary>
    /// Destroy the game object after every particles has died
    /// </summary>
    /// <returns></returns>
    private IEnumerator DestroyGameObject()
    {
        float deathTime = doorParticleSystem.main.duration + doorParticleSystem.main.startLifetime.constant;
        yield return new WaitForSeconds(deathTime);
        Destroy(gameObject);
    }
}
