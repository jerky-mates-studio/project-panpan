using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ArmoredDoorMovingEvent))]
public class ArmoredDoor : MonoBehaviour
{
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void OpenDoor()
    {
        animator.SetBool("IsDoorOpen", true);
        GetComponent<ArmoredDoorMovingEvent>().TriggerArmoredDoorOpening();
    }

    public void CloseDoor()
    {
        animator.SetBool("IsDoorOpen", false);
        GetComponent<ArmoredDoorMovingEvent>().TriggerArmoredDoorClosing();
    }

}
