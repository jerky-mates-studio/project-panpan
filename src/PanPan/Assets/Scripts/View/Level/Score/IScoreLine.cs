using System.Collections;

public interface IScoreLine
{
    /// <summary>
    /// Display the score line at the screen with an animation
    /// </summary>
    public IEnumerator DisplayLine();

    /// <summary>
    /// Hide the score line with an animation
    /// </summary>
    public IEnumerator HideLine();
}
