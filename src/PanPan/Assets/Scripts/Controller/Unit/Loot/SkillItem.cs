using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider2D))]
public class SkillItem : MonoBehaviour
{
    
    private SkillItemData m_skillData;
    public SkillItemData SkillData {
        get { return m_skillData; }
        set {
            m_skillData = value;
            UpdateSkillSprite();
        }
    }

    /// <summary>
    /// Update this item sprite according to the skill it represents
    /// </summary>
    private void UpdateSkillSprite()
    {
        SpriteRenderer spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            Debug.LogError("SkillItem::UpdateSkillSprite : This object has no sprite renderer in its children");
            return;
        }

        spriteRenderer.sprite = SkillData.Image;
    }

    /**
    * This method is called by Unity when this object collides with another
    */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Trigger an activation when the player enter the room
        if (collision.tag == "Player")
        {
            
            if(SkillData == null)
                Debug.LogError("SkillItem::OnTriggerEnter2D : the 'SkillData' property is not set");
            else
                PlayerPickUpNewSkillEvent.GetInstance().TriggerPlayerPickUpNewSkill(SkillData);
            
            Destroy(gameObject);

        }

    }

}
