using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(CheckpointEvent))]
public class CheckpointSoundManager : MonoBehaviour
{
    [Header("Checkpoint activation")]
    [Tooltip("The clip to play when a checkpoint is activated")]
    [SerializeField]
    private AudioClip activationSound;
    [Tooltip("False if the the first activation event must be ignored (for exemple: avoid to play sound at the first checkpoint when the player start the level)")]
    [SerializeField]
    private bool mustPlayFirstActvationSound = true;

    [Header("Checkpoint respawning")]
    [Tooltip("The clip to play when a checkpoint is respawning")]
    [SerializeField]
    private AudioClip respawningSound;
    [Tooltip("The pitch to apply to the 'respawning' audio clip")]
    [SerializeField]
    [Range(-3f, 3f)]
    private float respawningPitch = 1.78f;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        GetComponent<CheckpointEvent>().CheckpointActivated += PlayCheckpointActivationSound;
        GetComponent<CheckpointEvent>().CheckpointStartRespawning += PlayCheckpointRespawningSound;
        GetComponent<CheckpointEvent>().CheckpointStopRespawning += StopCheckpointRespawningSound;
    }

    private void OnDisable()
    {
        GetComponent<CheckpointEvent>().CheckpointActivated -= PlayCheckpointActivationSound;
        GetComponent<CheckpointEvent>().CheckpointStartRespawning -= PlayCheckpointRespawningSound;
        GetComponent<CheckpointEvent>().CheckpointStopRespawning -= StopCheckpointRespawningSound;
    }

    private void PlayCheckpointActivationSound()
    {
        // ignore 1st activation event if needed
        if ( ! mustPlayFirstActvationSound)
        {
            mustPlayFirstActvationSound = true;
            return;
        }

        audioSource.PlayOneShot(activationSound);
    }

    private void PlayCheckpointRespawningSound()
    {
        audioSource.loop = true;
        audioSource.clip = respawningSound;
        audioSource.pitch = respawningPitch;
        audioSource.Play();
    }

    private void StopCheckpointRespawningSound()
    {
        audioSource.loop = false;
        audioSource.pitch = 1f;
        audioSource.Stop();
    }
}
