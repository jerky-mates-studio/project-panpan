﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Model/Material")]
public class Material : ScriptableObject
{
    public static readonly string materialPath = "Prefabs/Data/Loot";

    [Tooltip("The name of the material")]
    [SerializeField]
    private string materialName;
    [Tooltip("The image that that match to this material")]
    [SerializeField]
    private Sprite image;

    public string MaterialName { get => materialName;}

    public Sprite Image { get => image;}
}
