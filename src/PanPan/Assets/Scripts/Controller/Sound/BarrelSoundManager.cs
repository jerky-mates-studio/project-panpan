using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitTookDamagesEvent))]
[RequireComponent(typeof(ObjectExplodeEvent))]
[RequireComponent(typeof(SoundManager))]
public class BarrelSoundManager : MonoBehaviour
{
    [Tooltip("The audio clip to play when the barrel explodes")]
    [SerializeField]
    private AudioClip explosionClip;
    [Tooltip("The audio clip to play when the barrel is hit")]
    [SerializeField]
    private AudioClip hitClip;

    private void OnEnable()
    {
        GetComponent<ObjectExplodeEvent>().ObjectExplode += PlayExplosionSound;
        GetComponent<UnitTookDamagesEvent>().UnitTookDamages += PlayHitSound;
    }

    private void OnDisable()
    {
        GetComponent<ObjectExplodeEvent>().ObjectExplode -= PlayExplosionSound;
        GetComponent<UnitTookDamagesEvent>().UnitTookDamages -= PlayHitSound;
    }

    private void PlayExplosionSound()
    {
        GetComponent<SoundManager>().RandomizePitch(explosionClip);
    }

    private void PlayHitSound(float damages)
    {
        GetComponent<SoundManager>().RandomizePitch(hitClip);
    }
}
