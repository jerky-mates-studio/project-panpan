using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    private static CheckpointManager instance;

    public static  CheckpointManager GetInstance()
    {
        return instance;
    }

    [Header("Parameters")]
    [Tooltip("The initial checkpoint of the level (start of the level)")]
    [SerializeField]
    private GameObject initialCheckpointGameObject;

    private ISpawner lastCheckpoint;
        
    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        PlayerDiedEvent.GetInstance().PlayerDied += OnPlayerDied;
    }

    private void OnDisable()
    {
        PlayerDiedEvent.GetInstance().PlayerDied -= OnPlayerDied;
    }

    // Start is called before the first frame update
    void Start()
    {
        SetLastCheckpoint(initialCheckpointGameObject.GetComponent<ISpawner>());
    }

    /// <summary>
    /// Process the checkpoint that has just been activated
    /// </summary>
    /// <param name="checkpoint">The checkpoint that has just been activated</param>
    public void OnCheckpointActivated(ISpawner checkpoint)
    {
        SetLastCheckpoint(checkpoint);
    }

    /// <summary>
    /// Process the death of the player
    /// </summary>
    /// <param name="player">The player that is dead</param>
    private void OnPlayerDied(Hero player)
    {
        if(lastCheckpoint != null)
            lastCheckpoint.Spawn(player);
    }

    public void SetLastCheckpoint(ISpawner spawner)
    {
        if (lastCheckpoint == spawner)
            return;

        if (lastCheckpoint != null)
            lastCheckpoint.OnSpawnerDeactivated();
        lastCheckpoint = spawner;
        lastCheckpoint.OnSpawnerActivated();
    }

}
