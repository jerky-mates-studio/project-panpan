using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [Tooltip("The minimum time needed to complete the level")]
    [SerializeField]
    [Min(0f)]
    private float minimumTimeToFinish = 75f;
    [Tooltip("The total number of enemies to kill")]
    [SerializeField]
    [Min(0f)]
    private int totalEnemiesToKill = 26;
    [Tooltip("The number of death necessary to reach a score of 0% for this subject")]
    [SerializeField]
    [Min(0f)]
    private int maximumNumberOfDeath = 10;

    private float startLevelTime;
    private int enemiesKilled = 0;
    private int playerDeath = 0;

    private bool isScoreComputed = false;

    private void Start()
    {
        startLevelTime = Time.realtimeSinceStartup;
    }

    private void OnEnable()
    {
        EnemyDiedEvent.GetInstance().EnemyDied += OnEnemyDied;
        PlayerDiedEvent.GetInstance().PlayerDied += OnPlayerDied;
        LevelCompletedEvent.GetInstance().LevelCompleted += DisplayScore;
    }

    private void OnDisable()
    {
        EnemyDiedEvent.GetInstance().EnemyDied -= OnEnemyDied;
        PlayerDiedEvent.GetInstance().PlayerDied -= OnPlayerDied;
        LevelCompletedEvent.GetInstance().LevelCompleted -= DisplayScore;

    }

    /// <summary>
    /// Handle the death of an enemy
    /// </summary>
    private void OnEnemyDied()
    {
        enemiesKilled++;
    }

    /// <summary>
    /// Handle the death of the player
    /// </summary>
    /// <param name="player">The player that died (not used)</param>
    private void OnPlayerDied(Hero player)
    {
        playerDeath++;
    }

    /// <summary>
    /// Display the score of the player
    /// </summary>
    private void DisplayScore()
    {
        if (isScoreComputed)
            return;

        float time = Time.realtimeSinceStartup - startLevelTime;
        float score = ComputeScore(time, enemiesKilled, playerDeath);

        ScoreUI.Score scoreObject;
        scoreObject.completionTime = time;
        scoreObject.enemiesKilled = enemiesKilled;
        scoreObject.playerDeath = playerDeath;
        scoreObject.rank = ComputeRank(score);

        DisplayScorePanelEvent.GetInstance().TriggerScorePanelDisplay(scoreObject);
    }

    /// <summary>
    /// Compute the score of the player
    /// </summary>
    /// <param name="time">The time the player took to complete the level</param>
    /// <param name="enemiesKilled">The number of enemies killed by the player</param>
    /// <param name="playerDeath">The number of death of the player</param>
    /// <returns>The score of the player</returns>
    private float ComputeScore(float time, int enemiesKilled,  int playerDeath)
    {
        isScoreComputed = true;

        float timeScore = minimumTimeToFinish / time;
        float killScore = (float) enemiesKilled / (float) totalEnemiesToKill;
        float deathScore = (maximumNumberOfDeath - playerDeath) / (float) maximumNumberOfDeath;
        return (timeScore + killScore + deathScore) * 0.33f;
    }

    /// <summary>
    /// Compute the rank to assign to the player
    /// </summary>
    /// <param name="score">The score of the player in this level</param>
    /// <returns>The rank of the player
    /// </returns>
    private ScoreUI.Rank ComputeRank(float score)
    {
        if (score >= 0.9f)
            return ScoreUI.Rank.MutantEliteSoldierPrototype;
        else if (score >= 0.7f)
            return ScoreUI.Rank.UnionInterventionBrigadesCaptain;
        else if (score >= 0.5f)
            return ScoreUI.Rank.UnionInterventionBrigadesSoldier;
        else
            return ScoreUI.Rank.CapitalDomeCitizen;
    }
}
