﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
    //C:\Users\Maxim\AppData\LocalLow\JerkyMateStudio\PanPan\Saves
    private static string saveDirectory = Application.persistentDataPath + Path.DirectorySeparatorChar+ "Saves" + Path.DirectorySeparatorChar;
    private static string playerSaveFile = "inventory.bin";
    private static string upgradesSaveFile = "upgrades.bin";

    /**
     * Check if the "saveDirectory" exist. If not, create it.
     */
    private static void EnsureSaveFolderExists()
    {
        if (!Directory.Exists(saveDirectory))
            Directory.CreateDirectory(saveDirectory);
    }

    /**
     * Return true iif all the save files exists
     */
    public static bool SaveFilesExist()
    {
        return SaveFileExists(playerSaveFile) && SaveFileExists(upgradesSaveFile);
    }

    /**
     * Erase the save files
     */
    public static void EraseSaveFiles()
    {
        // if the files doesn't exists, no exception is thrown
        // but if the directory doesn't exist, an exception is thrown
        Upgrade.Reset();
        EnsureSaveFolderExists();
        File.Delete(saveDirectory + playerSaveFile);
        File.Delete(saveDirectory + upgradesSaveFile);
    }

    /**
     * Check if the "filename" file exist in the "saveDirectory"
     */
    private static bool SaveFileExists(string filename)
    {
        return File.Exists(saveDirectory + filename);
    }

    /**
     * Generic function that save the "data" in the "saveFile" file in the "saveDirectory" folder
     */
    private static void SaveData<T>(string saveFile, T data)
    {
        EnsureSaveFolderExists();
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(saveDirectory + saveFile, FileMode.Create);
        formatter.Serialize(stream, data);
        stream.Close();
    }

    /**
     * Generic function that load some data from the "saveFile" file in the "saveDirectory" folder;
     * If the "saveFile" file doesn't exist, return the default value for the type "T".
     */
    private static T LoadData<T>(string saveFile)
    {
        if (SaveFileExists(saveFile))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(saveDirectory + saveFile, FileMode.Open);
            T data = (T)formatter.Deserialize(stream);
            stream.Close();
            return data;
        }
        return default(T);
    }

    /**
     * Save all the data of the game
     */
    public static void Save()
    {
        SavePlayer(Hero.Player().GetComponent<Hero>());
        SaveUpgrades();
    }

    /**
     * Load all the data of the game
     */
    public static void Load()
    {
        LoadPlayer(Hero.Player().GetComponent<Hero>());
        LoadUpgrades();
    }

    /**
     * Save the state of all the upgrades in the "upgrade.bin"
     */
    public static void SaveUpgrades()
    {
        UpgradeData data = new UpgradeData();
        SaveData<UpgradeData>(upgradesSaveFile, data);
    }

    /**
     * Load the state of all the upgrades and update their data
     */
    public static void LoadUpgrades()
    {
        UpgradeData data = LoadData<UpgradeData>(upgradesSaveFile);
        if(data != null)
            data.ApplyDataOnUpgrades();
    } 

    /**
     * Save the "player" object in the "inventory.bin" file in the "saveDirectory" folder
     */
    public static void SavePlayer(Hero player)
    {
        PlayerData data = new PlayerData(player);
        SaveData<PlayerData>(playerSaveFile, data);
    }

    /**
     * Load the data stored in the "inventory.bin" file and update the data of the "player" object
     */
    public static void LoadPlayer(Hero player)
    {
        PlayerData data = LoadData<PlayerData>(playerSaveFile);
        if( data != null)
            player.Inventory = data.toDictionnary();
    }

    [System.Serializable]
    public class PlayerData
    {
        int[] amount;
        string[] material_list;

        /**
         * Transform data from the player into simpler data that can be saved in a binary file
         */
        public PlayerData(Hero player)
        {
            int i = 0;
            Material[] materials = Resources.LoadAll<Material>(Material.materialPath);
            amount = new int[materials.Length];
            material_list = new string[materials.Length];
            foreach(Material material in materials)
            {
                amount[i] = player.Inventory[material];
                material_list[i] = material.MaterialName; 
                i++;
            }
        }

        /**
         * Transform the saved data into a dictionnary usable as an inventory for the player.
         */
        public Dictionary<Material, int> toDictionnary()
        {
            Material[] materials = Resources.LoadAll<Material>(Material.materialPath);
            Dictionary<Material, int> dict = new Dictionary<Material, int>();
            for (int i = 0; i<amount.Length; i++)
            {
                foreach(Material material in materials)
                {
                    if(material_list[i] == material.MaterialName)
                    {
                        dict.Add(material, amount[i]);
                        break;
                    }
                }
                
            }
            return dict;
        }
    }

    [System.Serializable]
    public class UpgradeData
    {

        int[] upgradesLevel;

        public UpgradeData()
        {
            Upgrade[] upgrades = Upgrade.GetUpgrades();
            upgradesLevel = new int[upgrades.Length];
            for(int i=0; i<upgrades.Length; i++)
            {
                upgradesLevel[i] = upgrades[i].CurrentUpgradeLevel;
            }
        }

        /**
         * Apply the "upgradesLevel" data on the upgrades
         */
        public void ApplyDataOnUpgrades()
        {
            Upgrade[] upgrades = Upgrade.GetUpgrades();
            for (int i = 0; i < upgradesLevel.Length; i++)
            {
                upgrades[i].CurrentUpgradeLevel = upgradesLevel[i];
            }
        }
    }
}

