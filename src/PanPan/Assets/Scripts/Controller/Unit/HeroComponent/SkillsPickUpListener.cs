using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Dash))]
public class SkillsPickUpListener : MonoBehaviour
{

    private void OnEnable()
    {
        PlayerPickUpNewSkillEvent.GetInstance().PlayerPickUpNewSkill += HandleSkillPickedUp;
    }

    private void OnDisable()
    {
        PlayerPickUpNewSkillEvent.GetInstance().PlayerPickUpNewSkill -= HandleSkillPickedUp;
    }

    /// <summary>
    /// Activate the skill that the player just piscked up
    /// </summary>
    /// <param name="skillData">The skill data related that the player picked up</param>
    private void HandleSkillPickedUp(SkillItemData skillData)
    {
        switch (skillData.SkillName)
        {
            case SkillName.Dash:
                GetComponent<Dash>().SetUnlocked(true);
                break;
            default:
                return;
        }
    }
}
