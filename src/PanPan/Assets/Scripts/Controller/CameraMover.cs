﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    /**
     * Tell the cinemachine to follow the player
     */
    private void Start()
    {
        GetComponent<CinemachineVirtualCamera>().Follow = Hero.Player().transform;
    }

}
