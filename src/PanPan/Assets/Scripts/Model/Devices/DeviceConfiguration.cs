﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Platform Dependant/Device")]
public class DeviceConfiguration : ScriptableObject
{
    [Tooltip("The name of the device, it must be a valid input System name. These can be found in the Input Debugger of a PlayerInput component.")]
    [SerializeField]
    private string deviceName;
    [Tooltip("The sprite that represents the interaction button of the device.")]
    [SerializeField]
    private Sprite interactionButton;

    public string DeviceName { get => deviceName;}
    public Sprite InteractionButton { get => interactionButton;}
}
