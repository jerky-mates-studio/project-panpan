using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretIdleState : AIState
{
    private TurretAI Context { get; set; }

    public TurretIdleState(TurretAI context) : base(context)
    {
        Context = context;
    }

    public override void OnEnterState()
    {
        return;
    }

    public override void UpdateState()
    {
        if (isPlayerOnSight())
        {
            Context.SwitchState(Context.AimState);
        }
    }
}
