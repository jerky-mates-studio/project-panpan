﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialItem : HealItem
{

    [Tooltip("The Material represented")]
    public Material material;

    /**
     * Will taught the player he picked up some loot
     */
    protected override void OnPlayerCollided(Collider2D collision)
    {
        collision.gameObject.GetComponent<Hero>().PickUpLoot(material, itemAmount);
        Destroy(gameObject);
    }
}
