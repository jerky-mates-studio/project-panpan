﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{
    [Tooltip("The name of the scene in wich the player can play")]
    public string VillageSceneName = "Village";
    
    [Header("Panel Navigation")]
    [Tooltip("The main menu")]
    public GameObject mainMenu;
    [Tooltip("The credits panel")]
    public GameObject credits;
    [Tooltip("The confirm panel to display before erasing save files")]
    [SerializeField]
    private GameObject confirmPanel;

    public void OnNewGame()
    {
        if (SaveSystem.SaveFilesExist())
            SwitchPanel(mainMenu, confirmPanel);
        else
        {
            OnPlayNewGame();
        }

    }

    /**
     * Start a new game and erase the save files
     */
    public void OnPlayNewGame()
    {
        SaveSystem.EraseSaveFiles();
        LoadVillageScene();
    }

    /**
     * Go back to the main menu from the confirm pannel
     */
    public void OnCancelNewGame()
    {
        SwitchPanel(confirmPanel, mainMenu);
    }

    /**
     * Load the village scene
     */
    public void LoadVillageScene()
    {
        SceneManager.LoadScene(VillageSceneName);
    }

    /**
     * Switch between the oldPanel (the current panel) and the new panel
     */
    private void SwitchPanel(GameObject oldPanel, GameObject newPanel)
    {
        newPanel.SetActive(true);
        oldPanel.SetActive(false);
    }

    /**
     * Show the credits
     */
    public void OnCreditsClicked()
    {
        SwitchPanel(mainMenu, credits);
    }

    /**
     * Return to the main menu after the credits ends
     */
    public void OnCreditEnds()
    {
        SwitchPanel(credits, mainMenu);
    }

    /**
     * Exit the game
     */
    public void ExitGame()
    {
        Application.Quit();
    }

}
