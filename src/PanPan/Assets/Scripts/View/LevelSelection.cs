﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.SceneManagement;
using static UnityEngine.InputSystem.InputAction;

public class LevelSelection : MonoBehaviour
{
    [Header("Scene settings")]
    [Tooltip("The name of the scene in wich the player can play")]
    public string levelSceneName = "Level";
    [Tooltip("The object that passes data between this scene and the level scene")]
    public LevelSelectionData levelData;

    private InputActionMap UIActionMap;

    private void Start()
    {
        UIActionMap = Hero.Player().GetComponent<Hero>().UIActionMap;
        UIActionMap.FindAction("Cancel").performed += OnCancel;
        EventSystem.current.SetSelectedGameObject(GetComponentInChildren<LevelsDisplayer>().FirstButton);
    }

    /// <summary>
    /// Is called when the Level Selection menu should be closed
    /// </summary>
    /// <param name="ctx">The context of the action</param>
    private void OnCancel(CallbackContext ctx)
    {
        if (ctx.interaction is PressInteraction)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Unbind the controls to the action maps in order to avoid action triggered by destroyed objects 
    /// </summary>
    private void OnDestroy()
    {
        UIActionMap.FindAction("Cancel").performed -= OnCancel;
    }

    /**
     * Load the level scene and store the "mapName" in the level data
     */
    public void LoadLevelScene(string mapName)
    {
        SaveSystem.Save();
        levelData.levelName = mapName;
        SceneManager.LoadScene(levelSceneName);
    }

    private void OnEnable()
    {
        Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("UI");
        EventSystem.current.SetSelectedGameObject(GetComponentInChildren<LevelsDisplayer>().FirstButton);
    }

    private void OnDisable()
    {
        if(Hero.Player() != null)
            Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("Player");
    }
}
