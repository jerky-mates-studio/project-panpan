using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DashState 
{
    protected DashableUnit Context { get; set; }

    public DashState(DashableUnit context)
    {
        Context = context;
    }

    /// <summary>
    /// The method that must be called when this state become the current state
    /// </summary>
    public abstract void OnEnterState();

    /// <summary>
    /// The method that must be called in order to update the current state
    /// </summary>
    public abstract void UpdateState();

    /// <summary>
    /// Handle the collision between this object and another one
    /// </summary>
    public abstract void HandleCollision(Collision2D collision);

}
