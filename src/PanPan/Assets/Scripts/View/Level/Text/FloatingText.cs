﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    private GameObject speaker;
    [Tooltip("The offset to apply to the dialog box according to the speaker")]
    [SerializeField]
    private Vector3 offset;

    public GameObject Speaker { get => speaker; set => speaker = value; }

    // Update is called once per frame
    void Update()
    {
        // Check if the speaker still exist
        if (Speaker == null)
            Destroy(gameObject);
        else
            transform.position = Speaker.transform.position + offset;

    }
}
