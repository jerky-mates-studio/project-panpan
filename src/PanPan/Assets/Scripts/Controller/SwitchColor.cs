﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchColor : MonoBehaviour
{

    private static SwitchColor instance;
    public static SwitchColor Instance()
    {
        return instance;
    }

    private Camera switchColorCamera;
    private bool switchColor = false;

    private void Awake()
    {
        if (Instance() == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<RawImage>().texture = CameraRecorder.Instance().gameObject.GetComponent<Camera>().targetTexture;
    }

    /**
     * Activate or deactivate the color switch
     */
    public void TriggerSwitch()
    {
        switchColor = !switchColor;
        // boolean don't really exist in shaders. 1f is true and 0f is false
        GetComponent<RawImage>().material.SetFloat("_SwitchColor", switchColor ? 1f : 0f);
    }
}
