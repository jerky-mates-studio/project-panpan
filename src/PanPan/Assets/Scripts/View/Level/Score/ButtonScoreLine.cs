using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(AudioSource))]
public class ButtonScoreLine : MonoBehaviour, IScoreLine
{
    [Tooltip("The sound to play when a score line is displayed")]
    [SerializeField]
    private AudioClip lineDisplayedSound;

    private Button button;
    private Text buttonText;
    private AudioSource audioSource;

    private Color initialButtonColor;
    private Color initialTextColor;

    public IEnumerator DisplayLine()
    {
        button.image.color = initialButtonColor;
        buttonText.color = initialTextColor;

        button.interactable = true;
        audioSource.PlayOneShot(lineDisplayedSound);
        yield return null;
    }

    public IEnumerator HideLine()
    {
        Color newColor = initialTextColor;
        newColor.a = 0f;
        buttonText.color = newColor;

        newColor = initialButtonColor;
        newColor.a = 0f;
        button.image.color = newColor;

        button.interactable = false;
        yield return null;
    }

    private void Awake()
    {
        button = GetComponent<Button>();
        buttonText = GetComponentInChildren<Text>();
        audioSource = GetComponent<AudioSource>();

        initialButtonColor = button.image.color;
        initialTextColor = buttonText.color;

        InitDisplay();
    }

    private void InitDisplay()
    {
        Color newColor = initialButtonColor;
        newColor.a = 0f;
        button.image.color = newColor;

        newColor = initialTextColor;
        newColor.a = 0f;
        buttonText.color = newColor;

        button.interactable = false;
    }
}
