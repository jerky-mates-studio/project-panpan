using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchLoadedCondition : ActivationCondition, ISwitchLoadedCondition
{
    [Header("Condition parameter")]
    [SerializeField]
    [Tooltip("The LoadSwitch gameobject that must be loaded so that this condition can be true")]
    private GameObject loadSwitch;

    private void OnEnable()
    {
        loadSwitch.GetComponent<LoadSwitchEvent>().SwitchActivated += HandleSwitchActivation;
    }

    private void OnDisable()
    {
        loadSwitch.GetComponent<LoadSwitchEvent>().SwitchActivated -= HandleSwitchActivation;
    }

    /// <summary>
    /// Handle the activation/deactivation of a LoadSwitch
    /// </summary>
    /// <param name="isActivated">True iif the switch has been activated, false if it has beeen deactivated</param>
    private void HandleSwitchActivation(bool isActivated)
    {
        TriggerActivation(isActivated);
    }

    public void Deactivate()
    {
        LoadSwitch loadSwitchScript = loadSwitch.GetComponent<LoadSwitch>();

        if (loadSwitchScript)
        {
            if (loadSwitchScript.IsActivated())
            {
                loadSwitchScript.ResetLoad();
            }
        }
    }

    public void Activate()
    {
        LoadSwitch loadSwitchScript = loadSwitch.GetComponent<LoadSwitch>();

        if (loadSwitchScript)
        {
            if ( ! loadSwitchScript.IsActivated())
            {
                loadSwitchScript.FillLoad();
            }
        }
    }
}
