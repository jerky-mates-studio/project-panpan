using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.InputSystem;

public class ScoreUI : MonoBehaviour
{
    public enum Rank
    {
        [Description("Capital Dome Citizen")]
        CapitalDomeCitizen,
        [Description("Union Intervention Brigades Soldier")]
        UnionInterventionBrigadesSoldier,
        [Description("Union Intervention Brigades Captain")]
        UnionInterventionBrigadesCaptain,
        [Description("Mutant Elite Soldier Prototype")]
        MutantEliteSoldierPrototype
    }

    public struct Score
    {
        public float completionTime;
        public int enemiesKilled;
        public int playerDeath;
        public Rank rank;
    }

    [Tooltip("The panel that is responsible to display the score at the screen")]
    [SerializeField]
    private GameObject scorePanel;

    private void OnEnable()
    {
        DisplayScorePanelEvent.GetInstance().ScorePanelDisplayed += DisplayScorePanel;
    }

    private void OnDisable()
    {
        DisplayScorePanelEvent.GetInstance().ScorePanelDisplayed -= DisplayScorePanel;
    }

    /// <summary>
    /// Display the player score at the screen + prevent player to provide inputs to the character
    /// </summary>
    /// <param name="score"></param>
    private void DisplayScorePanel(Score score)
    {
        Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("UI");

        scorePanel.SetActive(true);
        ScorePanel scorePanelScript = scorePanel.GetComponent<ScorePanel>();
        scorePanelScript.SetScore(score);
    }

    /// <summary>
    /// Close the score panel + allows the player to provide inputs to the character
    /// </summary>
    public void CloseScorePanel()
    {
        scorePanel.SetActive(false);
        Hero.Player().GetComponent<PlayerInput>().SwitchCurrentActionMap("Player");
        StartNextLevelEvent.GetInstance().TriggerStartNextLevel();
    }

}
